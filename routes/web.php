<?php

use App\Http\Controllers\AddonsController;
use App\Http\Controllers\ArtisanController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\PdfController;

use App\Http\Controllers\ClientController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DatabaseBackupController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\Auditor\ActivationController;
use App\Http\Controllers\Auditor\AuditStageController;
use App\Http\Controllers\Auditor\FeedbackController;
use App\Http\Controllers\Auditor\FinancialStatementController;
use App\Http\Controllers\Auditor\ChartOfAccountController;
use App\Http\Controllers\Auditor\ProjectController;
use App\Http\Controllers\Auditor\TrialBalanceController;
use App\Http\Controllers\Auditor\QuestionController;
use App\Http\Controllers\Company\CompanyAuditorController;
use App\Http\Controllers\Company\CompanyClientController;
use App\Http\Controllers\Company\CompanyDocumentController;
use App\Http\Controllers\Company\CompanyProjectController;
use App\Http\Controllers\Company\CompanySettingController;
use App\Http\Controllers\PackagesController;
use Illuminate\Foundation\Application;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return Inertia::render('Welcome', [
    'canLogin' => Route::has('login'),
    'canRegister' => Route::has('register'),
    'laravelVersion' => Application::VERSION,
    'phpVersion' => PHP_VERSION,
  ]);
});

Route::get('/email/verify', function () {
  return view('auth.verify-email');
})->middleware('auth')->name('verification.notice');

Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
  $request->fulfill();

  return redirect('/home');
})->middleware(['auth', 'signed'])->name('verification.verify');

Route::prefix('auditor')->name('auditor.')->controller(ActivationController::class)->group(function () {
  Route::get('activation/{token}', 'index')->name('activation');
  Route::post('activate', 'activate')->name('activate');
});

Route::get('/document/upload', function () {
  return Inertia::render("AdminKAP/DocumentUpload");
})->name('document.upload');

Route::get('/thankyou', function () {
  return Inertia::render("Auth/Thankyou");
})->name('thankyou');


// Register
Route::prefix('register')->name('register.')
  ->controller(RegisteredUserController::class)
  ->group(function () {
    Route::get('submited', function () {
      return Inertia::render("AdminKAP/Register/RegisterSubmited");
    })->name('submited');

    Route::middleware(['auth', 'verified', 'role:company-admin'])->group(function () {
      Route::get('account-registration', 'accountRegistration')->name('account-registration');
      Route::post('account-registration', 'accountRegistrationStore')->name('account-registration.store');

      Route::get('upload-document-legal', 'uploadDocumentLegal')->name('upload-document-legal');
      Route::post('upload-document-legal', 'uploadDocumentLegalStore')->name('upload-document-legal.store');

      Route::get('document-verification', 'documentVerification')->name('document-verification');

      Route::prefix('package-billing')->name('package-billing.')->group(function () {
        Route::get('/', 'packageBilling')->name('index');
        Route::post('/', 'packageBillingStore')
          ->name('store')
          ->middleware('throttle:3,1');
        Route::post('cancel', 'packageBillingCancel')
          ->name('cancel')
          ->middleware('throttle:3,1');
        Route::post('payment', 'packageBillingPayment')
          ->name('payment')
          ->middleware('throttle:3,1');
      });
    });
  });

Route::middleware(['auth', 'verified', 'status'])->group(function () {
  // Super Admin
  Route::prefix('admin')->name('admin.')->middleware(['role:super-admin'])->group(function () {
    Route::get('dashboard', function () {
      return Inertia::render("Admin/Dashboard");
    })->name('dashboard');

    Route::prefix('clients')->name('clients.')->controller(ClientController::class)->group(function () {
      Route::get('/', 'index')->name('index');
      Route::get('show/{company}', 'show')->name('show');
      Route::get('activate/{user}', 'activate')->name('activate');
      Route::post('incomplete/{user}', 'incomplete')->name('incomplete');
    });

    Route::get('database-backup', function () {
      return Inertia::render("Admin/DatabaseBackup");
    })->name('database-backup');

    Route::prefix('packages')->name('packages.')->controller(PackagesController::class)->group(function () {
      Route::get('/', 'index')->name('index');
      Route::post('store', 'store')->name('store');
      Route::get('edit/{package}', 'edit')->name('edit');
      Route::post('update', 'update')->name('update');
      Route::get('destroy/{package}', 'destroy')->name('destroy');
    });

    Route::prefix('addons')->name('addons.')->controller(AddonsController::class)->group(function () {
      Route::get('/', 'index')->name('index');
      Route::post('store', 'store')->name('store');
      Route::get('edit/{package_add_on}', 'edit')->name('edit');
      Route::post('update', 'update')->name('update');
      Route::get('destroy/{package_add_on}', 'destroy')->name('destroy');
    });

    Route::prefix('finance')->name('finance.')->controller(TransactionController::class)->group(function () {
      Route::get('/', 'index')->name('index');
    });

    // Settings
    Route::prefix('settings')->name('settings.')->controller(SettingController::class)->group(function () {
      Route::get('/', 'index')->name('index');

      Route::get('/about', 'about')->name('about');
      Route::post('/about/update', 'updateAbout')->name('about.update');

      Route::get('/storage', function () {
        return Inertia::render("Admin/Settings/Storage");
      })->name('storage');

      Route::get('/legal', function () {
        return Inertia::render("Admin/Settings/Legal");
      })->name('legal');
      Route::get('/legal/legal-notice', 'legalNotice')->name('legal.legal-notice');
      Route::get('/legal/license', 'legalLicense')->name('legal.license');
      Route::get('/privacy-policy', 'legalPrivacyPolicy')->name('legal.privacy-policy');
      Route::get('/term-and-conditions', 'legalTermAndConditions')->name('legal.term-and-conditions');
      Route::get('/legal/certification', 'legalCertification')->name('legal.certification');
      Route::post('/legal/update', 'updateLegal')->name('legal.update');
      Route::post('/legal/certification/update', 'updateLegalCertification')->name('legal.certification.update');

      Route::get('/materiality', 'materiality')->name('materiality');
      Route::post('/materiality/update', 'updateMateriality')->name('materiality.update');

      Route::get('/risk', 'risk')->name('risk');
      Route::post('/risk/update', 'updateRisk')->name('risk.update');
      Route::post('/risk/inherent/store', 'storeInherentRisk')->name('risk.inherent.store');
      Route::post('/risk/inherent/update', 'updateInherentRisk')->name('risk.inherent.update');
      Route::post('/risk/inherent/destroy', 'destroyInherentRisk')->name('risk.inherent.destroy');

      Route::get('/likelihood', 'likelihood')->name('likelihood');
      Route::post('/likelihood/update', 'updateLikelihood')->name('likelihood.update');

      Route::get('/control-risk', 'controlRisk')->name('control-risk');
      Route::post('/control-risk/update', 'updateControlRisk')->name('control-risk.update');

      Route::get('/confidence-level', 'confidenceLevel')->name('confidence-level');
      Route::post('/confidence-level/update', 'updateConfidenceLevel')->name('confidence-level.update');

      Route::get('/risk-of-incorrect-acceptance', 'riskOfIncorrectAcceptance')->name('risk-of-incorrect-acceptance');
      Route::post('/risk-of-incorrect-acceptance/update', 'updateRiskOfIncorrectAcceptance')->name('risk-of-incorrect-acceptance.update');

      Route::get('/user-access-control', 'userAccessControl')->name('user-access-control');
      Route::post('/user-access-control/update', 'updateUserAccessControl')->name('user-access-control.update');

      Route::get('/menu-settings', 'menu')->name('menu-settings');
      Route::post('/menu-settings/update', 'updateMenu')->name('menu-settings.update');

      Route::get('/sign-off-setting', function () {
        return Inertia::render("Admin/Settings/SignOffSetting");
      })->name('sign-off-setting');

      Route::prefix('sign-off-setting')->name('sign-off-setting.')->group(function () {
        Route::get('/pre-engagement', 'preEngagement')->name('pre-engagement');
        Route::get('/identify-and-assess-risk', 'identifyAndAssessRisk')->name('identify-and-assess-risk');
        Route::get('/design-audit-response', 'designAuditResponse')->name('design-audit-response');
        Route::get('/obtain-audit-evidance', 'obtainAuditEvidance')->name('obtain-audit-evidance');
        Route::get('/completion-and-reporting', 'completionAndReporting')->name('completion-and-reporting');
      });

      Route::get('/change-password', function () {
        return Inertia::render("Admin/Settings/ChangePassword");
      })->name('change-password');
      Route::post('/change-password/update', 'updateChangePassword')->name('change-password.update');
    });
  });

  // Company Admin
  Route::prefix('company')->name('company.')->middleware(['role:company-admin'])->group(function () {
    Route::prefix('documents')->name('documents.')->controller(CompanyDocumentController::class)->group(function () {
      Route::get('/', 'index')->name('index');
      Route::get('/{type}', 'type')->name('type');
      Route::post('store', 'store')->name('store');
      Route::post('rename', 'rename')->name('rename');
      Route::get('destroy/{company_document}', 'destroy')->name('destroy');
    });

    Route::get('dashboard', function () {
      return Inertia::render("AdminKAP/Dashboard");
    })->name('dashboard');

    Route::prefix('clients')->name('clients.')->controller(CompanyClientController::class)->group(function () {
      Route::get('/', 'index')->name('index');
      Route::post('store', 'store')->name('store');
      Route::get('edit/{company_client}', 'edit')->name('edit');
      Route::post('update', 'update')->name('update');
      Route::get('destroy/{company_client}', 'destroy')->name('destroy');
    });

    Route::prefix('auditors')->name('auditors.')->controller(CompanyAuditorController::class)->group(function () {
      Route::get('/', 'index')->name('index');
      Route::get('roles', 'roles')->name('roles');
      Route::post('store', 'store')->name('store');
      Route::get('edit/{auditor}', 'edit')->name('edit');
      Route::post('update', 'update')->name('update');
      Route::get('destroy/{auditor}', 'destroy')->name('destroy');
    });

    Route::prefix('projects')->name('projects.')->controller(CompanyProjectController::class)->group(function () {
      Route::get('/on-going', 'onGoing')->name('on-going');
      Route::get('/completed', 'completed')->name('completed');
    });

    // Settings
    Route::prefix('settings')->name('settings.')->controller(CompanySettingController::class)->group(function () {
      Route::get('/', 'index')->name('index');
      Route::get('/about', 'about')->name('about');
      Route::get('/date-time', 'date')->name('date');
      Route::post('/date-time/update', 'updateDate')->name('date.update');

      Route::get('/fonts', 'fonts')->name('fonts');
      Route::get('/fonts/type', function () {
        return Inertia::render("AdminKAP/Settings/FontType");
      })->name('fonts.type');
      Route::post('/fonts/update', 'updateFonts')->name('fonts.update');

      Route::get('/language', 'language')->name('language');
      Route::get('/language/detail', function () {
        return Inertia::render("AdminKAP/Settings/Language");
      })->name('language.detail');
      Route::get('/language/region', function () {
        return Inertia::render("AdminKAP/Settings/Region");
      })->name('language.region');
      Route::post('/language/update', 'updateLanguageRegion')->name('language.update');

      Route::get('/legal', 'legal')->name('legal');
      Route::get('/legal/notice', 'legalNotice')->name('legal.notice');
      Route::get('/legal/license', 'legalLicense')->name('legal.license');
      Route::get('/legal/certification', 'legalCertification')->name('legal.certification');

      Route::get('/materiality', 'materiality')->name('materiality');
      Route::post('/materiality/update', 'updateMateriality')->name('materiality.update');

      Route::get('/risk', 'risk')->name('risk');
      Route::post('/risk/update', 'updateRisk')->name('risk.update');
      Route::post('/risk/inherent/store', 'storeInherentRisk')->name('risk.inherent.store');
      Route::post('/risk/inherent/update', 'updateInherentRisk')->name('risk.inherent.update');
      Route::post('/risk/inherent/destroy', 'destroyInherentRisk')->name('risk.inherent.destroy');

      Route::get('/likelihood', 'likelihood')->name('likelihood');
      Route::post('/likelihood/update', 'updateLikelihood')->name('likelihood.update');
      Route::get('/menu-settings', 'menu')->name('menu');
      Route::post('/menu-settings/update', 'updateMenu')->name('menu.update');

      Route::get('/company-profile', function () {
        return Inertia::render("AdminKAP/Settings/CompanyProfile");
      })->name('company');
      Route::get('/company-profile/general', 'companyProfile')->name('company.general');
      Route::get('/company-profile/billing-rate', 'billingRate')->name('company.billing-rate');
      Route::post('/company-profile/billing-rate/update', 'updateBillingRate')->name('company.billing-rate.update');
      Route::get('/company-profile/permit-legal-document', function () {
        return Inertia::render("AdminKAP/Settings/PermitLegal");
      })->name('company.permit-legal-document');

      Route::get('/company-profile/privacy-policy', 'legalPrivacyPolicy')->name('company.privacy-policy');
      Route::get('/company-profile/term-and-conditions', 'legalTermAndConditions')->name('company.term-and-conditions');

      Route::get('/display', 'display')->name('display');
      Route::get('/display/auto-lock', function () {
        return Inertia::render("AdminKAP/Settings/AutoLock");
      })->name('display.auto-lock');
      Route::post('/display/update', 'updateDisplay')->name('display.update');

      Route::get('/change-password', function () {
        return Inertia::render("AdminKAP/Settings/ChangePassword");
      })->name('change-password');
      Route::post('/change-password/update', 'updateChangePassword')->name('change-password.update');
    });

    Route::get('settings/update', function () {
      return Inertia::render("AdminKAP/Settings/SoftwareUpdate");
    })->name('settings.update');
    Route::get('settings/storage', function () {
      return Inertia::render("AdminKAP/Settings/Storage");
    })->name('settings.storage');
  });

  // Auditor
  Route::prefix('auditor')->name('auditor.')->middleware(['role:auditor'])->group(function () {
    Route::get('/dashboard', function () {
      return Inertia::render("Auditor/Dashboard");
    })->name('dashboard');

    // Settings
    Route::get('settings', function () {
      return Inertia::render("Auditor/Settings/Index");
    })->name('settings.index');
    Route::get('settings/about', function () {
      return Inertia::render("Auditor/Settings/About");
    })->name('settings.about');
    Route::get('settings/update', function () {
      return Inertia::render("Auditor/Settings/SoftwareUpdate");
    })->name('settings.update');
    Route::get('settings/storage', function () {
      return Inertia::render("Auditor/Settings/Storage");
    })->name('settings.storage');
    Route::get('settings/date-time', function () {
      return Inertia::render("Auditor/Settings/DateTime");
    })->name('settings.date');
    Route::get('settings/fonts', function () {
      return Inertia::render("Auditor/Settings/Fonts");
    })->name('settings.fonts');
    Route::get('/fonts/type', function () {
      return Inertia::render("Auditor/Settings/FontType");
    })->name('settings.fonts.type');
    Route::get('settings/language', function () {
      return Inertia::render("Auditor/Settings/LanguageRegion");
    })->name('settings.language');
    Route::get('/language/detail', function () {
      return Inertia::render("Auditor/Settings/Language");
    })->name('settings.language.detail');
    Route::get('/language/region', function () {
      return Inertia::render("Auditor/Settings/Region");
    })->name('settings.language.region');
    Route::get('settings/legal', function () {
      return Inertia::render("Auditor/Settings/Legal");
    })->name('settings.legal');
    Route::get('/legal/notice', function () {
      return Inertia::render("Auditor/Settings/LegalNotice");
    })->name('settings.legal.notice');
    Route::get('/legal/license', function () {
      return Inertia::render("Auditor/Settings/License");
    })->name('settings.legal.license');
    Route::get('/legal/certification', function () {
      return Inertia::render("Auditor/Settings/Certification");
    })->name('settings.legal.certification');
    Route::get('settings/display', function () {
      return Inertia::render("Auditor/Settings/Display");
    })->name('settings.display');
    Route::get('/display/auto-lock', function () {
      return Inertia::render("Auditor/Settings/AutoLock");
    })->name('settings.display.auto-lock');
    Route::get('/change-password', function () {
      return Inertia::render("Auditor/Settings/ChangePassword");
    })->name('settings.change-password');

    Route::prefix('projects')->name('projects.')->controller(ProjectController::class)->group(function () {
      Route::get('/', 'index')->name('index');
      Route::get('existing', 'existing')->name('existing');
      Route::get('audit', 'audit')->name('audit');
      Route::post('store', 'store')->name('store');
      Route::get('edit/{project}', 'edit')->name('edit');
      Route::post('update', 'update')->name('update');
      Route::get('destroy/{project}', 'destroy')->name('destroy');
    });

    Route::prefix('audit-stages')->name('audit-stages.')->controller(AuditStageController::class)->group(function () {
      Route::get('/{project_id}', 'index')->name('index');
      Route::get('/detail/{audit_stage_id}', 'detail')->name('detail');
    });

    Route::prefix('chart-of-accounts/{project_id}')
      ->name('chart-of-accounts.')
      ->middleware('project')
      ->controller(ChartOfAccountController::class)
      ->group(function () {
        Route::get('/', 'index')->name('index');
        Route::get('/option', 'option')->name('option');
        Route::post('/store', 'store')->name('store');
        Route::get('destroy/{chart_of_account}', 'destroy')->name('destroy');
      });

    Route::prefix('trial-balances/{project_id}')
      ->name('trial-balances.')
      ->middleware('project')
      ->controller(TrialBalanceController::class)
      ->group(function () {
        Route::get('/', 'index')->name('index');
        Route::post('/store', 'store')->name('store');
        Route::get('destroy/{chart_of_account}', 'destroy')->name('destroy');
      });

    Route::prefix('financial-statements/{project_id}')
      ->name('financial-statements.')
      ->middleware('project')
      ->controller(FinancialStatementController::class)
      ->group(function () {
        Route::get('/', 'index')->name('index');
      });

    Route::prefix('aseries/{project_id}')
      ->name('aseries.')
      ->middleware('project')
      ->controller(QuestionController::class)
      ->group(function () {
        Route::get('a1', 'a1')->name('a1');
        Route::post('a1/store', 'a1Store')->name('a1.store');

        Route::get('a3', 'a3')->name('a3');

        Route::get('a23', function () {
          return Inertia::render('Auditor/ASeries/A23');
        })->name('a23');

        Route::get('a24', function () {
          return Inertia::render('Auditor/ASeries/A24');
        })->name('a24');
      });

    Route::prefix('feedbacks')
      ->name('feedbacks.')
      ->controller(FeedbackController::class)
      ->group(function () {
        Route::get('/', 'index')->name('index');
      });

    Route::prefix('feedbacks/{project_id}')
      ->name('feedbacks.')
      ->middleware('project')
      ->controller(FeedbackController::class)
      ->group(function () {
        Route::post('store', 'store')->name('store');
        Route::post('reply', 'reply')->name('reply');
      });

    Route::prefix('slicing')->group(function () {
      Route::get('a4', function () {
        return Inertia::render('Auditor/ASeries/A4/index');
      });
      Route::get('a5', function () {
        return Inertia::render('Auditor/ASeries/A5/index');
      });
      Route::get('a6', function () {
        return Inertia::render('Auditor/ASeries/A6/index');
      });
      Route::get('a6/NewClientAcceptance', function () {
        return Inertia::render('Auditor/ASeries/A6/NewClientAcceptance');
      });
    });
  });
});

Route::prefix('webhooks')->group(function () {
  Route::post('payment', [PaymentController::class, 'index'])->name('payment.index');
});

require __DIR__ . '/auth.php';


/* Dev only */
/* Artisan */
Route::prefix('artisan')
  ->name('artisan.')
  ->controller(ArtisanController::class)
  ->group(function () {
    Route::get('migrate', 'migrate')->name('migrate');
    Route::get('migrate-fresh', 'migrateFresh')->name('migrate-fresh');
    Route::get('seed', 'seed')->name('seed');
  });

/* Email */
Route::prefix('mail')
  ->name('mail.')
  ->controller(MailController::class)
  ->group(function () {
    Route::get('invoice', 'invoice')->name('invoice');
    Route::get('expired', 'expired')->name('expired');
    Route::get('paid', 'paid')->name('paid');
  });

/* PDF */
Route::prefix('pdf')
  ->name('pdf.')
  ->controller(PdfController::class)
  ->group(function () {
    Route::get('invoice', 'invoice')->name('invoice');
  });
