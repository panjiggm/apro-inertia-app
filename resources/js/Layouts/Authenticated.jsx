import React, { useState, Fragment } from "react";
import NavbarSidebar from "@/Components/NavbarSidebar";
import ResponsiveNavLink from "@/Components/ResponsiveNavLink";
import { Link } from "@inertiajs/inertia-react";
import { Menu, Transition } from "@headlessui/react";

export default function Authenticated({ auth, header, children, childrenPadding = true , mainClass = ""}) {
    const [sidebarSmall, setSidebarSmall] = useState(false);

    return (
        <>
            <aside className={`ml-[-100%] fixed z-10 top-0 pb-6 px-6 w-full flex flex-col justify-between h-screen border-r border-[#E6E6E8] bg-white transition duration-300 lg:ml-0 ${sidebarSmall ? "md:w-[100px] lg:w-[100px] xl:w-[100px] 2xl:w-[100px]" : "md:w-4/12 lg:w-[25%] xl:w-[20%] 2xl:w-[15%]"} `}>
                <div className="relative flex flex-col h-full">
                    {/* <button onClick={ () => setSidebarSmall(!sidebarSmall)} className={`border bg-white border-[#E6E6E8] rounded-md text-lg px-2 absolute top-[26px] ${sidebarSmall ? "left-[59px]" : "left-[100%] ml-[7px]"}`}>=</button> */}
                    {
                        sidebarSmall ? (
                            <div className="mt-[20px] mb-[65px]">
                                <a href="#" title="home">
                                    <h1 className="font-montserrat text-xl text-[#2A2A2A] flex items-center justify-center">
                                        <img src="/images/logo-icon.png" alt="APPRO" />
                                    </h1>
                                </a>
                            </div>
                        ) : (
                            <div className="p-[30px]">
                                <a href="#" title="home">
                                    <h1 className="font-montserrat text-xl text-[#2A2A2A] flex items-center justify-center">
                                        <img src="/images/dummy-client-logo.png" alt="Dummy Client Logo" className="w-32" />
                                    </h1>
                                </a>
                            </div>
                        )
                    }

                    <NavbarSidebar role={auth.user.role} sidebarSmall={sidebarSmall} />

                    <div className="flex-1 flex items-end justify-center">
                        <a href="#" title="home">
                        <span className="text-center text-xs text-[rgba(0,0,0,0.4)] w-full block mb-2">Powered By</span>
                            <h1 className="font-montserrat text-xl text-[#2A2A2A] flex items-center justify-center">
                                <img src="/images/logo.png" alt="APPRO" className="w-32" />
                            </h1>
                        </a>
                    </div>
                </div>
            </aside>
            <div className={`ml-auto ${sidebarSmall ? "lg:w-[calc(100%-100px)] xl:w-[calc(100%-100px)] 2xl:w-[calc(100%-100px)]" : "lg:w-[75%] xl:w-[80%] 2xl:w-[85%]"} `}>
                <RenderHeader header={header} auth={auth} />
                <div className={`${childrenPadding ? "px-6 pt-6" : ""} 3xl:container bg-[#f3f4f694] min-h-screen ${mainClass}`}>
                    <main>{children}</main>
                </div>
            </div>
        </>
    );
}

const RenderHeader = ({ header, auth }) => {
    if (!header) {
        return null;
    }

    return (
        <div className="sticky top-0 py-[19px] border-b border-[#ECEFF7] bg-white z-10">
            <div className="px-6 flex items-center justify-between space-x-4 3xl:container">
                {header}
                <button className="w-12 h-16 -mr-2 border-r lg:hidden">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-6 w-6 my-auto"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                    >
                        <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth="2"
                            d="M4 6h16M4 12h16M4 18h16"
                        />
                    </svg>
                </button>
                <div className="flex space-x-4 items-center">

                    <MyDropdown auth={auth} />
                </div>
            </div>
        </div>
    )
}

function MyDropdown({ auth }) {
    return (
        <Menu as="div" className="relative inline-block text-left">
            <Menu.Button className="flex items-center justify-between min-w-[250px]">
                <div className="flex items-center">
                    <img
                        src="/images/user_profile.png"
                        className="w-11 h-11 rounded-full mr-5"
                    />
                    <div className="flex-1 flex flex-col justify-start text-left">
                        <strong className="block text-[#2A2A2A] text-sm font-semibold">
                            {auth.user.name}
                        </strong>
                        <span className="text-[#464646cc] text-xs font-medium">
                            Admin
                        </span>
                    </div>
                </div>

                <div className="w-6 h-6 bg-[#ECEFF7] rounded-full flex items-center justify-center">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="10"
                        height="6"
                        fill="none"
                        viewBox="0 0 10 6"
                    >
                        <path
                            fill="#2A2A2A"
                            d="M9.395.555a.82.82 0 010 1.176L5.697 5.323a1 1 0 01-1.394 0L.605 1.731A.82.82 0 011.748.555l2.904 2.82a.5.5 0 00.696 0L8.252.556a.82.82 0 011.143 0z"
                        ></path>
                    </svg>
                </div>
            </Menu.Button>

            <Transition
                as={Fragment}
                enter="transition ease-out duration-100"
                enterFrom="transform opacity-0 scale-95"
                enterTo="transform opacity-100 scale-100"
                leave="transition ease-in duration-75"
                leaveFrom="transform opacity-100 scale-100"
                leaveTo="transform opacity-0 scale-95"
            >
                <Menu.Items className="absolute right-0 mt-5 w-56 origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                    <div className="px-1 py-1 ">
                        <Menu.Item>
                            {({ active }) => (
                                <button
                                    className={`${active ? 'bg-[#55a8ef1f] text-gray-600' : 'text-gray-600'
                                        } group flex w-full items-center rounded-md px-2 py-2 text-sm`}
                                >
                                    <svg xmlns="http://www.w3.org/2000/svg" className="mr-2 h-5 w-5 text-violet-400" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
                                        <path strokeLinecap="round" strokeLinejoin="round" d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
                                    </svg>
                                    My Profile
                                </button>
                            )}
                        </Menu.Item>
                    </div>
                    <div className="px-1 py-1">
                        <Menu.Item>
                            {({ active }) => (
                                <Link
                                    method="get" href={route('logout')} as="button"
                                    className={`${active ? 'bg-[#55a8ef1f] text-gray-600' : 'text-gray-600'
                                        } group flex w-full items-center rounded-md px-2 py-2 text-sm`}
                                >
                                    <svg xmlns="http://www.w3.org/2000/svg" className="mr-2 h-5 w-5 text-violet-400" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
                                        <path strokeLinecap="round" strokeLinejoin="round" d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1" />
                                    </svg>
                                    Logout
                                </Link>
                            )}
                        </Menu.Item>
                    </div>
                </Menu.Items>
            </Transition>
        </Menu>
    )
}
