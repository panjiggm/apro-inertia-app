export default function TitleWithDropdown({ title, label }) {
    return (
        <div className="mb-9 flex items-center justify-between mx-6">
            <h2 className="text-lg font-semibold">{title}</h2>
            <div className="flex justify-between items-center p-3 rounded-lg border border-[#E2E2E4]">
                <span className="mr-2 text-xs text-[#464646]">{label}</span>
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="8"
                    height="5"
                    fill="none"
                    viewBox="0 0 8 5"
                >
                    <path
                        fill="#464646"
                        d="M7.516.644a.656.656 0 010 .94l-2.82 2.74a1 1 0 01-1.393 0L.484 1.583a.656.656 0 01.914-.94l2.254 2.189a.5.5 0 00.696 0L6.602.643a.656.656 0 01.914 0z"
                    ></path>
                </svg>
            </div>
        </div>
    );
}
