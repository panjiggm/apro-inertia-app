import EmptyState from "../EmptyState";
import ItemClientTalk from "./ItemClientTalk";
import TitleWithSeeAll from "./TitleWIthSeeAll";

export default function ClientTalk() {
    return (
        <div className="w-full border border-[#E6E6E8] rounded-2xl px-8 py-9 bg-white mb-5 shadow-md">
            <TitleWithSeeAll title="Staff Talk" />
            <EmptyState image="/images/discus.png" description="No Discussion Yet" />
            {/* <ItemClientTalk
                title="Bessie Cooper"
                subtitle="Where we gonna do?"
                date="7/11/19"
                count="4"
            />
            <ItemClientTalk
                title="Bessie Cooper"
                subtitle="What's the progress on ..."
                date="7/11/19"
                count="4"
            />
            <ItemClientTalk
                title="Guy Hawkins"
                subtitle="I went there yesterday"
                date="7/11/19"
                count="120"
            />
            <ItemClientTalk
                title="Jacob Jones"
                subtitle="Where we gonna do?"
                date="7/11/19"
                count="4"
            /> */}
        </div>
    );
}
