import {
    Bar,
    BarChart,
    CartesianGrid,
    Cell,
    Legend,
    Pie,
    PieChart,
    ResponsiveContainer,
    Sector,
    Tooltip,
    XAxis,
    YAxis,
} from "recharts";
import EmptyState from "../EmptyState";
import { ItemStatusClient } from "./ItemStatusClient";
import TitleWithSeeAll from "./TitleWIthSeeAll";
const data1 = [
    { name: "Group A", value: 80 },
    { name: "Group C", value: 20 },
];
const COLORS = ["#00CB75", "#FFA513", "#FF007A"];

export default function ClientStatus() {
    const RADIAN = Math.PI / 180;
    const renderCustomizedLabel = ({
        cx,
        cy,
        midAngle,
        innerRadius,
        outerRadius,
        percent,
        index,
    }) => {
        const radius = innerRadius + (outerRadius - innerRadius) * 0.25;
        const x = cx + radius * Math.cos(-midAngle * RADIAN);
        const y = cy + radius * Math.sin(-midAngle * RADIAN);
        console.log('x', x, y)
        return (
            <text
                x={index === 0 ? 165 : 220}
                y={index === 0 ? 210 : 188}
                fill="white"
                textAnchor={x > cx ? "start" : "end"}
                dominantBaseline="central"
            >
                {`${(percent * 100).toFixed(0)}%`}
            </text>
        );
    };
    return (
        <div className="w-full border border-[#E6E6E8] rounded-2xl px-8 py-9 bg-white mb-5 shadow-md">
            <TitleWithSeeAll title="Package Informations" seeAll={false}  />
            <div className="w-full h-[260px]">
                <ResponsiveContainer>
                    <PieChart>
                        <defs>
                            <linearGradient id="colorUv3" x1="0" y1="0" x2="0" y2="1">
                                <stop offset="5%" stopColor="#FFA513" stopOpacity={1} />
                                <stop offset="95%" stopColor="#F5415C" stopOpacity={1} />
                            </linearGradient>
                        </defs>
                        <Pie
                            data={data1}
                            cx={160}
                            cy={120}
                            innerRadius={60}
                            labelLine={false}
                            label={renderCustomizedLabel}
                            outerRadius={120}
                            fill="url(#colorUv3)"
                            dataKey="value"
                            activeShape={renderActiveShape}
                            activeIndex={0}
                        />
                    </PieChart>
                </ResponsiveContainer>
            </div>
            <div className="flex mt-4">
                <div className="flex-1 flex gap-2 items-center justify-center">
                    <div className="bg-[#00CB75] w-4 h-4" />
                    <span>Used</span>
                </div>
                <div className="flex-1 flex gap-2 items-center justify-center">
                    <div className="bg-[#FFA513] w-4 h-4" />
                    <span>Available</span>
                </div>
            </div>
            <div className="bg-[#157BCA] rounded-xl text-white flex p-3 justify-between text-xs items-center pl-5 mt-8">
                <span>10,15 Gb available </span>
                <button className="bg-[#2EA6FF] p-2 px-3 rounded-lg">Get More Space</button>
            </div>


        </div>
    );
}



const renderActiveShape = (props) => {
    const RADIAN = Math.PI / 180;
    const { cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle, fill, payload, percent, value } = props;
    const sin = Math.sin(-RADIAN * midAngle);
    const cos = Math.cos(-RADIAN * midAngle);
    const sx = cx + (outerRadius + 10) * cos;
    const sy = cy + (outerRadius + 10) * sin;
    const mx = cx + (outerRadius + 30) * cos;
    const my = cy + (outerRadius + 30) * sin;
    const ex = mx + (cos >= 0 ? 1 : -1) * 22;
    const ey = my;
    const textAnchor = cos >= 0 ? 'start' : 'end';

    return (
        <g>
            <text x={cx} y={cy} fontSize={25} fontWeight={700} dy={4} textAnchor="middle" fill={"#9FB3C5"}>
                Starter
            </text>
            <text x={cx} y={cy} fontSize={12} dy={20} textAnchor="middle" fill={"#9FB3C5"}>
                Exp. 11/2023
            </text>
            <defs>
                <linearGradient id="colorUv1" x1="0" y1="0" x2="0" y2="1">
                    <stop offset="5%" stopColor="#0AF08F" stopOpacity={1} />
                    <stop offset="95%" stopColor="#02A962" stopOpacity={1} />
                </linearGradient>
            </defs>
            <Sector
                cx={cx}
                cy={cy}
                innerRadius={innerRadius}
                outerRadius={outerRadius}
                startAngle={startAngle}
                endAngle={endAngle}
                fill="url(#colorUv1)"
                fillOpacity={1}
            />
        </g>
    );
};
