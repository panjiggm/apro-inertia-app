export default function ItemClientTalk({ title, subtitle, date, count }) {
    return (
        <div className="flex justify-between mb-4">
            <div className="flex space-x-4">
                <img
                    src="/images/dummy_user_profile.png"
                    className="w-11 h-11 rounded-full"
                />
                <div className="">
                    <strong className="block text-[#464646] text-sm font-medium">
                        {title}
                    </strong>
                    <span className="text-[#9FB3C5] text-xs">{subtitle}</span>
                </div>
            </div>
            <div className="text-right">
                <span className="text-[9px] text-[#464646] block">{date}</span>
                <span className="text-[8px] bg-[#FF5050] p-1 px-2 text-white inline-flex rounded-full">
                    {count}
                </span>
            </div>
        </div>
    );
}
