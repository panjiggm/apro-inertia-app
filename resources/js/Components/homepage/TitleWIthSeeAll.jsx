export default function TitleWithSeeAll({ title, seeAll = true }) {
    return (
        <div className="flex justify-between mb-8">
            <h2 className="text-[#2A2A2A] font-medium">{title}</h2>
            {
                seeAll && (
                    <a href="#" className="text-[#1989E9] text-xs">
                        See All
                    </a>
                )
            }
        </div>
    );
}
