import React, { useState } from 'react'
import InputForm from './clients/InputForm'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const dataField = {
    company_name: "",
    // tax_id: "",
    business_id_number: "",
    business_id_date: "",
    business_license_number: "",
    business_license_date: "",
    address: "",
    phone: "",
    website: "",
}

const labelField = {
    company_name: "Company Name",
    // tax_id: "Tax ID",
    business_id_number: "Business Identification Number (NIB) No.",
    business_id_date: "Dated",
    business_license_number: "Business Licence No.",
    business_license_date: "Dated",
    address: "Address",
    phone: "Phone No.",
    website: "Website",
}

export default function TabFormCompany({ onCancel, onSubmit }) {
    const [data, setData] = useState(dataField)
    const [errors, setErrors] = useState({})
    const [binDate, setBinDate] = useState(undefined)
    const [blnDate, setBlnDate] = useState(undefined)

    const handelSetData = (key, value) => {
        const temp = { ...data }
        temp[key] = value
        setData(temp)
    }

    const handleSubmit = () => {
        const tmp = { ...data }
        delete tmp.website
        tmp.business_license_date = binDate ? binDate.toISOString().split('T')[0] : ""
        tmp.business_id_date = blnDate ? blnDate.toISOString().split('T')[0] : ""
        const temp = { ...errors }
        for (const [key, value] of Object.entries(tmp)) {
            if (value === "") {
                temp[key] = `${labelField[key]} is required`
            } else {
                delete temp[key]
            }
        }
        setErrors(temp)
        if(Object.keys(temp).length === 0 && temp.constructor === Object){
            onSubmit(tmp)
        }
    }

    const handleReset = () => {
        setData(dataField)
    }

    return (
        <div className="flex flex-col space-y-6">
            <InputForm
                error={errors?.company_name}
                value={data?.company_name} onChange={(e) => handelSetData('company_name', e.target.value)}
                type="text"
                className="w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5"
                placeholder={labelField.company_name}
            />
            {/*<InputForm
                error={errors?.tax_id}
                value={data?.tax_id} onChange={(e) => handelSetData('tax_id', e.target.value)}
                type="text"
                className="w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5"
                placeholder="Tax ID"
            />*/}
            <div className="flex space-x-5">
                <div className="flex-1">
                    <InputForm
                        error={errors?.business_id_number}
                        value={data?.business_id_number} onChange={(e) => handelSetData('business_id_number', e.target.value)}
                        type="text"
                        className="w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5"
                        placeholder={labelField.business_id_number}
                    />
                </div>
                <div className="w-32">
                    <DatePicker placeholderText={labelField.business_id_date} className={`w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5 ${errors?.business_license_date ? "border border-red-500" : ""}`} selected={binDate} onChange={(date) => setBinDate(date)} />
                    {
                        errors?.business_license_date && (
                            <span className="text-xs text-red-500 mr-1 mt-2">{errors?.business_license_date}</span>
                        )
                    }
                </div>
            </div>
            <div className="flex space-x-5">
                <div className="flex-1">
                    <InputForm
                        error={errors?.business_license_number}
                        value={data?.business_license_number} onChange={(e) => handelSetData('business_license_number', e.target.value)}
                        type="text"
                        className="w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5"
                        placeholder={labelField.business_license_number}
                    />
                </div>
                <div className="w-32">
                    <DatePicker placeholderText={labelField.business_license_date} className={`w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5 ${errors?.business_license_date ? "border border-red-500" : ""}`} selected={blnDate} onChange={(date) => setBlnDate(date)} />
                    {
                        errors?.business_license_date && (
                            <span className="text-xs text-red-500 mr-1 mt-2">{errors?.business_license_date}</span>
                        )
                    }
                </div>
            </div>
            <InputForm
                error={errors?.address}
                value={data?.address} onChange={(e) => handelSetData('address', e.target.value)}
                type="text"
                className="w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5"
                placeholder={labelField.address}
            />
            <div className="flex space-x-5">
                <div className="flex-1">
                    <InputForm
                        error={errors?.phone}
                        value={data?.phone} onChange={(e) => handelSetData('phone', e.target.value)}
                        type="text"
                        className="w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5"
                        placeholder={labelField.phone}
                    />
                </div>
                <div className="flex-1">
                    <InputForm
                        value={data?.website} onChange={(e) => handelSetData('website', e.target.value)}
                        type="text"
                        className="w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5"
                        placeholder={labelField.website}
                    />
                </div>
            </div>
            <div className="flex justify-end">
                <div className="flex">
                    <button onClick={handleReset} className="py-4 px-7 text-[#1088E4] rounded-lg font-medium">Reset</button>
                    <button onClick={handleSubmit} className="py-4 px-7 bg-[#1088E4] text-white rounded-lg font-semibold">Next</button>
                </div>
            </div>
        </div>
    )
}