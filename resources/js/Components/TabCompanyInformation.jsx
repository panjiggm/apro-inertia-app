import { Inertia } from '@inertiajs/inertia'
import React, { useState } from 'react'
import TabFormCompany from './TabFormCompany'
import TabFormContact from './TabFormContact'
import ValidationErrors from '@/Components/ValidationErrors';
import { usePage } from '@inertiajs/inertia-react'

export default function TabCompanyInformation() {
    const [active, setActive] = useState("company")
    const [data, setData] = useState({})
    const { errors } = usePage().props

    return (
        <div className="border rounded-lg p-5 px-10 bg-white w-[600px] m-auto">
            <ValidationErrors errors={errors} />

            <div className="flex">
                <button disabled onClick={() => setActive("company")} className={`p-4 border-b-4 text-center flex-1 ${active === "company" ? "border-b-[#1088E4] text-[#1088E4]" : "border-b-transparent"}`}>Company Information</button>
                <button disabled onClick={() => setActive("contact")} className={`p-4 border-b-4 text-center flex-1 ${active === "contact" ? "border-b-[#1088E4] text-[#1088E4]" : "border-b-transparent"}`}>Contact Person</button>
            </div>
            <div className="mt-10">
                <div className={`${active === "company" ? "flex" : "hidden"}`}>
                    <TabFormCompany onSubmit={(dataCompany) => {
                        setData({ ...data, ...dataCompany })
                        setActive("contact")
                    }} />
                </div>
                <div className={`${active === "contact" ? "flex" : "hidden"}`}>
                    <TabFormContact onCancel={() => setActive("company")} onSubmit={(dataContact) => {
                        let params = { ...data, ...dataContact }
                        Inertia.post(route('register.account-registration.store'), params)
                    }} />
                </div>
            </div>
        </div>
    )
}



