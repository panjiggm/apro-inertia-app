import SidebarLink from "./SidebarLink";

export default function NavbarAdmin() {
    return (
        <>
            <div className="mt-5 text-center">
                <div className="flex flex-row p-1 px-4 bg-[#F9F9FB] rounded-lg">
                    <div className="flex items-center">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="18"
                            height="18"
                            fill="none"
                            viewBox="0 0 18 18"
                        >
                            <path
                                fill="#2A2A2A"
                                d="M16.275 14.175L13.95 11.85a7.01 7.01 0 01-2.1 2.1l2.325 2.325c.3.3.75.3 1.05 0l1.05-1.05c.3-.3.3-.75 0-1.05z"
                            ></path>
                            <path
                                fill="#5E6278"
                                d="M8.25 15C4.5 15 1.5 12 1.5 8.25s3-6.75 6.75-6.75S15 4.5 15 8.25 12 15 8.25 15zm0-12A5.218 5.218 0 003 8.25a5.218 5.218 0 005.25 5.25 5.218 5.218 0 005.25-5.25A5.218 5.218 0 008.25 3z"
                                opacity="0.3"
                            ></path>
                        </svg>
                    </div>

                    <input
                        type="text"
                        placeholder="Search..."
                        className="w-full border-none bg-[#F9F9FB] text-xs"
                    />
                </div>
            </div>

            <div className="flex justify-center my-3">
                <hr className="w-3/4" />
            </div>

            <ul className="space-y-2 tracking-wide">
                <li>
                    <SidebarLink
                        href={route("admin.dashboard")}
                        active={route().current("admin.dashboard")}
                        image="/images/icon_dashboard.png"
                        label="Dashboard"
                    />
                </li>
                <li>
                    <SidebarLink
                        href={route("admin.clients.index")}
                        active={route().current("admin.clients.*")}
                        image="/images/icon_user.png"
                        label="User List"
                    />
                </li>
                <li>
                    <SidebarLink
                        href={route("admin.database-backup")}
                        active={route().current("admin.database-backup")}
                        image="/images/icon_database.png"
                        label="Database Backup"
                    />
                </li>
                <li>
                    <SidebarLink
                        href={route("admin.packages.index")}
                        active={route().current("admin.packages.*")}
                        image="/images/icon_database.png"
                        label="Packages"
                    />
                </li>
                <li>
                    <SidebarLink
                        href={route("admin.addons.index")}
                        active={route().current("admin.addons.*")}
                        image="/images/icon_database.png"
                        label="Addons"
                    />
                </li>
                <li>
                    <SidebarLink
                        href={route("admin.finance.index")}
                        active={route().current("admin.finance.index")}
                        image="/images/icon_database.png"
                        label="Finance"
                    />
                </li>
            </ul>

            <div className="flex justify-center my-3">
                <hr className="w-3/4" />
            </div>

            <ul className="space-y-2 tracking-wide">
                <li>
                    <SidebarLink
                        href={route("artisan.migrate")}
                        active={route().current("artisan.migrate")}
                        image="/images/icon_database.png"
                        label="Database Migrate"
                    />
                </li>
                <li>
                    <SidebarLink
                        href={route("artisan.migrate-fresh")}
                        active={route().current("artisan.migrate-fresh")}
                        image="/images/icon_database.png"
                        label="Database Migrate Fresh"
                    />
                </li>
                <li>
                    <SidebarLink
                        href={route("artisan.seed")}
                        active={route().current("artisan.seed")}
                        image="/images/icon_database.png"
                        label="Database Seed"
                    />
                </li>
            </ul>

            <div className="flex justify-center my-3">
                <hr className="w-3/4" />
            </div>

            <ul className="space-y-2 tracking-wide">
                <li>
                    <SidebarLink
                        href={route("admin.settings.index")}
                        active={route().current("admin.settings.*")}
                        image="/images/icons/settings.png"
                        label="Settings"
                    />
                </li>
                <li>
                    <SidebarLink
                        href={route("admin.settings.index")}
                        active={route().current("admin.settings.*")}
                        image="/images/icons/pbc.png"
                        label="Report"
                    />
                </li>
            </ul>
        </>
    );
}
