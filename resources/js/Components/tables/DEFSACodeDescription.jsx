import { moveCursorToEnd } from "@/utlis/common";
import React, { useRef, useCallback, useEffect } from "react";

// DataEditor FSA Code & Description
const DEFSACodeDescription = ({ onChange, cell }) => {
    const inputRef = useRef(null);

    const handleChange = useCallback(
        (event) => {
            onChange({ ...cell, value: event.target.value });
        },
        [onChange, cell]
    );

    useEffect(() => {
        if (inputRef.current) {
            moveCursorToEnd(inputRef.current);
        }
    }, [inputRef]);

    const value = cell?.value ?? "";

    return (
        <div className="Spreadsheet__data-editor whitespace-normal">
            <select ref={inputRef} onChange={handleChange}
                value={value}
                autoFocus>
                <option value="">Please Select...</option>
                <option value="1101 Cash and Cash Equivalent">1101 Cash and Cash Equivalent</option>
                <option value="1102 Time Deposits">1102 Time Deposits</option>
                <option value="1103 Derivative Assets">1103 Derivative Assets</option>
                <option value="1104 Invertment Securities Avaiable for Sale">1104 Invertment Securities Avaiable for Sale</option>
                <option value="1105 Trade Receivable - Current">1105 Trade Receivable - Current</option>
            </select>
        </div>
    );
};
export default DEFSACodeDescription