import { moveCursorToEnd } from "@/utlis/common";
import React, { useRef, useCallback, useEffect } from "react";

// DataEditor Level
const DESign = ({ onChange, cell }) => {
    const inputRef = useRef(null);

    const handleChange = useCallback(
        (event) => {
            onChange({ ...cell, value: event.target.value });
        },
        [onChange, cell]
    );

    useEffect(() => {
        if (inputRef.current) {
            moveCursorToEnd(inputRef.current);
        }
    }, [inputRef]);

    const value = cell?.value ?? "";

    return (
        <div className="Spreadsheet__data-editor whitespace-normal">
            <select ref={inputRef} onChange={handleChange}
                value={value}
                autoFocus>
                <option value="">Please Level...</option>
                <option value="Debit">Debit</option>
                <option value="Kredit">Kredit</option>
                <option value="N/A">N/A</option>
            </select>
        </div>
    );
};
export default DESign