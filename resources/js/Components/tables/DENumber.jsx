import { moveCursorToEnd } from "@/utlis/common";
import React, { useRef, useCallback, useEffect } from "react";

// DataEditor Number
const DENumber = ({ onChange, cell }) => {
    const inputRef = useRef(null);

    const handleChange = useCallback(
        (event) => {
            onChange({ ...cell, value: event.target.value });
        },
        [onChange, cell]
    );

    useEffect(() => {
        if (inputRef.current) {
            moveCursorToEnd(inputRef.current);
        }
    }, [inputRef]);

    const value = cell?.value ?? "";

    return (
        <div className="Spreadsheet__data-editor whitespace-normal">
            <input
                ref={inputRef}
                type="text"
                onKeyPress={(event) => {
                    if (!/[0-9]/.test(event.key)) {
                      event.preventDefault();
                    }
                  }}
                onChange={handleChange}
                value={value}
                autoFocus
            />
        </div>
    );
};
export default DENumber