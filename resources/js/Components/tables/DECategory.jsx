import { moveCursorToEnd } from "@/utlis/common";
import React, { useRef, useCallback, useEffect } from "react";

// DataEditor Category
const DECategory = ({ onChange, cell }) => {
    const inputRef = useRef(null);

    const handleChange = useCallback(
        (event) => {
            onChange({ ...cell, value: event.target.value });
        },
        [onChange, cell]
    );

    useEffect(() => {
        if (inputRef.current) {
            moveCursorToEnd(inputRef.current);
        }
    }, [inputRef]);

    const value = cell?.value ?? "";

    return (
        <div className="Spreadsheet__data-editor whitespace-normal">
            <select ref={inputRef} onChange={handleChange}
                value={value}
                autoFocus>
                <option value="">Please Category...</option>
                <option value="FSA">FSA</option>
                <option value="GA">GA</option>
            </select>
        </div>
    );
};
export default DECategory