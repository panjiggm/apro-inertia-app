import { getComputedValue } from "@/utlis/common";
import React from "react";

export const TRUE_TEXT = "TRUE";
export const FALSE_TEXT = "FALSE";

/** DataView Curency*/
const DVCurency = ({
    cell,
    formulaParser,
}) => {
    const value = getComputedValue({ cell, formulaParser });
    return typeof value === "boolean" ? (
        <span className="Spreadsheet__data-viewer Spreadsheet__data-viewer--boolean">
           {convertBooleanToText(value)}
        </span>
    ) : (
        <span className="Spreadsheet__data-viewer">{Intl.NumberFormat("id-ID", {
            style: "currency",
            currency: "IDR"
        }).format(value)}</span>
    );
};

export default DVCurency;

export function convertBooleanToText(value) {
    return value ? TRUE_TEXT : FALSE_TEXT;
}