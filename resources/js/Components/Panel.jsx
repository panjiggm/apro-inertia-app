import { Link } from "@inertiajs/inertia-react";

function Panel({ children }) {
    return (
        <div className="border  w-full bg-white">
            {children}
        </div>
    );
}

function PanelHeader({ children }) {
    return (
        <div className="flex w-full border-b items-center p-4 ">{children}</div>
    );
}

function PanelTitle({ title, subtitle = undefined }) {
    return (
        <div className="flex-1">
            <h2 className="text-lg font-semibold text-[#2A2A2A] ">{title}</h2>
            {subtitle && (
                <p className="text-[#464646cc] text-xs font-medium">
                    {subtitle}
                </p>
            )}
        </div>
    );
}

function PanelBack({ href = undefined }) {
    return (
        <Link href={href} className="p-2 mr-6">
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="8"
                height="13"
                fill="none"
                viewBox="0 0 8 13"
            >
                <path
                    fill="#323232"
                    d="M7.297.387a1.119 1.119 0 00-1.642 0L.628 5.821a1 1 0 000 1.358l5.027 5.434a1.118 1.118 0 101.642-1.52L3.362 6.84a.5.5 0 010-.68l3.935-4.253a1.119 1.119 0 000-1.52z"
                ></path>
            </svg>
        </Link>
    );
}

function PanelContent({ children }) {
    return <div className="flex flex-col">{children}</div>;
}

function PanelBreadcrumb({ children }) {
    return (
        <div className="bg-[##F9F9FB] px-8 py-5 flex space-x-2 border-t border-[#E6E6E8]">
            {children}
        </div>
    );
}

function PanelBreadcrumbItem({ text, href = undefined, current = false }) {
    return (
        <div className="flex space-x-2 items-center">
            <Link href={href} className="text-[#2A2A2ACC] text-xs">
                {text}
            </Link>
            {!current && (
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="6"
                    height="11"
                    fill="none"
                    viewBox="0 0 6 11"
                >
                    <path
                        fill="#2A2A2A"
                        d="M.555 1.105a.82.82 0 011.176 0l3.592 3.698a1 1 0 010 1.394L1.731 9.895A.82.82 0 01.555 8.752l2.82-2.904a.5.5 0 000-.696L.556 2.248a.82.82 0 010-1.143z"
                    ></path>
                </svg>
            )}
        </div>
    );
}

function PanelSearch({placeholder}) {
    return (
        <div className="flex flex-row p-1 px-4 bg-[#F9F9FB] rounded-lg">
            <div className="flex items-center">
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="18"
                    height="18"
                    fill="none"
                    viewBox="0 0 18 18"
                >
                    <path
                        fill="#2A2A2A"
                        d="M16.275 14.175L13.95 11.85a7.01 7.01 0 01-2.1 2.1l2.325 2.325c.3.3.75.3 1.05 0l1.05-1.05c.3-.3.3-.75 0-1.05z"
                    ></path>
                    <path
                        fill="#5E6278"
                        d="M8.25 15C4.5 15 1.5 12 1.5 8.25s3-6.75 6.75-6.75S15 4.5 15 8.25 12 15 8.25 15zm0-12A5.218 5.218 0 003 8.25a5.218 5.218 0 005.25 5.25 5.218 5.218 0 005.25-5.25A5.218 5.218 0 008.25 3z"
                        opacity="0.3"
                    ></path>
                </svg>
            </div>

            <input
                type="text"
                placeholder={placeholder}
                className="w-full border-none bg-[#F9F9FB] text-xs"
            />
        </div>
    );
}

Panel.Title = PanelTitle;
Panel.Header = PanelHeader;
Panel.Breadcrumb = PanelBreadcrumb;
Panel.BreadcrumbItem = PanelBreadcrumbItem;
Panel.Content = PanelContent;
Panel.Back = PanelBack;
Panel.Search = PanelSearch;

export default Panel;
