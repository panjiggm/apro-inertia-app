import MenuSettings from "./MenuSettings";

export default function AdminKAPMenuSettings({ full = false }) {
    return (
        <MenuSettings full={full}>
            <MenuSettings.Header title="General" />
            <MenuSettings.Item borderBottom={true} title="About" href={route('company.settings.about')} active={route().current("company.settings.about")} roundedTop={true} />
            <MenuSettings.Item borderBottom={true} title="Software Update" href={route('company.settings.update')} active={route().current("company.settings.update")} />
            <MenuSettings.Item borderBottom={true} title="Storage" href={route('company.settings.storage')} active={route().current("company.settings.storage")} />
            <MenuSettings.Item borderBottom={true} title="Date & Time" href={route('company.settings.date')} active={route().current("company.settings.date")} />
            <MenuSettings.Item borderBottom={true} title="Font" href={route('company.settings.fonts')} active={route().current("company.settings.fonts.*") || route().current("company.settings.fonts")} />
            <MenuSettings.Item borderBottom={true} title="Language & Region" href={route('company.settings.language')} active={route().current("company.settings.language.*") || route().current("company.settings.language")} />
            <MenuSettings.Item title="Legal & Regulatory" href={route('company.settings.legal')} active={route().current("company.settings.legal")} roundedBottom={true} />

            <MenuSettings.Header title="CONTROL CENTER" />
            <MenuSettings.Item borderBottom={true} title="Materiality" href={route('company.settings.materiality')} active={route().current("company.settings.materiality")} roundedTop={true} />
            <MenuSettings.Item borderBottom={true} title="Risk Material Misstatement" href={route('company.settings.risk')} active={route().current("company.settings.risk")} />
            <MenuSettings.Item borderBottom={true} title="Likelihood & Magnitude" href={route('company.settings.likelihood')} active={route().current("company.settings.likelihood")} />
            <MenuSettings.Item title="APRO Menu Setting" href={route('company.settings.menu')} active={route().current("company.settings.menu")} roundedBottom={true} />

            <MenuSettings.Space />
            <MenuSettings.Item title="Company Profile" href={route('company.settings.company')} active={route().current("company.settings.company.*") || route().current("company.settings.company")} roundedBottom={true} roundedTop={true} />

            <MenuSettings.Space />
            <MenuSettings.Item borderBottom={true} title="Display" href={route('company.settings.display')} active={route().current("company.settings.display.*") || route().current("company.settings.display")} roundedTop={true} />
            <MenuSettings.Item title="Password" href={route('company.settings.change-password')} active={route().current("company.settings.change-password")} roundedTop={true} />

            <MenuSettings.Space />
            <MenuSettings.Signout borderBottom={false} borderTop={false} href={route('logout')} />
        </MenuSettings>
    )
}