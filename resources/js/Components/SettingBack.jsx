import { Link } from "@inertiajs/inertia-react";

export default function SettingBack({ title, href = undefined }) {
    return (
        <Link href={href} className="text-lg pl-3 text-[#1989E9] flex font-semibold gap-2 items-center justify-center space-x-2">
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="8"
                height="13"
                fill="none"
                viewBox="0 0 8 13"
            >
                <path
                    fill="#1989E9"
                    d="M7.297.387a1.119 1.119 0 00-1.642 0L.628 5.821a1 1 0 000 1.358l5.027 5.434a1.118 1.118 0 101.642-1.52L3.362 6.84a.5.5 0 010-.68l3.935-4.253a1.119 1.119 0 000-1.52z"
                ></path>
            </svg>
            <span>{title}</span>
        </Link>
    );
}
