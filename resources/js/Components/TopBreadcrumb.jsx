export default function TopBreadcrumb({ home = "Dashboard", before = "", current = "" }) {
    return (
        <h5 className="text-lg pl-3 text-[#2A2A2A] font-semibold flex items-center justify-center space-x-2">
            <span>{home}</span>

            {
                before !== "" && (
                    <>
                        <span className="w-1  h-1 inline-block rounded-full bg-[#46464633]" />
                        <span className="text-xs text-[#464646cb]">{before}</span>
                    </>
                )
            }
            <span className="w-1  h-1 inline-block rounded-full bg-[#46464633]" />
            <span className="text-xs text-[#464646cb]">{current !== "" ? current : null}</span>
        </h5>
    );
}
