import { formatBytes } from '@/helper';
import useOnClickOutside from '@/hooks/useOnClickOutside';
import React, { useCallback, useState, useRef } from 'react'
import { useDropzone } from 'react-dropzone'

export default function MyDropzone({ onChange }) {

    const [positionX, setPositionX] = useState(0);
    const [positionY, setPositionY] = useState(0);
    const [modal, setModal] = useState(false);

    const ref = useRef();
    // Call hook passing in the ref and a function to call on outside click
    useOnClickOutside(ref, () => setModal(false));

    const onDrop = useCallback(acceptedFiles => {
        // Do something with the files
        onChange(acceptedFiles)
    }, [])

    const { acceptedFiles, getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop })

    const handleClick = (e) => {
        if (e.nativeEvent.which === 3) {
            e.preventDefault()
            // console.log("Right click");
            setPositionX(e.pageX)
            setPositionY(e.pageY)
            const xCoordinate = e.pageX;
            const yCoordinate = e.pageY;
            setModal(true);
            // console.log(`x: ${xCoordinate}, y: ${yCoordinate}`)
        }
    };

    const Modal = ({ id }) => {
        return (
            <div className={`w-[200px] bg-white shadow-md rounded-lg p-4 fixed ${modal ? 'flex' : 'hidden'}`} style={{
                left: positionX,
                top: positionY,
            }}>
                <ul>
                    <li className="flex items-center cursor-pointer w-[174px] gap-4 p-4 hover:bg-[#F4F4F6] text-[#464646]">

                        <img src="/images/download.png" alt="download" />
                        <span>Download</span>
                    </li>
                    <li className="flex items-center cursor-pointer w-[174px] gap-4 p-4 hover:bg-[#F4F4F6] text-[#464646]">
                        <img src="/images/pencil.png" alt="download" />
                        <span>Ganti Nama</span>
                    </li>
                    <li></li>
                    <li className="flex items-center cursor-pointer w-[174px] gap-4 p-4 hover:bg-[#F4F4F6] text-[#464646]">
                        <img src="/images/trash.png" alt="download" />
                        <span>Hapus</span>
                    </li>
                </ul>
            </div>
        )

    }

    const files = acceptedFiles.map(file => (
        <li ref={ref} onContextMenu={handleClick} className="flex flex-col max-w-[220px] gap-4 border rounded-lg border-gray-100 items-center justify-center py-6 px-12" key={file.name}>
            <div className="">
                <img src="/images/icon_pdf.png" alt={file.name} />
            </div>
            <div className="flex gap-2 flex-col">
                <p className="truncate max-w-[100px] inline-block">{file.name}</p>
                <div className="gap-2 flex">
                    <p>{formatBytes(file.size)}</p>
                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="10" cy="10" r="9.0625" fill="#4BAE4F" />
                        <path fillRule="evenodd" clipRule="evenodd" d="M4.98976 9.73826C4.75382 9.9742 4.75382 10.3567 4.98977 10.5927L7.07853 12.6815C7.08079 12.6838 7.08308 12.6861 7.08538 12.6884L8.45772 14.0607C8.53055 14.1336 8.61735 14.1839 8.70949 14.2118C8.91589 14.2743 9.14914 14.2239 9.3123 14.0608L10.6846 12.6885C10.6878 12.6853 10.6909 12.6821 10.6939 12.6789L15.269 8.10391C15.5049 7.86797 15.5049 7.48543 15.269 7.24949L13.8966 5.87715C13.6607 5.64121 13.2782 5.64121 13.0422 5.87715L8.88498 10.0344L7.21652 8.36593C6.98058 8.12998 6.59804 8.12998 6.3621 8.36593L4.98976 9.73826Z" fill="white" />
                    </svg>
                </div>
            </div>
        </li>
    ));

    return (
        <>
            {
                !files?.length > 0 && (
                    <div className="my-0 flex gap-3 justify-center mt-12 border-2 border-dashed bg-[#F9FCFF] p-20" {...getRootProps()}>
                        <input {...getInputProps()} />
                        {
                            isDragActive ?
                                <p>Drop the files here ...</p> :
                                <div className="text-center flex flex-col justify-center items-center gap-3 text-[#464E5F]">
                                    <p>Drop Your Document Here</p>
                                    <span className="text-sm">OR</span>
                                    <div className="bg-[#EBEEF3]  rounded-lg py-2 px-7 text-center hover:cursor-pointer">Select File</div>
                                </div>

                        }
                    </div>
                )
            }
            <div className="flex justify-center mt-12 ">
                <ul className="flex gap-3 mx-0 my-auto flex-wrap">
                    {files}
                </ul>
            </div>
            <Modal />
        </>
    )
}