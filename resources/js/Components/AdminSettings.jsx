import MenuSettings from "./MenuSettings";

export default function AdminSettings({ full = false }) {
    return (
        <MenuSettings full={full}>
            <MenuSettings.Header title="General" />
            <MenuSettings.Item title="About" borderBottom={true} borderTop={false} href={route('admin.settings.about')} active={route().current("admin.settings.about")} roundedTop={true} />
            <MenuSettings.Item title="Storage" borderBottom={true} borderTop={true} href={route('admin.settings.storage')} active={route().current("admin.settings.storage")} />

            <MenuSettings.Item title="Legal & Regulatory" borderBottom={false} borderTop={true} href={route('admin.settings.legal')} active={route().current("admin.settings.legal")} roundedBottom={true} />

            <MenuSettings.Header title="CONTROL CENTER" />
            <MenuSettings.Item title="Materiality" borderBottom={true} borderTop={false} href={route('admin.settings.materiality')} active={route().current("admin.settings.materiality")} roundedBottom={true} roundedTop={true} />
            <MenuSettings.Item title="Risk Material Misstatement" borderBottom={true} borderTop={true} href={route('admin.settings.risk')} active={route().current("admin.settings.risk")} />
            <MenuSettings.Item title="Likelihood & Magnitude" borderBottom={true} borderTop={true} href={route('admin.settings.likelihood')} active={route().current("admin.settings.likelihood")} />
            <MenuSettings.Item title="Control Risk" borderBottom={true} borderTop={true} href={route('admin.settings.control-risk')} active={route().current("admin.settings.control-risk")} />
            <MenuSettings.Item title="Confidence Level" borderBottom={true} borderTop={true} href={route('admin.settings.confidence-level')} active={route().current("admin.settings.confidence-level")} />
            <MenuSettings.Item title="Risk of Incorrect Acceptance (RIA)" borderBottom={true} borderTop={true} href={route('admin.settings.risk-of-incorrect-acceptance')} active={route().current('admin.settings.risk-of-incorrect-acceptance')} />
            <MenuSettings.Item title="User Access Control" borderBottom={true} borderTop={true} href={route('admin.settings.user-access-control')} active={route().current('admin.settings.user-access-control')} />
            <MenuSettings.Item title="APRO Menu Setting" roundedBottom={true} borderBottom={true} borderTop={true} href={route("admin.settings.menu-settings")} active={route().current('admin.settings.menu-settings')} />
            <MenuSettings.Item title="Sign Off Setting" borderBottom={false} borderTop={true} href={route('admin.settings.sign-off-setting')} active={route('admin.settings.sign-off-setting')} />

            <MenuSettings.Space />
            <MenuSettings.Item title="Password" borderBottom={false} borderTop={false} href={route('admin.settings.change-password')} active={route('admin.settings.change-password')} roundedBottom={true} />

            <MenuSettings.Space />
            <MenuSettings.Signout borderBottom={false} borderTop={false} href={route('logout') } />
        </MenuSettings>
    )
}