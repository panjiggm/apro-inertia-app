import {Menu, Transition} from '@headlessui/react'
import {Link} from '@inertiajs/inertia-react'
import {Fragment, useEffect, useRef, useState} from 'react'

export default function ButtonSignIn({text = 'Sign in'}) {
  return (
    <div>
      <Menu as="div" className="relative inline-block text-left">
        <div>
          <Menu.Button className="text-[16px] px-[16px] py-[16px] font-medium bg-gradient-to-b rounded-lg from-[#2EA6FF] to-[#0C7CD1] text-white flex gap-3">
            {text}
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-6 h-6"
            >
              <path strokeLinecap="round" strokeLinejoin="round" d="M19.5 8.25l-7.5 7.5-7.5-7.5" />
            </svg>
          </Menu.Button>
        </div>
        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <Menu.Items className="absolute right-0 mt-2 w-56 origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
            <div className="px-1 py-1 ">
              <Menu.Item>
                {({active}) => (
                  <Link
                    href={route('company.login')}
                    className={`${
                      active ? 'bg-[#2EA6FF] text-white' : 'text-gray-900'
                    } group flex w-full items-center rounded-md px-2 py-2 text-sm`}
                  >
                    Admin
                  </Link>
                )}
              </Menu.Item>
              <Menu.Item>
                {({active}) => (
                  <Link
                    href={route('login')}
                    className={`${
                      active ? 'bg-[#2EA6FF] text-white' : 'text-gray-900'
                    } group flex w-full items-center rounded-md px-2 py-2 text-sm`}
                  >
                    Auditor
                  </Link>
                )}
              </Menu.Item>
            </div>
          </Menu.Items>
        </Transition>
      </Menu>
    </div>
  )
}
