export default function CardIcon({icon, title1, title2, descriptions}) {
  return (
    <div className="flex-1 shadow-2xl shadow-[rgba(92,55,212,0.22)] rounded-[32px] px-8 py-6 flex flex-col">
      {icon}
      <h3 className="text-[25px] font-semibold text-[#222222] mt-8">{title1}</h3>
      <h3 className="text-[25px] font-semibold text-[#222222] mb-4">{title2}</h3>
      <p className="text-[#22222280]">{descriptions}</p>
    </div>
  )
}
