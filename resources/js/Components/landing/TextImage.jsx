export default function TextImage({imagePosition = 'right', ...props}) {
  return (
    <div className="flex mb-9">
      {imagePosition === 'left' ? <RContent {...props} /> : <RImage {...props} imagePosition={imagePosition} />}
      {imagePosition === 'left' ? <RImage {...props} imagePosition={imagePosition} /> : <RContent {...props} />}
    </div>
  )
}

const RContent = ({title, descriptions, labelButton = "Start your free trial"}) => {
  return (
    <div className="flex-1 ">
      <div className="flex justify-center flex-col h-full max-w-[410px] mx-auto">
        <h4 className="text-[#222222] text-[25px] font-semibold">{title}</h4>
        <p className="text-[19px] text-[#22222280] py-4">{descriptions}</p>
        <a href="#" className="text-[#2EA6FF]">
          <div className="flex items-center gap-2">
            <span>{labelButton}</span>
            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M4.47206 12.4711C4.21171 12.7314 3.7896 12.7314 3.52925 12.4711C3.2689 12.2107 3.2689 11.7886 3.52925 11.5283L11.5292 3.52827C11.7896 3.26792 12.2117 3.26792 12.4721 3.52827C12.7324 3.78862 12.7324 4.21073 12.4721 4.47108L4.47206 12.4711Z"
                fill="#2EA6FF"
              />
              <path
                d="M5.33464 3.33301C4.96645 3.33301 4.66797 3.63148 4.66797 3.99967C4.66797 4.36786 4.96645 4.66634 5.33464 4.66634H11.3346V10.6663C11.3346 11.0345 11.6331 11.333 12.0013 11.333C12.3695 11.333 12.668 11.0345 12.668 10.6663V3.99967C12.668 3.63148 12.3695 3.33301 12.0013 3.33301H5.33464Z"
                fill="#2EA6FF"
              />
            </svg>
          </div>
        </a>
      </div>
    </div>
  )
}

const RImage = ({image, imagePosition}) => {
  return (
    <div className={`flex-1 flex ${imagePosition === 'left' ? 'justify-end' : 'justify-start'} items-center`}>
      <img src={image} alt="title" className="inline-block" />
    </div>
  )
}
