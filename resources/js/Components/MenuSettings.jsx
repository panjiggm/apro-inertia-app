import { Link } from "@inertiajs/inertia-react"

function MenuSettings({ full = false, children }) {
    return (
        <div className={`${full ? 'w-full' : 'w-96'} h-screen border-r border-[#ECEFF7]`}>
            <ul>
                {children}
            </ul>
        </div>
    )
}

function LinkSetting({ title, active = false, href = undefined, borderTop = false, borderBottom = false, roundedTop = false, roundedBottom = false }) {
    return (
        <li>
            <Link href={href} className={`mx-4 flex bg-white ${roundedTop && "rounded-t-lg"} ${roundedBottom && "rounded-b-lg"} overflow-hidden`}>
                <div className={`${borderTop && "border-t"} ${borderBottom && "border-b"} border-[#ECEFF7] flex w-full p-4 pl-0 justify-between items-center text-[#464646] bg-white ml-5 font-medium text-xs `}>
                    <span>{title}</span>
                    <svg width="6" height="12" viewBox="0 0 6 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0.50297 1.20128C0.843237 0.817479 1.44248 0.817479 1.78274 1.20128L5.41184 5.29463C5.74749 5.67321 5.74749 6.24284 5.41184 6.62143L1.78275 10.7148C1.44248 11.0986 0.84324 11.0986 0.502972 10.7148C0.215941 10.391 0.215941 9.9039 0.502972 9.58015L3.42021 6.28973C3.58803 6.10043 3.58803 5.81562 3.42021 5.62633L0.50297 2.3359C0.215939 2.01215 0.215939 1.52503 0.50297 1.20128Z" fill="#464646" fill-opacity="0.5" />
                    </svg>
                </div>

            </Link>
        </li>
    )
}

function HeaderSetting({ title }) {
    return (
        <li>
            <h2 className="text-[#46464665] pl-9 p-4 uppercase mt-2 text-center">{title}</h2>
        </li>
    )
}

function SpaceSetting() {
    return (
        <li>
            <div className="h-8 w-full" />
        </li>
    )
}

function ButtonSignout({href = undefined}) {
    return (
        <li>
            <Link href={href} className={`mx-4 flex bg-white rounded overflow-hidden`}>
                <div className={`border-y border-[#ECEFF7] flex w-full p-4 pl-0 justify-between items-center bg-white ml-5 font-medium text-xs opacity-60`}>
                    <span className="text-[rgba(11,153,235,0.8)]">Signout</span>
                </div>
            </Link>
        </li>
    )
}

MenuSettings.Item = LinkSetting;
MenuSettings.Header = HeaderSetting;
MenuSettings.Space = SpaceSetting;
MenuSettings.Signout = ButtonSignout;

export default MenuSettings;