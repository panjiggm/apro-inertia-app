import MenuSettings from "./MenuSettings";

export default function AuditorMenuSettings({ full = false }) {
    return (
        <MenuSettings full={full}>
            <MenuSettings.Header title="General" />
            <MenuSettings.Item borderBottom={true} title="About" href={route('auditor.settings.about')} active={route().current("auditor.settings.about")} />
            <MenuSettings.Item borderBottom={true} title="Software Update" href={route('auditor.settings.update')} active={route().current("auditor.settings.update")} />
            <MenuSettings.Item borderBottom={true} title="Storage" href={route('auditor.settings.storage')} active={route().current("auditor.settings.storage")} />
            <MenuSettings.Item borderBottom={true} title="Date & Time" href={route('auditor.settings.date')} active={route().current("auditor.settings.date")} />
            <MenuSettings.Item borderBottom={true} title="Font" href={route('auditor.settings.fonts')} active={route().current("auditor.settings.fonts")} />
            <MenuSettings.Item borderBottom={true} title="Language & Region" href={route('auditor.settings.language')} active={route().current("auditor.settings.language")} />
            <MenuSettings.Item title="Legal & Regulatory" href={route('auditor.settings.legal')} active={route().current("auditor.settings.legal")} />

            <MenuSettings.Space />
            <MenuSettings.Item borderBottom={true} title="Display" href={route('auditor.settings.display')} active={route().current("auditor.settings.display")} />
            <MenuSettings.Item title="Password" href={route('auditor.settings.change-password')} active={route().current("auditor.settings.change-password")} />

            <MenuSettings.Space />
            <MenuSettings.Signout borderBottom={false} borderTop={false} href={route('logout') } />
        </MenuSettings>
    )
}