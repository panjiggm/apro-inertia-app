import SidebarLink from "./SidebarLink";

export default function NavbarAuditor({ sidebarSmall }) {
    return (
        <div className="mt-8">
            <ul className="space-y-2 tracking-wide">
                <li>
                    <SidebarLink
                        sidebarSmall={sidebarSmall}
                        href={route("auditor.dashboard")}
                        active={route().current("auditor.dashboard")}
                        image="/images/icons/dashboard.png"
                        label="Dashboard"
                    />
                </li>
                <li>
                    <SidebarLink
                        sidebarSmall={sidebarSmall}
                        href={route("auditor.projects.index")}
                        active={route().current("auditor.projects.*")}
                        image="/images/icons/project.png"
                        label="Project"
                    />
                </li>
            </ul>

            <div className="flex justify-center my-3">
                <hr className="w-3/4" />
            </div>

            <ul className="space-y-2 tracking-wide">
                <li>
                    <SidebarLink
                        sidebarSmall={sidebarSmall}
                        href={route("auditor.dashboard")}
                        active={route().current("auditor.dashboard")}
                        image="/images/icons/incomplete.png"
                        label="Incomplete Form"
                        badge="56"
                        badgeColor="#FFC107"
                    />
                </li>
                <li>
                    <SidebarLink
                        sidebarSmall={sidebarSmall}
                        href={route("auditor.dashboard")}
                        active={route().current("auditor.dashboard")}
                        image="/images/icons/ready_review.png"
                        label="Ready for Review"
                        badge="2"
                        badgeColor="#05E200"
                    />
                </li>
                <li>
                    <SidebarLink
                        sidebarSmall={sidebarSmall}
                        href={route("auditor.dashboard")}
                        active={route().current("auditor.dashboard")}
                        image="/images/icons/feedback.png"
                        label="Feedback"
                        badge="2"
                    />
                </li>
                <li>
                    <SidebarLink
                        sidebarSmall={sidebarSmall}
                        href={route("auditor.settings.about")}
                        active={route().current("auditor.settings.*")}
                        image="/images/icons/pbc.png"
                        label="Client Communication"
                        badge="3"
                        badgeColor="#05E200"
                    />
                </li>
            </ul>

            <div className="flex justify-center my-3">
                <hr className="w-3/4" />
            </div>

            <ul className="space-y-2 tracking-wide">
                <li>
                    <SidebarLink
                        sidebarSmall={sidebarSmall}
                        href={route("auditor.settings.about")}
                        active={route().current("auditor.settings.*")}
                        image="/images/icons/timesheet.png"
                        label="Timesheet"
                    />
                </li>
                <li>
                    <SidebarLink
                        sidebarSmall={sidebarSmall}
                        href={route("auditor.settings.index")}
                        active={route().current("auditor.settings.*")}
                        image="/images/icons/settings.png"
                        label="Setting"
                    />
                </li>
                <li>
                    <SidebarLink
                        sidebarSmall={sidebarSmall}
                        href={route("auditor.dashboard")}
                        active={route().current("auditor.dashboard")}
                        image="/images/icons/support.png"
                        label="IT Support"
                    />
                </li>
            </ul>
        </div>
    );
}
