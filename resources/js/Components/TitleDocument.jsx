export default function TitleDocument({ title, description }) {
    return (
        <div className="text-center max-w-[520px] my-0 mx-auto">
            <h3 className="text-zinc-800 font-semibold">{title}</h3>
            <p className="text-slate-400 mt-4 text-sm">{description}</p>
        </div>
    )
}