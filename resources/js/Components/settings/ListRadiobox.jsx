import { RadioGroup } from '@headlessui/react'

const ListRadiobox = ({ selected, setSelected, data }) => {
    return (
        <div className="w-full">
            <RadioGroup value={selected} onChange={setSelected}>
                <div>
                    {data.map((plan) => (
                        <RadioGroup.Option
                            key={plan.name}
                            value={plan}
                            className={() =>
                                `bg-white relative flex cursor-pointer px-5 py-4 border-b focus:outline-none`
                            }
                        >
                            {({ active, checked }) => (
                                <>
                                    <div className="flex w-full items-center justify-between">
                                        <div className="flex items-center">
                                            <div className="text-sm">
                                                <RadioGroup.Label
                                                    as="p"
                                                    className={`text-xs text-[#333]`}
                                                >
                                                    {plan.name}
                                                </RadioGroup.Label>
                                            </div>
                                        </div>
                                        {checked && (
                                            <div className="shrink-0 text-black">
                                                <CheckIcon className="h-4 w-4 bg-[#157BCA] rounded-full" />
                                            </div>
                                        )}
                                    </div>
                                </>
                            )}
                        </RadioGroup.Option>
                    ))}
                </div>
            </RadioGroup>
        </div>
    )
}

const CheckIcon = (props) => {
    return (
        <svg viewBox="0 0 24 24" fill="none" {...props}>
            <circle cx={12} cy={12} r={12} fill="#fff" opacity="0.2" />
            <path
                d="M7 13l3 3 7-7"
                stroke="#fff"
                strokeWidth={1.5}
                strokeLinecap="round"
                strokeLinejoin="round"
            />
        </svg>
    )
}


export default ListRadiobox