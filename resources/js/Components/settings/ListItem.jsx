const ListItem = ({ title, value, borderTop = false, borderBottom = true, roundedTop = false, roundedBottom = false }) => {
    return (
        <li className={`bg-white  ${roundedTop && "rounded-t-lg"} ${roundedBottom && "rounded-b-lg"} overflow-hidden`}>
            <div className={`text-xs p-4 pl-0 ml-5 flex justify-between items-center ${borderTop && "border-t"} ${borderBottom && "border-b"} text-[#464646]`}>
                <span>{title}</span>
                <span>{value}</span>
            </div>
        </li>
    )
}

export default ListItem

