import { Link } from "@inertiajs/inertia-react"

const ListItemLink = ({ title, value, href = undefined, borderTop = false, borderBottom = true, roundedTop = false, roundedBottom = false }) => {
    return (
        <li className={`bg-white  ${roundedTop && "rounded-t-lg"} ${roundedBottom && "rounded-b-lg"} overflow-hidden`}>
            <Link href={href} className={`text-xs p-4 pl-0 ml-5 flex justify-between items-center ${borderTop && "border-t"} ${borderBottom && "border-b"} text-[#464646]`}>
                <span>{title}</span>
                <div className="flex gap-2 items-center">
                    <span>{value}</span>
                    <svg width="6" height="12" viewBox="0 0 6 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0.468222 0.817492C0.821057 0.392638 1.47303 0.392638 1.82587 0.817493L5.49126 5.23104C5.79886 5.60143 5.79886 6.13843 5.49126 6.50882L1.82587 10.9224C1.47304 11.3472 0.821059 11.3472 0.468224 10.9224C0.196796 10.5955 0.196796 10.1217 0.468224 9.79486L3.46253 6.18937C3.61633 6.00418 3.61633 5.73568 3.46253 5.55048L0.468222 1.945C0.196795 1.61817 0.196794 1.14432 0.468222 0.817492Z" fill="#464646" fill-opacity="0.5" />
                    </svg>
                </div>
            </Link>
        </li>
    )
}

export default ListItemLink

