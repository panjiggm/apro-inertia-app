export default function HeaderTable({ title, badge = null, textButton = "", onClick }) {
    return (
        <div className="flex justify-between items-center mb-7 px-8 pt-8">
            <h2 className="text-lg font-semibold text-[#2A2A2A] flex items-center gap-4">{title}
                {
                    badge !== null && (
                        <span className="text-[9px] text-white bg-[#FF5050] flex items-center justify-center font-bold rounded-full w-5 h-5">{badge}</span>
                    )
                }
            </h2>
            {
                textButton && (
                    <button
                        onClick={onClick}
                        className="flex items-center justify-between p-3 border border-[#E2E2E4] rounded-lg"
                        type="button"
                    >
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="8"
                            height="8"
                            fill="none"
                            viewBox="0 0 8 8"
                        >
                            <path
                                fill="#9FB3C5"
                                fillRule="evenodd"
                                d="M3.8 0a.8.8 0 00-.8.8V3H.8a.8.8 0 00-.8.8v.4a.8.8 0 00.8.8H3v2.2a.8.8 0 00.8.8h.4a.8.8 0 00.8-.8V5h2.2a.8.8 0 00.8-.8v-.4a.8.8 0 00-.8-.8H5V.8a.8.8 0 00-.8-.8h-.4z"
                                clipRule="evenodd"
                            ></path>
                        </svg>
                        <span className="text-xs text-[#9FB3C5] ml-2">
                            {textButton}
                        </span>
                    </button>
                )
            }
        </div>
    );
}
