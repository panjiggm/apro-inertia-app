import { Tab } from "@headlessui/react";
import InputForm from "./InputForm";
import DatePicker from "react-datepicker";
import { useState } from "react";

import "react-datepicker/dist/react-datepicker.css";
import InputSelect from "../InputSelect";
import { useForm } from "@inertiajs/inertia-react";
import moment from "moment";

function classNames(...classes) {
    return classes.filter(Boolean).join(" ");
}

const options = [
    {
        label: "Status",
        value: ""
    },
    {
        label: "Active",
        value: "Active"
    },
    {
        label: "Inactive",
        value: "Inactive"
    },
]

export default function TabModal({ closeModal }) {
    const [selectedIndex, setSelectedIndex] = useState(0)
    const [nibDate, setNibDate] = useState(new Date());
    const [niuDate, setNiuDate] = useState(new Date());
    const [status, setStatus] = useState(options[0]);

    const { data, setData, post, progress , reset, errors} = useForm({
        name: '',
        tax_id_number: '',
        business_id_number: '',
        business_id_date: '',
        deed_of_establishment: '',
        deed_of_establishment_date: '',
        phone: '',
        address: '',
        pic_name: '',
        pic_email: '',
        pic_phone: '',
        status: '',
    });

    const handleSubmit = () => {
        data["status"] = status.value
        data["business_id_date"] = moment(nibDate).format('YYYY-MM-DD');
        data["deed_of_establishment_date"] = moment(niuDate).format('YYYY-MM-DD');
        post(route('company.clients.store'), {
            onSuccess: () => {
                reset()
                closeModal()
            },
            onError: (errors) => {
                console.log('error gaess',errors)
            }
        });
    }

    return (
        <Tab.Group selectedIndex={selectedIndex} onChange={setSelectedIndex}>
            <Tab.List className="flex space-x-1 rounded-xl my-10">
                <Tab
                    className={({ selected }) =>
                        classNames(
                            "w-full border-b-2 border-transparent py-5 text-sm font-medium leading-5 text-[#1088E4]",
                            selected
                                ? "border-b-2 border-[#1088E4]"
                                : "text-[#464E5F]"
                        )
                    }
                >
                    Perusahaan
                </Tab>
                <Tab
                    className={({ selected }) =>
                        classNames(
                            "w-full border-b-2 border-transparent py-5 text-sm font-medium leading-5 text-[#1088E4]",
                            selected
                                ? "border-b-2 border-[#1088E4]"
                                : "text-[#464E5F]"
                        )
                    }
                >
                    Contact Person
                </Tab>
            </Tab.List>
            <Tab.Panels className="mt-2">
                <Tab.Panel className="ring-white ring-opacity-60 ring-offset-2 ring-offset-blue-400 focus:outline-none focus:ring-2">
                    <div className="space-y-4">
                        <InputForm
                            error={errors?.name}
                            value={data?.name} onChange={(e) => setData('name', e.target.value)}
                            type="text"
                            className="w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5"
                            placeholder="Nama Perusahaan (Client)"
                        />

                        <InputForm error={errors?.tax_id_number} value={data?.tax_id_number} onChange={(e) => setData('tax_id_number', e.target.value)} type="text" placeholder="NPWP" />
                        <div className="flex space-x-4">
                            <InputForm error={errors?.business_id_number} value={data?.business_id_number} onChange={(e) => setData('business_id_number', e.target.value)} type="text" placeholder="No. NIB" />
                            <DatePicker className="w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5" selected={nibDate} onChange={(date) => setNibDate(date)} />
                        </div>
                        <div className="flex space-x-4">
                            <InputForm
                                error={errors?.deed_of_establishment}
                                value={data?.deed_of_establishment} onChange={(e) => setData('deed_of_establishment', e.target.value)}
                                type="text"
                                placeholder="No. Ijin Usaha KAP"
                            />
                            <DatePicker className="w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5" selected={niuDate} onChange={(date) => setNiuDate(date)} />
                        </div>
                        <textarea
                            value={data?.address} onChange={(e) => setData('address', e.target.value)}
                            className="w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5"
                            placeholder="Alamat"
                        ></textarea>
                        <div className="flex space-x-4">
                            <InputForm error={errors?.phone} value={data?.phone} onChange={(e) => setData('phone', e.target.value)} type="text" placeholder="No. Telepon" />
                            <InputForm type="text" placeholder="Website Perusahaan" />
                        </div>
                    </div>
                    <div className="mt-10 space-x-4 flex justify-end">
                        <button
                            type="button"
                            onClick={closeModal}
                            className="font-semibold text-sm p-5 px-8 text-[#1088E4] rounded-lg"
                        >
                            Batal
                        </button>
                        <button
                            type="button"
                            onClick={() => {
                                setSelectedIndex(1)
                            }}
                            className="font-semibold text-sm p-5 px-8 text-white bg-[#1088E4] rounded-lg"
                        >
                            Next
                        </button>
                    </div>
                </Tab.Panel>
                <Tab.Panel className="ring-white ring-opacity-60 ring-offset-2 ring-offset-blue-400 focus:outline-none focus:ring-2">
                    <div className="space-y-4">
                        <InputForm
                            error={errors?.pic_name}
                            value={data?.pic_name} onChange={(e) => setData('pic_name', e.target.value)}
                            type="text"
                            placeholder="Nama Pimpinan Perusahaan"
                        />
                        <div className="flex space-x-4">
                            <InputForm  error={errors?.pic_email} value={data?.pic_email} onChange={(e) => setData('pic_email', e.target.value)} type="text" placeholder="Email" />
                            <InputForm error={errors?.pic_phone}  value={data?.pic_phone} onChange={(e) => setData('pic_phone', e.target.value)} type="text" placeholder="No Telepon" />
                        </div>
                        <InputSelect options={options} value={status} onChange={(item) => setStatus(item)} />
                    </div>
                    <div className="mt-10 space-x-4 flex justify-end">
                        <button
                            type="button"
                            onClick={closeModal}
                            className="font-semibold text-sm p-5 px-8 text-[#1088E4] rounded-lg"
                        >
                            Batal
                        </button>
                        <button
                            type="button"
                            onClick={() => {
                                handleSubmit()
                            }}
                            className="font-semibold text-sm p-5 px-8 text-white bg-[#1088E4] rounded-lg"
                        >
                            Tambahkan
                        </button>
                    </div>
                </Tab.Panel>
            </Tab.Panels>
        </Tab.Group>
    );
}
