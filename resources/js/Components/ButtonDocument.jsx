import { Link } from "@inertiajs/inertia-react";

export default function ButtonDocument({ href, image, title, description }) {
    return (
        <Link href={href} className="border flex-1 border-zinc-200 rounded-3xl p-8 text-center flex flex-col justify-center items-center">
            <img src={image} alt={title} />
            <h3 className="text-slate-400 mt-3 mb-1">{title}</h3>
            <p className="text-zinc-700">{description}</p>
        </Link>
    )
}