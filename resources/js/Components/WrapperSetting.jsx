
import Authenticated from "@/Layouts/Authenticated";
import { Head } from "@inertiajs/inertia-react";
import SettingBack from "./SettingBack";

const WrapperSetting = ({ auth, errors, title, titleBack = "Settings", back, children }) => {
    return (
        <Authenticated
            childrenPadding={false}
            auth={auth}
            errors={errors}
            header={
                <SettingBack title={titleBack} href={back} />
            }
            mainClass="min-h-[calc(100vh-83px)]"
        >
            <Head title={title} />

            <div className="w-full">
                <div className="flex">
                    <div className="flex-1 flex">
                        <div className="flex-1 mx-4">
                            <div className="text-center my-4 mt-5">
                                <h2 className="text-[#46464665] font-semibold">{title}</h2>
                            </div>
                            {children}
                        </div>
                    </div>
                </div>
            </div>
        </Authenticated>
    )
}

export default WrapperSetting