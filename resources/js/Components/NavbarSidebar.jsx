import NavbarAdmin from "./NavbarAdmin";
import NavbarAdminKAP from "./NavbarAdminKAP";
import NavbarAuditor from "./NavbarAuditor";

export default function NavbarSidebar({ role, sidebarSmall }) {
    switch (role) {
        case "super-admin":
            return <NavbarAdmin sidebarSmall={sidebarSmall} />;

        case "company-admin":
            return <NavbarAdminKAP sidebarSmall={sidebarSmall} />;

        default:
            return <NavbarAuditor sidebarSmall={sidebarSmall} />;
    }
}
