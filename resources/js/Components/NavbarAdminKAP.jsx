import SidebarLink from "./SidebarLink";

export default function NavbarAdminKAP({ sidebarSmall }) {
    return (
        <>
            <div className="mt-5 text-center">
                <div className="flex flex-row p-1 px-4 bg-[#F9F9FB] rounded-lg">
                    <div className="flex items-center">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="18"
                            height="18"
                            fill="none"
                            viewBox="0 0 18 18"
                        >
                            <path
                                fill="#2A2A2A"
                                d="M16.275 14.175L13.95 11.85a7.01 7.01 0 01-2.1 2.1l2.325 2.325c.3.3.75.3 1.05 0l1.05-1.05c.3-.3.3-.75 0-1.05z"
                            ></path>
                            <path
                                fill="#5E6278"
                                d="M8.25 15C4.5 15 1.5 12 1.5 8.25s3-6.75 6.75-6.75S15 4.5 15 8.25 12 15 8.25 15zm0-12A5.218 5.218 0 003 8.25a5.218 5.218 0 005.25 5.25 5.218 5.218 0 005.25-5.25A5.218 5.218 0 008.25 3z"
                                opacity="0.3"
                            ></path>
                        </svg>
                    </div>

                    <input
                        type="text"
                        placeholder="Search..."
                        className="w-full border-none bg-[#F9F9FB] text-xs"
                    />
                </div>
            </div>

            <div className="flex justify-center my-3">
                <hr className="w-3/4" />
            </div>

            <ul className="space-y-2 tracking-wide">
                <li>
                    <SidebarLink
                        sidebarSmall={sidebarSmall}
                        href={route("company.dashboard")}
                        active={route().current("company.dashboard")}
                        image="/images/icons/dashboard.png"
                        label="Dashboard"
                    />
                </li>
                <li>
                    <SidebarLink
                        sidebarSmall={sidebarSmall}
                        href={route("company.clients.index")}
                        active={route().current("company.clients.*")}
                        image="/images/icon_user.png"
                        label="Client List"
                    />
                </li>
                <li>
                    <SidebarLink
                        sidebarSmall={sidebarSmall}
                        href={route("company.projects.on-going")}
                        active={route().current("company.projects.on-going")}
                        image="/images/icon_on_going.png"
                        label="On Going Project"
                    />
                </li>
                <li>
                    <SidebarLink
                        sidebarSmall={sidebarSmall}
                        href={route("company.projects.completed")}
                        active={route().current("company.projects.completed")}
                        image="/images/icon_completed_project.png"
                        label="Completed Project"
                    />
                </li>
                <li>
                    <SidebarLink
                        sidebarSmall={sidebarSmall}
                        href={route("company.auditors.index")}
                        active={route().current("company.auditors.index")}
                        image="/images/icon_auditor_list.png"
                        label="Auditor List"
                    />
                </li>
                <li>
                    <SidebarLink
                        sidebarSmall={sidebarSmall}
                        href={route("admin.database-backup")}
                        active={route().current("admin.database-backup")}
                        image="/images/icons/timesheet.png"
                        label="Monitor Absensi"
                    />
                </li>
            </ul>

            <div className="flex justify-center my-3">
                <hr className="w-3/4" />
            </div>

            <ul className="space-y-2 tracking-wide">
                <li>
                    <SidebarLink
                        sidebarSmall={sidebarSmall}
                        href={route("company.dashboard")}
                        active={route().current("company.dashboard")}
                        image="/images/icons/pbc.png"
                        label="Report"
                    />
                </li>
                <li>
                    <SidebarLink
                        sidebarSmall={sidebarSmall}
                        href={route("company.settings.index")}
                        active={route().current("company.settings.*") | route().current("company.settings")}
                        image="/images/icons/settings.png"
                        label="Setting"
                    />
                </li>
                <li>
                    <SidebarLink
                        sidebarSmall={sidebarSmall}
                        href={route("company.dashboard")}
                        active={route().current("company.dashboard")}
                        image="/images/icons/support.png"
                        label="IT Support"
                    />
                </li>
            </ul>
        </>
    );
}
