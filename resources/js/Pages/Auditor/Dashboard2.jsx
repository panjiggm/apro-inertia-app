import Card from "@/Components/homepage/Card";
import ClientStatus from "@/Components/homepage/ClientStatus";
import ClientTalk from "@/Components/homepage/ClientTalk";
import Insight from "@/Components/homepage/Insight";
import TitleWithDropdown from "@/Components/homepage/TitleWIthDropdown";
import TopBreadcrumb from "@/Components/TopBreadcrumb";
import Authenticated from "@/Layouts/Authenticated";
import { Head } from "@inertiajs/inertia-react";
import {
    Bar,
    BarChart,
    CartesianGrid,
    Cell,
    Legend,
    Pie,
    PieChart,
    ResponsiveContainer,
    Sector,
    Tooltip,
    XAxis,
    YAxis,
} from "recharts";

const data = [
    {
        name: "The Walt Disn ...",
        percen: 80,
        pv: 2400,
        amt: 2400,
    },
    {
        name: "McDonald's",
        percen: 96,
        pv: 1398,
        amt: 2210,
    },
    {
        name: "Apple",
        percen: 77,
        pv: 9800,
        amt: 2290,
    },
    {
        name: "Louis Vuitton",
        percen: 84,
        pv: 3908,
        amt: 2000,
    },
    {
        name: "eBay",
        percen: 49,
        pv: 4800,
        amt: 2181,
    },
    {
        name: "Facebook",
        percen: 62,
        pv: 3800,
        amt: 2500,
    },
];

const data1 = [
    { name: "Group A", value: 80 },
    { name: "Group C", value: 20 },
];
const COLORS = ["#00CB75", "#FFA513", "#FF007A"];

export default function Dashboard(props) {
    const CustomLabel = (props) => {
        const { x, y, width, height, value } = props;
        const fireOffset = value.toString().length < 5;
        const offset = fireOffset ? -40 : 5;
        return (
            <text
                x={x + width - offset}
                y={y + height - 5}
                className="font-montserrat"
                dy={-4}
                fill="#464646"
                textAnchor="middle"
            >
                {value}%
            </text>
        );
    };

    const RADIAN = Math.PI / 180;
    const renderCustomizedLabel = ({
        cx,
        cy,
        midAngle,
        innerRadius,
        outerRadius,
        percent,
        index,
    }) => {
        const radius = innerRadius + (outerRadius - innerRadius) * 0.25;
        const x = cx + radius * Math.cos(-midAngle * RADIAN);
        const y = cy + radius * Math.sin(-midAngle * RADIAN);
        console.log('x', x, y)
        return (
            <text
                x={index === 0 ? 165 : 220}
                y={index === 0 ? 210 : 188}
                fill="white"
                textAnchor={x > cx ? "start" : "end"}
                dominantBaseline="central"
            >
                {`${(percent * 100).toFixed(0)}%`}
            </text>
        );
    };

    return (
        <Authenticated
            auth={props.auth}
            errors={props.errors}
            header={<TopBreadcrumb />}
        >
            <Head title="Dashboard" />

            <div className="w-full">
                <div className="flex space-x-8">
                    <div className="flex-1">
                        <div className="w-full border border-[#E6E6E8] rounded-2xl px-8 py-9 bg-white mb-5 mt-5 shadow-md">
                            <TitleWithDropdown
                                title="Project Status"
                                label="All"
                            />
                            <div className="w-full h-[450px]">
                                <ResponsiveContainer>
                                    <BarChart
                                        layout="vertical"
                                        margin={{
                                            top: 5,
                                            right: 30,
                                            left: 20,
                                            bottom: 5,
                                        }}
                                        width={150}
                                        height={40}
                                        data={data}
                                    >
                                        <XAxis
                                            type="number"
                                            tick={false}
                                            axisLine={false}
                                            padding={{ right: 127 }}
                                        />
                                        <YAxis
                                            type="category"
                                            dataKey="name"
                                            width={200}
                                        />
                                        <CartesianGrid strokeDasharray="3 3" />
                                        <defs>
                                            <linearGradient
                                                id="colorUv"
                                                x1="1"
                                                y1="0.5"
                                                x2="0"
                                                y2="0.5"
                                            >
                                                <stop
                                                    offset="0%"
                                                    stop-color="#ffffb5"
                                                />
                                                <stop
                                                    offset="0%"
                                                    stop-color="#f6d860"
                                                />
                                                <stop
                                                    offset="0%"
                                                    stop-color="#ffffff"
                                                />
                                                <stop
                                                    offset="0%"
                                                    stop-color="#ed894b"
                                                />
                                                <stop
                                                    offset="0%"
                                                    stop-color="#d53a42"
                                                />
                                                <stop
                                                    offset="0%"
                                                    stop-color="#3fc4fd"
                                                />
                                                <stop
                                                    offset="100%"
                                                    stop-color="#3f8dfd"
                                                />
                                            </linearGradient>
                                        </defs>
                                        <Bar
                                            dataKey="percen"
                                            barSize={25}
                                            fill={`url(#colorUv)`}
                                            for
                                            label={<CustomLabel />}
                                            radius={[0, 30, 30, 0]}
                                        />
                                    </BarChart>
                                </ResponsiveContainer>
                            </div>
                        </div>
                    </div>

                    <div className="w-96">
                        <div className="w-full border border-[#E6E6E8] rounded-2xl px-8 py-9 bg-white mb-5 mt-5 shadow-md">
                            <TitleWithDropdown
                                label="This Month"
                                title="Total Achivement"
                            />
                            <div className="w-full h-[450px]">
                                <ResponsiveContainer>
                                    <PieChart
                                        width={800}
                                        height={400}
                                    // onMouseEnter={this.onPieEnter}
                                    >
                                        <defs>
                                            <linearGradient id="colorUv3" x1="0" y1="0" x2="0" y2="1">
                                                <stop offset="5%" stopColor="#FFA513" stopOpacity={1} />
                                                <stop offset="95%" stopColor="#F5415C" stopOpacity={1} />
                                            </linearGradient>
                                        </defs>
                                        <Pie
                                            data={data1}
                                            cx={160}
                                            cy={120}
                                            innerRadius={60}
                                            labelLine={false}
                                            label={renderCustomizedLabel}
                                            outerRadius={120}
                                            fill="url(#colorUv3)"
                                            dataKey="value"
                                            activeShape={renderActiveShape}
                                            activeIndex={0}
                                        />

                                    </PieChart>
                                </ResponsiveContainer>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="w-full border border-[#E6E6E8] rounded-2xl px-8 py-9 bg-white mb-5 mt-5 shadow-md">
                    <div className="mb-9 flex items-center justify-between mx-6">
                        <h2 className="text-lg font-semibold">
                            Team Duscussion
                        </h2>
                        <div className="flex justify-between items-center p-3 rounded-lg border border-[#E2E2E4]">
                            <span className="mr-2 text-xs text-[#464646]">
                                New Message
                            </span>
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="8"
                                height="5"
                                fill="none"
                                viewBox="0 0 8 5"
                            >
                                <path
                                    fill="#464646"
                                    d="M7.516.644a.656.656 0 010 .94l-2.82 2.74a1 1 0 01-1.393 0L.484 1.583a.656.656 0 01.914-.94l2.254 2.189a.5.5 0 00.696 0L6.602.643a.656.656 0 01.914 0z"
                                ></path>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </Authenticated>
    );
}

const renderActiveShape = (props) => {
    const RADIAN = Math.PI / 180;
    const { cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle, fill, payload, percent, value } = props;
    const sin = Math.sin(-RADIAN * midAngle);
    const cos = Math.cos(-RADIAN * midAngle);
    const sx = cx + (outerRadius + 10) * cos;
    const sy = cy + (outerRadius + 10) * sin;
    const mx = cx + (outerRadius + 30) * cos;
    const my = cy + (outerRadius + 30) * sin;
    const ex = mx + (cos >= 0 ? 1 : -1) * 22;
    const ey = my;
    const textAnchor = cos >= 0 ? 'start' : 'end';

    return (
        <g>
            <text x={cx} y={cy} fontSize={25} fontWeight={700} dy={4} textAnchor="middle" fill={"#9FB3C5"}>
                Starter
            </text>
            <text x={cx} y={cy} fontSize={12} dy={20} textAnchor="middle" fill={"#9FB3C5"}>
                Exp. 11/2023
            </text>
            <defs>
                <linearGradient id="colorUv1" x1="0" y1="0" x2="0" y2="1">
                    <stop offset="5%" stopColor="#0AF08F" stopOpacity={1} />
                    <stop offset="95%" stopColor="#02A962" stopOpacity={1} />
                </linearGradient>
            </defs>
            <Sector
                cx={cx}
                cy={cy}
                innerRadius={innerRadius}
                outerRadius={outerRadius}
                startAngle={startAngle}
                endAngle={endAngle}
                fill="url(#colorUv1)"
                fillOpacity={1}
            />
        </g>
    );
};
