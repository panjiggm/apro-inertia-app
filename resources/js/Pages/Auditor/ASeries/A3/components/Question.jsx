export default function Question({title, className = '', children}) {
  return (
    <div className={`pl-28 pr-28 p-8 ${className}`}>
      {title ? (
        <div className="flex font-medium text-[#2A2A2A] mb-6">
          <div className="flex-1">
            <span>{title}</span>
          </div>
        </div>
      ) : null}
      <div className="mb-8">{children}</div>
    </div>
  )
}
