export default function TextArea({value, placeholder, onChange}) {
  return (
    <textarea
      className="w-full mt-3 border border-[#CDCDCD] bg-[#f5f5f566] rounded-lg"
      placeholder={placeholder}
      value={value}
      rows="6"
      onChange={onChange}
    />
  )
}
