export default function TextInputInline({description, value, onChange}) {
  return (
    <div className="flex justify-between text-[#2a2a2acc] mb-4 items-center">
      <p>{description}</p>
      <div className="flex text-sm gap-3 text-[#2a2a2acc]">
        <input className="border rounded-lg border-[#CDCDCD] text-sm min-w-[300px]" type="text" value={value} onChange={onChange} />
      </div>
    </div>
  )
}
