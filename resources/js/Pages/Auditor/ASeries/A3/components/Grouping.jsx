export default function Grouping({children}){
    return(
        <div className="rounded-lg p-6 bg-[#F5F5F5] shadow mb-5">
            {children}
        </div>
    )
}