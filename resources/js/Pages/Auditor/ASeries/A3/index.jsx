import Authenticated from '@/Layouts/Authenticated'
import {Head} from '@inertiajs/inertia-react'
import {Inertia} from '@inertiajs/inertia'
import {useState} from 'react'

import Assesment from './Form/Assesment'
import Calculation from './Form/Calculation'
import SupportDocument from './Form/SupportDocument'

export default function A3(props) {
  const [answer, setAnswer] = useState({})

  const handleAnswer = (parent, id, value, type = 'answer') => {
    setAnswer({
      ...answer,
      [parent]: {
        ...answer[parent],
        [id]: {
          ...answer[parent][id],
          [type]: value,
        },
      },
    })
  }

  const handleSubmit = () => {
    let {project_id} = route().params
    let params = {
      answers: answer,
    }
    Inertia.post(route('auditor.aseries.a3.store', {project_id}), params)
  }

  return (
    <Authenticated auth={props.auth} errors={props.errors} header={false} childrenPadding={false}>
      <Head title="A.1 Scoping Questionnaire " />
      <div className="h-screen flex flex-col bg-white">
        <div className="h-14 bg-white items-center flex px-4">
          <button className="flex gap-5 items-center">
            <svg width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M7.29724 0.887453C6.85448 0.408884 6.0979 0.408885 5.65514 0.887454L0.628292 6.32089C0.273675 6.70419 0.273676 7.29581 0.628292 7.67911L5.65514 13.1125C6.0979 13.5911 6.85447 13.5911 7.29723 13.1125C7.69388 12.6838 7.69388 12.0221 7.29723 11.5933L3.36176 7.33955C3.18446 7.1479 3.18446 6.85209 3.36176 6.66044L7.29724 2.40667C7.69389 1.97793 7.69389 1.31619 7.29724 0.887453Z"
                fill="#323232"
              />
            </svg>
            <span className="font-semibold">A.3 Materiality Determination</span>
          </button>
        </div>
        <div className="flex-1 overflow-y-auto">
          <Assesment />
          <Calculation />
          <SupportDocument />
        </div>
        <div className="h-20 bg-slate-400">
          <button onClick={handleSubmit} className="bg-teal-500 rounded-lg p-4 px-8 text-white">
            {' '}
            Submit Testing
          </button>
        </div>
      </div>
    </Authenticated>
  )
}
