import React from 'react'
import Accordion from '@/Components/Accordion'
import RadioInput from '@/Components/RadioInput'

export default function ClientAcceptanceProcedure() {
  return (
    <Accordion
      buttonContent={
        <div className=" justify-between px-3 py-8 border-y uppercase text-[#464646] font-semibold">
          <p>II. Client acceptance procedure</p>
        </div>
      }
    >
      <div className="p-8 max-w-[720px]">
        <p className=" text-[#2A2A2A] font-medium mb-2 text-sm">
          <span className="inline-block mr-4">II.A</span>
          Competence, ability and resources
        </p>
        <p className="pl-10 text-[#2A2A2A] opacity-80 mb-8 text-sm">
          Consider whether the Firm has the competence, capability and resources to perform new Engagements from new
          clients and include whether:
        </p>
        <div className="px-4 overflow-auto">
          <table className='className="w-full"'>
            <tbody>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">1.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                  <label className="opacity-80">
                    Firm personnel have knowledge of the industry or relevant matters relating to the client
                  </label>
                </td>
                <td className="py-2">
                  <div className="flex items-center space-x-4">
                    <RadioInput label="Yes" value="1" />
                    <RadioInput label="No" value="0" />
                  </div>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">2.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                  <label className="opacity-80">
                    Firm personnel have experience in accordance with regulatory or reporting requirements, or have
                    sufficient knowledge and expertise
                  </label>
                </td>
                <td className="py-2">
                  <div className="flex items-center space-x-4">
                    <RadioInput label="Yes" value="1" />
                    <RadioInput label="No" value="0" />
                  </div>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">3.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                  <label className="opacity-80">
                    The firm has sufficient personnel with the necessary competencies and capabilities
                  </label>
                </td>
                <td className="py-2">
                  <div className="flex items-center space-x-4">
                    <RadioInput label="Yes" value="1" />
                    <RadioInput label="No" value="0" />
                  </div>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">4.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                  <label className="opacity-80">Experts are available, if needed.</label>
                </td>
                <td className="py-2">
                  <div className="flex items-center space-x-4">
                    <RadioInput label="Yes" value="1" />
                    <RadioInput label="No" value="0" />
                  </div>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">5.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                  <label className="opacity-80">
                    Have personnel who meet the criteria and eligibility to conduct an Engagement Quality Control
                    Review, if necessary.
                  </label>
                </td>
                <td className="py-2">
                  <div className="flex items-center space-x-4">
                    <RadioInput label="Yes" value="1" />
                    <RadioInput label="No" value="0" />
                  </div>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">6.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                  <label className="opacity-80">
                    The firm is able to complete the Engagement according to the reporting deadline.
                  </label>
                </td>
                <td className="py-2">
                  <div className="flex items-center space-x-4">
                    <RadioInput label="Yes" value="1" />
                    <RadioInput label="No" value="0" />
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div className="border-t">
        <div className="p-8 max-w-[720px]">
          <p className=" text-[#2A2A2A] font-medium mb-2 text-sm">
            <span className="inline-block mr-4">II.B</span>
            Corporate eleigibility, independence and conflict of interest
          </p>
          <div className="px-4 overflow-auto">
            <table className='className="w-full"'>
              <tbody>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">1.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      Does the engagement comply with the rules and procedures as described in the firm's Independence
                      Policy.
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">2.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      Have you confirmed potential conflicts of interest and independence?
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">3.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      Are there any conflicts of interest and independence issues with this prospective client?
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div className="border-t">
        <div className="p-8 max-w-[720px]">
          <p className=" text-[#2A2A2A] font-medium mb-2 text-sm">
            <span className="inline-block mr-4">II.C</span>
            Integrity and reputation of prospective clients
          </p>
          <div className="px-4 overflow-auto">
            <table className='className="w-full"'>
              <tbody>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">1.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80" colSpan={2}>
                    <label className="opacity-80">
                      The client indicates that there is a limitation on the scope of audit work
                    </label>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80"></td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">Does the prospective client have good ethics and reputation?</label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80"></td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      Is the prospective client not currently in litigation/legal issues?
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80"></td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      Are key management, members of the board of directors and commissioners currently in
                      litigation/legal issues with other parties?
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">2.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      Is there information regarding the attitude of owners, key management and those charged with
                      governance towards aggressive interpretations of accounting standards and the internal control
                      environment.
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">3.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      Discuss with Quality & Risk Management Division whether they are aware of any information about
                      cases of fraud, money laundering and terrorist financing, litigation, adverse publicity, tax
                      evasion, etc which may involve potential clients and their shareholders, board of directors and
                      commissioners and key senior personnel ?
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80"></td>
                  <td className="py-2 pl-2" colSpan={2}>
                    <textarea
                      placeholder="Comment..."
                      className="py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                    />
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">4.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      Whether the firm is considering employing an investigative agency to obtain information on
                      criminal/civil records, lawsuits and decisions; tax, bankruptcy, litigation, property and credit
                      records for Clients, officers, directors, commissioners or significant shareholders.
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80"></td>
                  <td className="py-2 pl-2" colSpan={2}>
                    <textarea
                      placeholder="Comment..."
                      className="py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                    />
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80"></td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      In particular, have any issues been identified with respect to the above procedure? If yes,
                      discuss the nature of the issues raised and what impact they may have.
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div className="border-t">
        <div className="p-8 max-w-[720px]">
          <p className=" text-[#2A2A2A] font-medium mb-2 text-sm">
            <span className="inline-block mr-4">II.D</span>
            Changes of auditor
          </p>
          <div className="px-4 overflow-auto">
            <table className='className="w-full"'>
              <tbody>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">1.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      Have you communicated with the predecessor auditor prior to accepting the Engagement?
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">2.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      Did the predecessor auditor permit and give us access to their working papers?
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80"></td>
                  <td className="py-2 pl-2" colSpan={2}>
                    <p className="text-sm font-normal italic text-[#2A2A2A] opacity-80 mb-6">
                      If such access is not granted, the reasons are explained, as well as the nature of the procedures
                      we plan to ensure regarding the opening balance:
                    </p>
                    <textarea
                      placeholder="Comment..."
                      className="py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                    />
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">3.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      If comparative financial statements are to be issued, are the predecessor auditors prepared to
                      reissue their report on the previous year's financial statements, or approve their use. (If not,
                      the reasons and nature of it or our audit procedures in this case are explained).
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80"></td>
                  <td className="py-2 pl-2" colSpan={2}>
                    <textarea
                      placeholder="Comment..."
                      className="py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                    />
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div className="border-t">
        <div className="p-8 max-w-[720px]">
          <p className="text-[#2A2A2A] font-medium mb-8 text-sm">
            <span className="inline-block mr-4">II.E</span>
            Financial Risk
          </p>
          <div className="px-4 overflow-auto">
            <table className='className="w-full"'>
              <tbody>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">1.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">Does the client have the resources to pay the audit fees?</label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">2.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      Whether there is substantial doubt about the Client's ability to continue to exist for at least
                      one year from the end of the reporting date (eg 31 December 20xx). If so, a description of the
                      condition giving rise to the doubt, management's plan to address it, and determining whether the
                      plan appears feasible, or determining whether management properly considered it and disclosed the
                      circumstances, is recorded. (Usually, a lack of a viable plan to address substantial doubts or
                      improper accounting and disclosure will preclude acceptance of prospective clients.)
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80"></td>
                  <td className="py-2 pl-2" colSpan={2}>
                    <textarea
                      placeholder="Comment..."
                      className="py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                    />
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">3.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      Does the client aggressively keep audit fees as low as possible?
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80"></td>
                  <td className="py-2 pl-2" colSpan={2}>
                    <p className="text-sm text-[#2A2A2A] opacity-80 mb-6">
                      In particular, have any concerns about the financial risks been identified? If so, discuss the
                      nature of the issues raised and what impact they might have.
                    </p>
                    <textarea
                      placeholder="Comment..."
                      className="py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                    />
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div className="border-t">
        <div className="p-8 max-w-[720px]">
          <p className="text-[#2A2A2A] font-medium mb-8 text-sm">
            <span className="inline-block mr-4">II.F</span>
            Other Risk
          </p>
          <div className="px-4 overflow-auto">
            <table className='className="w-full"'>
              <tbody>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">1.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      The client indicates that there is a limitation on the scope of audit work
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">2.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      The client engages in overseas transactions in a save heaven country, or confidential jurisdiction
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">3.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      The client is in the process of seeking new investors/funding in the future through an IPO, debt
                      issuance, institutional investors or others.
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">4.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      The audit opinion of the client's financial statements will be used as part of the opinion of the
                      public company
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">5.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      Existence of highly complex accounting (eg derivative/hedging; off-balance sheet financing; bond
                      conversion) or auditing issues
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">6.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      The client's financial statements will be used by banks/non-bank financial institutions or for
                      loans purposes to bank/non-bank financial institution and other financial institutions
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">7.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">The client has significant overseas operations.</label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">8.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      The client's financial reports are addressed to foreign regulators.
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">9.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      Participate in speculative business, unusual transactions, and/or there are too many and unusually
                      complex related party transactions
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">10.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      There are significant disputes between shareholders or with management.
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">11.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      The client is known to have conducted business or had relationships with local parties or
                      Politically Exposed Persons (PEP) abroad. "PEP is a person who has or has had public authorities
                      including State Administrators as referred to in the laws and regulations governing State
                      Administrators, and/or has been registered as a member of a political party that has influence
                      over political policies and operations, both Indonesian citizens and foreign nationals".
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">12.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      The Client's Business has any relationship with any individual, entity or country sanctioned by
                      international, local or other state bodies.
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">13.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">The client has severe going concern issues.</label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">14.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      There is a potential restatement of the previous year's audited financial statements.
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">15.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      It is possible that an unqualified opinion will not be issued on this year's audit of financial
                      statements.
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">16.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">
                      The client is involved in disputes and unresolved issues with the predecessor auditor.
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">17.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px] opacity-80">
                    <label className="opacity-80">Other</label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80"></td>
                  <td className="py-2 pl-2 opacity-80" colSpan={2}>
                    <p className="text-sm text-[#2A2A2A] font-normal italic mb-6 opacity-80">
                      If there is a risk profile other than those identified above, please state it in the column below
                      and select yes or no (if yes is selected, then High Risk, and vice versa)
                    </p>
                    <textarea
                      placeholder="Comment..."
                      className="py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                    />
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </Accordion>
  )
}
