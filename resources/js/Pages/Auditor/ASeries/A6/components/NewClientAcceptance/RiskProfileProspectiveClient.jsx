import Accordion from '@/Components/Accordion'
import React from 'react'

export default function RiskProfileProspectiveClient() {
  return (
    <Accordion
      buttonContent={
        <div className="justify-between px-3 py-8 border-y uppercase text-[#464646] font-semibold">
          <p>III. Risk profile of prospective clients</p>
        </div>
      }
    >
      <div className="px-4 py-10">
        <div className="max-w-[608px] mx-auto">
          <p className="opacity-80 text-justify text-[#2A2A2A] font-normal">
            Prospective Clients have [<span className="text-[#FF5050] font-semibold">High Risk</span> /{' '}
            <span className="text-[#05E200] font-semibold">Low Risk</span>] because ......... and you are advised to get
            approval from the Managing Partner and Risk Management Partner before accepting the engagement
          </p>
        </div>
      </div>
    </Accordion>
  )
}
