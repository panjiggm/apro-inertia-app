import Input from '@/Components/Input'
import Modal from '@/Components/clients/Modal'
import React, {useState} from 'react'

export default function AddForm({isOpen = false, onClose = () => {}, onAddNewData = () => {}}) {
  const [newTreasuryCycle, setNewTreasuryCycle] = useState('')

  const handleCloseModal = () => {
    setNewTreasuryCycle('')
    onClose()
  }

  const handleAddNewData = e => {
    e?.preventDefault()
    if (!newTreasuryCycle?.length) return
    onAddNewData(newTreasuryCycle)
    handleCloseModal()
  }

  return (
    <Modal open={isOpen} onClose={handleCloseModal}>
      <div className="w-ful">
        <h2 className="text-center font-semibold text-lg mb-10 uppercase">ADD CYCLE</h2>
        <form onSubmit={e => handleAddNewData(e)}>
          <Input
            placeholder="TREASURY CYCLE"
            value={newTreasuryCycle}
            handleChange={e => setNewTreasuryCycle(e.target.value)}
            className="w-full mb-10"
          />

          <div className="flex items-center justify-center space-x-1 w-full">
            <button
              type="button"
              onClick={handleCloseModal}
              className="flex-1 p-[14px] text-[#1088E4] rounded-lg text-sm font-semibold"
            >
              Cancel
            </button>
            <button type="submit" className="flex-1 p-[14px] text-white bg-[#1088E4] rounded-lg text-sm font-semibold">
              Add
            </button>
          </div>
        </form>
      </div>
    </Modal>
  )
}
