import Accordion from '../../Components/Accordion'
import Question from '../../Components/Question'
import RadioButtonComment from '../../Components/RadioButtonComment'

export default function InitialConsideration({answer, handleAnswer}) {
  return (
    <Accordion number={'I.'} title="APRO LIBRARY CONSIDERATION">
      <Question number={'1.1'} title="Initial Audit Engagement" className="border-b">
        <RadioButtonComment
          description="Is this an initial audit engagement?"
          type="horizontal"
          value={answer[1].answer}
          comment={answer[1].comment}
          name="1.1"
          onChange={e => {
            handleAnswer('I', 1, e.target.value, 'answer')
          }}
          onChangeComment={ e => {
            handleAnswer('I', 1, e.target.value, 'comment')
          }}
          options={[
            {label: 'Yes', value: 'Yes'},
            {label: 'No', value: 'No'},
          ]}
        />
      </Question>
      <Question number={'1.2'} title="Additional Engagement">
        <RadioButtonComment
          description="Do you plan to complete any of the following additional engagement in this file?"
          value={answer[2].answer}
          name="1.2"
          comment={answer[2].comment}
          onChange={e => {
            handleAnswer('I', 2, e.target.value, 'answer')
          }}
          onChangeComment={ e => {
            handleAnswer('I', 2, e.target.value, 'comment')
          }}
          options={[
            {label: 'Agreed upon procedurest', value: 'Agreed upon procedurest'},
            {label: 'Other assurance engagement', value: 'Other assurance engagement'},
            {label: 'No', value: 'No'},
          ]}
        />
      </Question>
    </Accordion>
  )
}
