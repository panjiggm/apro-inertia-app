import Accordion from '../../Components/Accordion'
import Question from '../../Components/Question'
import RadioButtonComment from '../../Components/RadioButtonComment'

export default function EngagementTeamAndOtherResources({answer, handleAnswer}) {
  return (
    <Accordion number={'IV.'} title="APRO LIBRARY CONSIDERATION">
      <Question
        number={'4.1'}
        title="Is an engagement quality Control Review (EQC Review) needed on this engagement?"
        className="border-b"
      >
        <RadioButtonComment
          type="horizontal"
          name="4.1"
          value={answer[1].answer}
          comment={answer[1].comment}
          onChange={e => {
            handleAnswer('IV', 1, e.target.value, 'answer')
          }}
          onChangeComment={e => {
            handleAnswer('IV', 1, e.target.value, 'comment')
          }}
          options={[
            {label: 'Yes', value: 'Yes'},
            {label: 'No', value: 'No'},
          ]}
        />
      </Question>
      <Question number={'4.2'} title="Additional Engagement">
        <RadioButtonComment
          name="4.2"
          value={answer[2].answer}
          comment={answer[2].comment}
          onChange={e => {
            handleAnswer('IV', 2, e.target.value, 'answer')
          }}
          onChangeComment={e => {
            handleAnswer('IV', 2, e.target.value, 'comment')
          }}
          options={[
            {label: 'Yes', value: 'Yes'},
            {label: 'No', value: 'No'},
          ]}
        />
      </Question>
      <Question number={'4.3'} title="Are you planning to use the work of interna auditors (IA) ?">
        <RadioButtonComment
          name="4.3"
          value={answer[3].answer}
          comment={answer[3].comment}
          onChange={e => {
            handleAnswer('IV', 3, e.target.value, 'answer')
          }}
          onChangeComment={e => {
            handleAnswer('IV', 3, e.target.value, 'comment')
          }}
          options={[
            {label: 'Yes', value: 'Yes'},
            {label: 'No', value: 'No'},
          ]}
        />
      </Question>
      <Question
        number={'4.4'}
        title="Does the client use a service organization to process material transaction affecting the financial statements ?"
      >
        <RadioButtonComment
          name="4.4"
          value={answer[4].answer}
          comment={answer[4].comment}
          onChange={e => {
            handleAnswer('IV', 4, e.target.value, 'answer')
          }}
          onChangeComment={e => {
            handleAnswer('IV', 4, e.target.value, 'comment')
          }}
          options={[
            {label: 'Yes', value: 'Yes'},
            {label: 'No', value: 'No'},
          ]}
        />
      </Question>
    </Accordion>
  )
}
