export default function Question({number, title, className = "", children}) {
  return (
    <div className={`pl-28 pr-28 p-8 ${className}`}>
      <div className="flex font-medium text-[#2A2A2A] mb-6">
        <div className="w-10 mr-2">
          <span>{number}</span>
        </div>
        <div className="flex-1">
          <span>{title}</span>
        </div>
      </div>
      <div className="mb-8 pl-12">{children}</div>
    </div>
  )
}
