import { useState } from "react"

const Accordion = ({number, title, children}) => {
  const [active, setActive] = useState(true)
  return (
    <div className="flex flex-col">
      <button
        className="flex justify-between items-center border-t border-b p-4 pl-20 my-1"
        onClick={() => setActive(!active)}
      >
        <div className="text-sm font-medium flex">
          <span className="w-8 text-left">{number}</span>
          <span>{title}</span>
        </div>
        <div className={`${!active ? 'rotate-180' : ''}`}>
          <svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              opacity="0.4"
              d="M1.1651 0.527074C0.806174 0.859142 0.806174 1.42657 1.1651 1.75864L5.0704 5.37171C5.4537 5.72633 6.04532 5.72633 6.42862 5.37171L10.3339 1.75864C10.6928 1.42657 10.6928 0.859143 10.3339 0.527075C10.0124 0.229587 9.51606 0.229587 9.19451 0.527075L6.08907 3.40014C5.89742 3.57745 5.60161 3.57745 5.40996 3.40014L2.30451 0.527074C1.98296 0.229586 1.48665 0.229586 1.1651 0.527074Z"
              fill="#323232"
            />
          </svg>
        </div>
      </button>
      <div className={`${active ? 'flex' : 'hidden'} w-full`}>
        <div className="w-full">{children}</div>
      </div>
    </div>
  )
}

export default Accordion
