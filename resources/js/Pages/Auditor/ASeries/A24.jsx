import React, {useState, useRef} from 'react'
import Authenticated from '@/Layouts/Authenticated'
import {Head} from '@inertiajs/inertia-react'
import Panel from '@/Components/Panel'
import Swal from 'sweetalert2'

export default function A24({project_id = 1, ...props}) {
  const [errorIndex, setErrorIndex] = useState(undefined)
  const [errorRow, setErrorRow] = useState([])
  const [currentData, setCurrentData] = useState([
    {
      row1: 'FSA',
      row2: 'B/S',
      row3: '3.5 Non-controlling Interest',
      row4: '3.5',
      row5: 'desc',
      row6: 'Credit',
    },
    {
      row1: 'GA',
      row2: 'GA',
      row3: 'N/A',
      row4: 'D.1',
      row5: 'desc',
      row6: 'N/A',
    },
    {
      row1: '',
      row2: '',
      row3: '',
      row4: '',
      row5: '',
      row6: '',
    },
  ])

  const handleChange = (key, value, index) => {
    const temp = [...currentData]
    temp[index][key] = value
    if (temp.length === index + 1) {
      setCurrentData([
        ...temp,
        {
          row1: '',
          row2: '',
          row3: '',
          row4: '',
          row5: '',
          row6: '',
        },
      ])
    } else {
      setCurrentData(temp)
    }
  }

  const handleSave = () => {
    setErrorIndex(undefined)
    setErrorRow([])
    checkDuplicateRSACode()
    checkEmptyValue()
    if (errorIndex === undefined) {
      console.log('values', currentData)
    }
  }

  const checkEmptyValue = () => {
    for (let i = 0; i < currentData.length - 1; i++) {
      let br = false
      let tempRow = []
      Object.keys(currentData[i]).forEach(key => {
        if (currentData[i][key] === '') {
          setErrorIndex(i)
          tempRow.push(key)
          br = true
        }
      })
      if (br) {
        setErrorRow(tempRow)
        break
      }
    }
  }

  const checkDuplicateRSACode = () => {
    const valueArr = currentData.map(function (item) {
      return item.row4
    })
    const isDuplicate = valueArr.some(function (item, idx) {
      if (valueArr.indexOf(item) != idx) {
        setErrorIndex(idx)
        setErrorRow([...errorRow, 'row4'])
        return true
      } else {
        return false
      }
    })
    return isDuplicate
  }

  const handleDelete = index => {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      iconColor: '#ff5050',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then(result => {
      if (result.isConfirmed) {
        const tmp = [...currentData]
        tmp.splice(index, 1)
        setCurrentData(tmp)
        Swal.fire({
          title: 'Deleted!',
          icon: 'success',
          iconColor: '#05e200',
          text: 'Your file has been deleted.',
          confirmButtonColor: '#05e200',
        })
      }
    })
  }

  return (
    <Authenticated auth={props.auth} errors={props.errors} header={false} childrenPadding={false}>
      <Head title="A.2.4 Mapping Financial Statement Area " />

      <div className="w-ful">
        <div className="flex space-x-8">
          <div className="flex-1 overflow-auto">
            <Panel>
              <Panel.Header>
                <Panel.Back
                  href={route('auditor.audit-stages.index', {
                    project_id,
                  })}
                />
                <Panel.Title title="AUD - Biffco Enterprises Ltd." subtitle="Jan 01, 2014 - Dec 31, 2014" />
                <Panel.Search placeholder="Search..." />
              </Panel.Header>
              <Panel.Content>
                <div className="bg-[#F8F8FA] text-center relative">
                  <h2 className="font-semibold my-5">A.2.4 Mapping Financial Statement Area </h2>
                  <button
                    className="bg-[#05e200] text-white rounded-md p-1 font-bold px-6 absolute top-4 right-7"
                    onClick={handleSave}
                  >
                    Save
                  </button>
                </div>
                <div className="w-full h-[calc(100vh-200px)] overflow-auto">
                  <table className="table-auto border w-full">
                    <thead>
                      <tr className="bg-[#F5F5F5] sticky top-0">
                        <th className="text-left p-3 border w-[150px]">Category</th>
                        <th className="text-left p-3 border w-[150px]">Level</th>
                        <th className="text-left p-3 border w-[425px]">Account Class</th>
                        <th className="text-left p-3 border w-[150px]">FSA Code</th>
                        <th className="text-left p-3 border">FSA Description</th>
                        <th className="text-left p-3 border w-[150px]">Sign</th>
                        <th className="text-left p-3 border">&nbsp;</th>
                      </tr>
                    </thead>
                    <tbody>
                      {currentData.map((item, index) => (
                        <tr key={`tr-${index}`} className={`border`}>
                          <td className="border">
                            <select
                              className="border-none w-full"
                              value={item?.row1}
                              onChange={e => {
                                handleChange('row1', e.target.value, index)
                              }}
                            >
                              <option value="">Select Category...</option>
                              <option value="FSA">FSA</option>
                              <option value="GA">GA</option>
                            </select>
                          </td>
                          <td
                            className={`border ${
                              errorIndex === index && errorRow.includes('row2') ? 'bg-red-500' : ''
                            }`}
                          >
                            <select
                              className="border-none w-full"
                              value={item?.row2}
                              onChange={e => handleChange('row2', e.target.value, index)}
                            >
                              <option value="">Select Level...</option>
                              {item?.row1 === 'FSA' ? (
                                <>
                                  <option value="B/S">B/S</option>
                                  <option value="P/L">P/L</option>
                                </>
                              ) : (
                                <option value="GA">GA</option>
                              )}
                            </select>
                          </td>
                          <td
                            className={`border ${
                              errorIndex === index && errorRow.includes('row3') ? 'bg-red-500' : ''
                            }`}
                          >
                            <select
                              className="border-none w-full"
                              value={item?.row3}
                              onChange={e => {
                                handleChange('row3', e.target.value, index)
                                if (item?.row2 !== 'GA') {
                                  handleChange('row4', e.target.value.substring(0, 3), index)
                                } else {
                                  handleChange('row4', 'D.', index)
                                }
                              }}
                            >
                              <option value="">Select Account Class...</option>
                              {item?.row2 !== 'GA' ? (
                                <>
                                  <option value="1.1 Current Assets">1.1 Current Assets</option>
                                  <option value="1.2 Non-current Assets">1.2 Non-current Assets</option>
                                  <option value="2.1 Short Term Liability">2.1 Short Term Liability</option>
                                  <option value="2.2 Long Term Liability">2.2 Long Term Liability</option>
                                  <option value="3.1 Capital Stock">3.1 Capital Stock</option>
                                  <option value="3.2 Other Equity Component">3.2 Other Equity Component</option>
                                  <option value="3.3 Comprehensive Income - B/S">3.3 Comprehensive Income - B/S</option>
                                  <option value="3.4 Retained Earning">3.4 Retained Earning</option>
                                  <option value="3.5 Non-controlling Interest">3.5 Non-controlling Interest</option>
                                  <option value="4.1 Revenue">4.1 Revenue</option>
                                  <option value="5.1 Cost of Revenue">5.1 Cost of Revenue</option>
                                  <option value="6.1 Operating Expenses">6.1 Operating Expenses</option>
                                  <option value="7.1 Other (Income)/Expenses">7.1 Other (Income)/Expenses </option>
                                  <option value="8.1 Income Tax">8.1 Income Tax</option>
                                  <option value="9.1 Other Comprehensive Income (OCI) - P/L">
                                    9.1 Other Comprehensive Income (OCI) - P/L
                                  </option>
                                </>
                              ) : (
                                <option value="N/A">N/A</option>
                              )}
                            </select>
                          </td>
                          <td
                            className={`border ${
                              errorIndex === index && errorRow.includes('row4') ? 'bg-red-500' : ''
                            }`}
                          >
                            <input
                              className="border-none w-full"
                              type="text"
                              placeholder="FSA CODE"
                              value={item?.row4}
                              maxLength={6}
                              onChange={e => handleChange('row4', e.target.value, index)}
                            />
                          </td>
                          <td
                            className={`border ${
                              errorIndex === index && errorRow.includes('row5') ? 'bg-red-500' : ''
                            }`}
                          >
                            <input
                              className="border-none w-full"
                              type="text"
                              placeholder="FSA description"
                              value={item?.row5}
                              onChange={e => handleChange('row5', e.target.value, index)}
                            />
                          </td>
                          <td
                            className={`border ${
                              errorIndex === index && errorRow.includes('row6') ? 'bg-red-500' : ''
                            }`}
                          >
                            <select
                              className="border-none w-full"
                              value={item?.row6}
                              onChange={e => handleChange('row6', e.target.value, index)}
                            >
                              <option value="">Select Sign...</option>
                              {item?.row2 !== 'GA' ? (
                                <>
                                  <option value="Debit">Debit</option>
                                  <option value="Credit">Credit</option>
                                </>
                              ) : (
                                <option value="N/A">N/A</option>
                              )}
                            </select>
                          </td>
                          <td className="border p-1 text-center">
                            <button onClick={() => handleDelete(index)}>
                              <svg
                                width="25"
                                height="25"
                                viewBox="0 0 25 25"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                              >
                                <circle cx="12.5" cy="12.5" r="12" fill="#FF5050" stroke="#FF5050" />
                                <rect
                                  x="7"
                                  y="15.8388"
                                  width="12.5"
                                  height="3.125"
                                  transform="rotate(-45 7 15.8388)"
                                  fill="white"
                                />
                                <rect
                                  x="14.7344"
                                  y="12.5246"
                                  width="4.68715"
                                  height="3.125"
                                  transform="rotate(45 14.7344 12.5246)"
                                  fill="white"
                                />
                                <rect
                                  x="9.20972"
                                  y="7"
                                  width="4.68678"
                                  height="3.125"
                                  transform="rotate(45 9.20972 7)"
                                  fill="white"
                                />
                              </svg>
                            </button>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </Panel.Content>
              <Panel.Breadcrumb>
                <Panel.BreadcrumbItem text="Existing Project" href={route('auditor.projects.existing')} />
                <Panel.BreadcrumbItem href={route('auditor.projects.existing')} text="Biffco Enterprises Ltd." />
                <Panel.BreadcrumbItem text="AUD - Biffco Enterprises Ltd." current={true} />
              </Panel.Breadcrumb>
            </Panel>
          </div>
        </div>
      </div>
    </Authenticated>
  )
}

const HeaderRowCustom = props => <tr className="bg-[#F5F5F5] sticky top-0" {...props} />
