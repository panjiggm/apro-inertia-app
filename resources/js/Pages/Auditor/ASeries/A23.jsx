import React, {useState, useRef} from 'react'
import Authenticated from '@/Layouts/Authenticated'
import {Head} from '@inertiajs/inertia-react'
import Panel from '@/Components/Panel'
import Swal from 'sweetalert2'

export default function A23({project_id = 1, ...props}) {
  const [errorIndex, setErrorIndex] = useState(undefined)
  const [errorRow, setErrorRow] = useState([])
  const [currentData, setCurrentData] = useState([
    {
      row1: '1.101 ',
      row2: 'Cash and Cash Equivalent',
      row3: '1',
    },
    {
      row1: '1.102',
      row2: 'Time Deposits 1.103 Derivative Assets 1.104 Investment Securities Available for Sale',
      row3: '2',
    },
    {
      row1: '1.103',
      row2: 'Derivative Assets',
      row3: '3',
    },
    {
      row1: '1.104',
      row2: 'Investment Securities Available for Sale',
      row3: '4',
    },
  ])

  const handleChange = (key, value, index) => {
    const temp = [...currentData]
    temp[index][key] = value
    if (temp.length === index + 1) {
      setCurrentData([
        ...temp,
        {
          row1: '',
          row2: '',
          row3: '',
        },
      ])
    } else {
      setCurrentData(temp)
    }
  }

  const handleSave = () => {
    setErrorIndex(undefined)
    setErrorRow([])
    checkDuplicateRSACode()
    checkEmptyValue()
    if (errorIndex === undefined) {
      console.log('values', currentData)
    }
  }

  const checkEmptyValue = () => {
    for (let i = 0; i < currentData.length - 1; i++) {
      let br = false
      let tempRow = []
      Object.keys(currentData[i]).forEach(key => {
        if (currentData[i][key] === '') {
          setErrorIndex(i)
          tempRow.push(key)
          br = true
        }
      })
      if (br) {
        setErrorRow(tempRow)
        break
      }
    }
  }

  const checkDuplicateRSACode = () => {
    const valueArr = currentData.map(function (item) {
      return item.row4
    })
    const isDuplicate = valueArr.some(function (item, idx) {
      if (valueArr.indexOf(item) != idx) {
        setErrorIndex(idx)
        setErrorRow([...errorRow, 'row4'])
        return true
      } else {
        return false
      }
    })
    return isDuplicate
  }

  return (
    <Authenticated auth={props.auth} errors={props.errors} header={false} childrenPadding={false}>
      <Head title="A.2.4 Mapping Financial Statement Area " />

      <div className="w-ful">
        <div className="flex space-x-8">
          <div className="flex-1 overflow-auto">
            <Panel>
              <Panel.Header>
                <Panel.Back
                  href={route('auditor.audit-stages.index', {
                    project_id,
                  })}
                />
                <Panel.Title title="AUD - Biffco Enterprises Ltd." subtitle="Jan 01, 2014 - Dec 31, 2014" />
                <Panel.Search placeholder="Search..." />
              </Panel.Header>
              <Panel.Content>
                <div className="bg-[#F8F8FA] text-center relative">
                  <h2 className="font-semibold my-5">A.2.3 Financial Statements Area</h2>
                  <button
                    className="bg-[#05e200] text-white rounded-md p-1 font-bold px-6 absolute top-4 right-7"
                    onClick={handleSave}
                  >
                    Export
                  </button>
                </div>
                <div className="w-full h-[calc(100vh-200px)] overflow-auto">
                  <table className="table-auto border w-full">
                    <thead>
                      <tr className="bg-[#F5F5F5] sticky top-0">
                        <th className="text-left p-3 border w-[150px]">FSA Code</th>
                        <th className="text-left p-3 border">FSA Description</th>
                        <th className="text-left p-3 border w-[150px]">Amount</th>
                      </tr>
                    </thead>
                    <tbody>
                      {currentData.map((item, index) => (
                        <tr key={`tr-${index}`} className={`border`}>
                          <td className="border">
                            <span className="p-2 px-3 block">{item?.row1}</span>
                          </td>
                          <td
                            className={`border ${
                              errorIndex === index && errorRow.includes('row2') ? 'bg-red-500' : ''
                            }`}
                          >
                            <span className="p-2 px-3 block">{item?.row2}</span>
                          </td>
                          <td
                            className={`border ${
                              errorIndex === index && errorRow.includes('row4') ? 'bg-red-500' : ''
                            }`}
                          >
                            <span className="p-2 px-3 block">{item?.row3}</span>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </Panel.Content>
              <Panel.Breadcrumb>
                <Panel.BreadcrumbItem text="Existing Project" href={route('auditor.projects.existing')} />
                <Panel.BreadcrumbItem href={route('auditor.projects.existing')} text="Biffco Enterprises Ltd." />
                <Panel.BreadcrumbItem text="AUD - Biffco Enterprises Ltd." current={true} />
              </Panel.Breadcrumb>
            </Panel>
          </div>
        </div>
      </div>
    </Authenticated>
  )
}

const HeaderRowCustom = props => <tr className="bg-[#F5F5F5] sticky top-0" {...props} />
