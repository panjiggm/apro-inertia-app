import React, { useState } from "react";
import { Inertia } from "@inertiajs/inertia";
import { ListRadiobox } from "@/Components/settings";
import WrapperSetting from "@/Components/WrapperSetting";
const plans = [
    {
        name: 'Singapore',
    },
    {
        name: 'Indonesia',
    },
    {
        name: 'Malaysia',
    },
    {
        name: 'United States',
    },
]

export default function Region(props) {
    const [selected, setSelected] = useState(plans[1])

    const handleChange = (value) => {
        setSelected(value)
        let params = {
            key: 'region',
            value: value.name
        }

        Inertia.post(route('auditor.settings.language.update'), params);
    }

    return (
        <WrapperSetting {...props} title="Region" titleBack="Language & Region" back={route('auditor.settings.language')} >
            <ListRadiobox selected={selected} setSelected={handleChange} data={plans} />
        </WrapperSetting>
    );
}
