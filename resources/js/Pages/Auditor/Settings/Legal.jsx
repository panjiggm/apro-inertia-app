import React, { useState } from "react";
import { ListItemLink } from "@/Components/settings";
import WrapperSetting from "@/Components/WrapperSetting";

export default function Legal(props) {
    const [active, setActive] = useState(false)
    return (
        <WrapperSetting {...props} title="Legal & Regulatory" back={route('auditor.settings.index')} >
            <ul>
                <ListItemLink href={route("auditor.settings.legal.notice")} title="Legal Notices" value="" />
                <ListItemLink href={route("auditor.settings.legal.license")}  title="License" value="" />
                <ListItemLink href={route("auditor.settings.legal.certification")}  title="Certification" value="" />
            </ul>
        </WrapperSetting>
    );
}
