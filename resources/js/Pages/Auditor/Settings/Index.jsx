import React, {useState} from "react";
import Authenticated from "@/Layouts/Authenticated";
import { Head } from "@inertiajs/inertia-react";
import TopBreadcrumb from "@/Components/TopBreadcrumb";
import AuditorMenuSettings from "@/Components/AuditorMenuSettings";

export default function SoftwareUpdate(props) {
    const [active, setActive] = useState(false)
    return (
        <Authenticated
            childrenPadding={false}
            auth={props.auth}
            errors={props.errors}
            header={
                <TopBreadcrumb home="Settings" />
            }
        >
            <Head title="Settings" />

            <div className="w-full">
                <div className="flex">
                    <div className="flex-1 flex">
                        <AuditorMenuSettings full={true} />
                    </div>
                </div>
            </div>
        </Authenticated>
    );
}
