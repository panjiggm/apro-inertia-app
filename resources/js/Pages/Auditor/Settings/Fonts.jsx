import React, { useState } from "react";
import { ListItemLink } from "@/Components/settings";
import WrapperSetting from "@/Components/WrapperSetting";

export default function SoftwareUpdate({ settings = { }, ...props }) {
    return (
        <WrapperSetting {...props} title="Fonts" back={route('auditor.settings.index')} >
            <ul>
                <ListItemLink href={route('auditor.settings.fonts.type')} title="Font Type" value={settings.font_type} borderBottom={false} />
            </ul>
        </WrapperSetting>
    );
}
