import React, { useState } from "react";
import { Inertia } from "@inertiajs/inertia";
import { ListItemLink, ListItemSpace, ListItemSwicther } from "@/Components/settings";
import WrapperSetting from "@/Components/WrapperSetting";

export default function Display({ settings = {}, ...props }) {
    const [active, setActive] = useState(settings.bold_text == '1' ? true : false)

    const handleChange = () => {
        let params = {
            key: 'bold_text',
            value: !active === true ? '1' : '0'
        }
        setActive(!active)

        Inertia.post(route('auditor.settings.display.update'), params);
    }

    return (
        <WrapperSetting {...props} title="Display" back={route('auditor.settings.index')} >
            <ul>
                <ListItemLink borderBottom={false} href={route('auditor.settings.display.auto-lock')} title="Auto-Lock" value={settings.auto_lock} />
                <ListItemSpace />
                <ListItemLink title="Text Size" value={settings.text_size} />
                <ListItemSwicther borderBottom={false} label="Bold Text" onChange={() => handleChange()} checked={active} />
            </ul>
        </WrapperSetting>
    );
}
