import TopBreadcrumb from "@/Components/TopBreadcrumb";
import Authenticated from "@/Layouts/Authenticated";
import { Head, Link } from "@inertiajs/inertia-react";

export default function ClientsDetail({ company, documents, ...props }) {
    return (
        <Authenticated
            auth={props.auth}
            errors={props.errors}
            header={
                <TopBreadcrumb current={`Detail KAP - ${company?.user?.name}`} />
            }
        >
            <Head title="Detail KAP Request" />
            <div className="flex bg-white ">
                <div className="flex-1 p-8 border-r">
                    <div className="mb-16">
                        <Title text="General Information" />
                        <Item title="Nama KAP" description={company?.user?.name} />
                        <Item title="NPWP" description={company?.tax_id_number} />
                        <Item title="Nomor NIB" description={company?.business_id_number} />
                        <Item title="Tanggal NIB" description={company?.business_id_date} />
                        <Item title="No. Ijin Usaha KAP" description={company?.business_license_number} />
                        <Item title="Tanggal Ijin Usaha KAP" description={company?.business_license_date} />
                        <Item title="Alamat KAP" description={company?.address} />
                        <Item title="No. Telepon" description={company?.phone} />
                        <Item title="Website KAP" description={company?.website} />
                    </div>
                    <div className="">
                        <Title text="Contact Person" />
                        <Item title="Nama" description={company?.pic_name} />
                        {/* <Item title="Jabatan" description="?" /> */}
                        <Item title="Whatsapp" description={company?.pic_phone} />
                        <Item title="Email" description={company?.pic_email} />
                    </div>
                </div>
                <div className="flex-1 pt-8">
                    <div className="mb-16 px-8">
                        <Title text="Legalitas Document" />
                        {
                            documents.map((item) => (
                                <ItemIcon key={item?.file_name} title={item?.file_name} size="4,5 MB" />
                            ))
                        }
                    </div>
                    <div className="mb-16 px-8">
                        <Title text="NPWP" />
                        {
                            documents?.NPWP?.map((item) => (
                                <ItemIcon key={item?.file_name} title={item?.file_name} size="4,5 MB" />
                            ))
                        }
                    </div>
                    <div className="mb-16 px-8">
                        <Title text="Identitas" />
                        {
                            documents?.Identitas?.map((item) => (
                                <ItemIcon key={item?.file_name} title={item?.file_name} size="4,5 MB" />
                            ))
                        }
                    </div>
                    <div className="border-t flex p-4 items-center px-8 gap-5">
                        <button className="bg-[#1989e90a] rounded-lg text-[#1989E9] flex-1 text-center p-4">Incomplete</button>
                        <Link href={route('admin.clients.activate', { user: company?.user?.id })} className="bg-[#1088E4] rounded-lg text-white flex-1 text-center p-4">Request Accept</Link>
                    </div>
                </div>
            </div>
        </Authenticated>
    )
}

const Title = ({ text }) => {
    return (
        <h2 className="text-lg font-semibold text-[#2A2A2A] mb-7">{text}</h2>
    )
}

const Item = ({ title, description }) => {
    return (
        <div className="flex justify-between text-xs mb-5 text-[#464646]">
            <p className="font-medium">{title}</p>
            <p>{description}</p>
        </div>
    )
}

const ItemIcon = ({ title, size, onDownload = () => { } }) => {
    return (
        <div className="flex justify-between text-xs mb-5 text-[#464646] items-center">
            <div className="flex gap-2 items-center w-[230px]">
                <img width="20" src="/images/icon_pdf.png" alt="document" />
                <p>{title}</p>
            </div>

            {/* <p>{size}</p> */}
            <button className="bg-[#1989e90a] text-[#1989E9] p-2 rounded-lg" onClick={onDownload}>Downdload</button>
        </div>
    )
}