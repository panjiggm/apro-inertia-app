import React from "react";
import Authenticated from "@/Layouts/Authenticated";
import Card from "@/Components/homepage/Card";
import { Head } from "@inertiajs/inertia-react";
import ClientStatus from "@/Components/homepage/ClientStatus";
import ClientTalk from "@/Components/homepage/ClientTalk";
import Insight from "@/Components/homepage/Insight";
import TopBreadcrumb from "@/Components/TopBreadcrumb";

export default function Dashboard(props) {
    return (
        <Authenticated
            auth={props.auth}
            errors={props.errors}
            header={
                <TopBreadcrumb />
            }
        >
            <Head title="Dashboard" />

            <div className="w-full">
                <div className="flex space-x-8">
                    <div className="flex-1">
                        <div className="flex space-x-4">
                            <Card label="0 Client" sublabel="All" />
                            <Card label="0 Client" sublabel="New" />
                            <Card label="0 Client" sublabel="On Review" />
                            <Card label="0 Client" sublabel="Rejected" />
                        </div>
                        <Insight />
                    </div>

                    <div className="w-96">
                        <ClientStatus />
                        <ClientTalk />
                    </div>
                </div>
            </div>
        </Authenticated>
    );
}
