import React, { useState } from "react";
import { ListItemInput, ListItemLink, ListItemSpace } from "@/Components/settings";
import WrapperSetting from "@/Components/WrapperSetting";
import { useForm } from "@inertiajs/inertia-react";
import Button from "@/Components/Button";

export default function Certification({ settings, ...props}) {
    const { data, setData, post, processing, errors } = useForm({
        registration_number: settings.registration_number,
        registration_date: settings.registration_date,
        date_of_protection_start: settings.date_of_protection_start,
        app_name: settings.app_name,
        registered_by: settings.registered_by,
        domain_name: settings.domain_name
    })

    const submit = () => {
        if (
            data.registration_number &&
            data.registration_date &&
            data.date_of_protection_start &&
            data.app_name &&
            data.registered_by &&
            data.domain_name
        ) {
            post(route('admin.settings.legal.certification.update'), data);
        }
    }

    return (
        <WrapperSetting {...props} title="Certification" titleBack="Legal & Regulatory" back={route('admin.settings.legal')} >
            <ul>
                <ListItemInput title="Registration Number" value={data.registration_number} onChange={e => setData('registration_number', e.target.value)} roundedTop={true} />
                <ListItemInput title="Registration Date" value={data.registration_date} onChange={e => setData('registration_date', e.target.value)} />
                <ListItemInput title="Date of Protection Start" value={data.date_of_protection_start} onChange={e => setData('date_of_protection_start', e.target.value)} borderBottom={false} roundedBottom={true} />
                <ListItemSpace />
                <ListItemInput title="Application Name" value={data.app_name} onChange={e => setData('app_name', e.target.value)} />
                <ListItemInput title="Registered by" value={data.registered_by} onChange={e => setData('registered_by', e.target.value)} borderBottom={false} roundedBottom={true} />
                <ListItemInput title="Domain Name" value={data.domain_name} onChange={e => setData('domain_name', e.target.value)} borderBottom={false} roundedBottom={true} />
            </ul>
            <div className="flex justify-end mt-7">
                <Button className="w-[150px] p-0 text-xs h-[30px]" onClick={submit}>Save</Button>
            </div>
        </WrapperSetting>
    );
}
