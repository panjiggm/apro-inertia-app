import React, { useState } from "react";
import WrapperSetting from "@/Components/WrapperSetting";

const header = {
    title: "User Type",
    columns: [
        "E.1",
        "E.2",
        "E.3",
        "E.4",
        "E.5",
        "E.6",
        "E.7",
        "E.8",
        "E.9",
        "E.10",
        "IARD",
        "ASO",
        "AC",
    ]
}

export default function CompletionAndReporting({ settings, ...props }) {
    return (
        <WrapperSetting {...props} title="E  Completion and Reporting" back={route('admin.settings.sign-off-setting')} >
            <div className="w-full m-auto bg-white">
                <ItemInput title={header.title} columns={header.columns} editable={false} header={true} />
                {
                    settings?.map((item, index) => (
                        <ItemInput key={index} title={item?.title} columns={item?.columns} />
                    ))
                }

            </div>
        </WrapperSetting>
    );
}

const ItemInput = ({ className = "", title, columns = [], header = false }) => {
    return (
        <div className="flex gap-1">
            <div className={`${className} w-[500px] ${header ? "text-white bg-[#1088e459] uppercase" : "text-[#464646BF] border-b-4 border-[#f3f4f694]"} p-3`}>
                {
                    title
                }
            </div>
            {
                columns.map((item) => (
                    <div className={`${className} flex-1 ${header ? "text-white bg-[#1088e459]" : "text-[#464646BF] border-b-4 border-[#f3f4f694]"} p-3`}>
                        {
                            <span className="text-center block">{item}</span>
                        }
                    </div>
                ))
            }
        </div>
    )
}