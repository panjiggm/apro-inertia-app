import React, { useState } from "react";
import WrapperSetting from "@/Components/WrapperSetting";
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import Button from "@/Components/Button";
import { useForm } from "@inertiajs/inertia-react";

export default function PrivacyPolicy({ settings = {}, ...props}) {
    const { data, setData, post, processing, errors } = useForm({
        value: settings?.value
    });

    const submit = () => {
        if (data.value) {
            data['key'] = 'privacy_policy'
            post(route('admin.settings.legal.update'), data);
        }
    }

    return (
        <WrapperSetting {...props} title="Privacy Policy" titleBack="Legal & Regulatory" back={route('admin.settings.legal')} >
            <div className="bg-white mb-6">
                <ReactQuill theme="snow" value={data.value} onChange={(e) => setData('value', e)} />
            </div>

            <div className="flex justify-end mt-7">
                <Button className="w-[150px] p-0 text-xs h-[30px]" onClick={submit}>Save</Button>
            </div>
        </WrapperSetting>
    );
}
