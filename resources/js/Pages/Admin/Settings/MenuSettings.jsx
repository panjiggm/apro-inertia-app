import React, { useState } from "react";
import { Inertia } from "@inertiajs/inertia";
import WrapperSetting from "@/Components/WrapperSetting";
import { ListItemSpace, ListItemSwicther } from "@/Components/settings";

export default function MenuSettings({ settings, ...props }) {
    const [swicther1, setSwicther1] = useState(settings.menu_b_1_2 == '1' ? true : false)
    const [swicther2, setSwicther2] = useState(settings.menu_b_1_3 == '1' ? true : false)
    const [swicther3, setSwicther3] = useState(settings.menu_e_6 == '1' ? true : false)
    const [swicther4, setSwicther4] = useState(settings.menu_e_7 == '1' ? true : false)

    const handleChange = (type) => {
        let params = {}
        switch(type) {
            case 'menu_b_1_2' :
                params['key'] = 'menu_b_1_2'
                params['value'] = !swicther1 === true ? '1' : '0'
                setSwicther1(!swicther1)
                break
            case 'menu_b_1_3' :
                params['key'] = 'menu_b_1_3'
                params['value'] = !swicther2 === true ? '1' : '0'
                setSwicther2(!swicther2)
                break
            case 'menu_e_6' :
                params['key'] = 'menu_e_6'
                params['value'] = !swicther3 === true ? '1' : '0'
                setSwicther3(!swicther3)
                break
            case 'menu_e_7' :
                params['key'] = 'menu_e_7'
                params['value'] = !swicther4 === true ? '1' : '0'
                setSwicther4(!swicther4)
                break
            default:
        }

        Inertia.post(route('admin.settings.menu-settings.update'), params);
    }

    return (
        <WrapperSetting {...props} title="APRO Menu Setting" back={route('admin.settings.index')} >
            <ul>
                <ListItemSwicther label="B.1.2   Fraud Considerations" onChange={() => handleChange('menu_b_1_2')} checked={swicther1} />
                <ListItemSwicther borderBottom={false} label="B.1.3   Going Concern Considerations" onChange={() => handleChange('menu_b_1_3')} checked={swicther2} />
                <ListItemSpace />
                <ListItemSwicther label="E.6  Key Audit Matters" onChange={() => handleChange('menu_e_6')} checked={swicther3} />
                <ListItemSwicther borderBottom={false} label="E.7  Audit Consultation" onChange={() => handleChange('menu_e_7')} checked={swicther4} />
            </ul>
        </WrapperSetting>
    );
}
