import React, { useState } from "react";
import { Inertia } from "@inertiajs/inertia";
import WrapperSetting from "@/Components/WrapperSetting";

export default function UserAccessControl({ settings, ...props }) {
    const [auditor, setAuditor] = useState(settings.user_access_auditor)
    const [manager, setManager] = useState(settings.user_access_manager)
    const [partner, setPartner] = useState(settings.user_access_partner)
    const [eqcrPartner, setEQCRPartner] = useState(settings.user_access_eqcr_partner)
    const [managingPartner, setManagingPartner] = useState(settings.user_access_managing_partner)
    const [riskManagementPartner, setRiskManagementPartner] = useState(settings.user_access_risk_management_partner)
    const [auditorExpert, setAuditorExpert] = useState(settings.user_access_auditor_expert)
    const [guess, setGuess] = useState(settings.user_access_guess)
    const [editable, setEditable] = useState(false)

    const handleChange = (parent, key, value) => {
        switch(parent) {
            case 'auditor':
                setAuditor({...auditor, [key]: value})
            break;
            case 'manager':
                setManager({...manager, [key]: value})
            break;
            case 'partner':
                setPartner({...partner, [key]: value})
            break;
            case 'eqcrPartner':
                setEQCRPartner({...eqcrPartner, [key]: value})
            break;
            case 'managingPartner':
                setManagingPartner({...managingPartner, [key]: value})
            break;
            case 'riskManagementPartner':
                setRiskManagementPartner({...riskManagementPartner, [key]: value})
            break;
            case 'auditorExpert':
                setAuditorExpert({...auditorExpert, [key]: value})
            break;
            case 'guess':
                setGuess({...guess, [key]: value})
            break;
            default:
        }
    }

    const handleSaveEdit = () => {
        if (editable) {
            // Update Data
            let params = {
                user_access_auditor: auditor,
                user_access_manager: manager,
                user_access_partner: partner,
                user_access_eqcr_partner: eqcrPartner,
                user_access_managing_partner: managingPartner,
                user_access_risk_management_partner: riskManagementPartner,
                user_access_auditor_expert: auditorExpert,
                user_access_guess: guess,
            }
            Inertia.post(route('admin.settings.user-access-control.update'), params)

            setEditable(false)
        } else {
            // local update
            setEditable(true)
        }
    }

    return (
        <WrapperSetting {...props} title="User Access Control" back={route('admin.settings.index')} >
            <div className="w-full m-auto">
                <div className="flex gap-1">
                    <div className="bg-[#1088e459] w-[500px] text-white uppercase p-3 text-center flex items-center justify-center">User Type</div>
                    <div className="bg-[#1088e459] flex-1 text-white uppercase p-3 text-center flex items-center justify-center">Can Assigned as an Admin?</div>
                    <div className="bg-[#1088e459] flex-1 text-white uppercase p-3 text-center flex items-center justify-center">Create</div>
                    <div className="bg-[#1088e459] flex-1 text-white uppercase p-3 text-center flex items-center justify-center">Read</div>
                    <div className="bg-[#1088e459] flex-1 text-white uppercase p-3 text-center flex items-center justify-center">Update</div>
                    <div className="bg-[#1088e459] flex-1 text-white uppercase p-3 text-center flex items-center justify-center">Delete</div>
                </div>

                <ItemInput
                    column1="Auditor"
                    column2={auditor.can_assigned_as_an_admin}
                    column3={auditor.create}
                    column4={auditor.read}
                    column5={auditor.update}
                    column6={auditor.delete}
                    editable={editable}
                    onChange2={(e) => handleChange('auditor', 'can_assigned_as_an_admin', e.target.value)}
                    onChange3={(e) => handleChange('auditor', 'create', e.target.value)}
                    onChange4={(e) => handleChange('auditor', 'read', e.target.value)}
                    onChange5={(e) => handleChange('auditor', 'update', e.target.value)}
                    onChange6={(e) => handleChange('auditor', 'delete', e.target.value)}
                />
                <ItemInput
                    column1="Manager"
                    column2={manager.can_assigned_as_an_admin}
                    column3={manager.create}
                    column4={manager.read}
                    column5={manager.update}
                    column6={manager.delete}
                    editable={editable}
                    onChange2={(e) => handleChange('manager', 'can_assigned_as_an_admin', e.target.value)}
                    onChange3={(e) => handleChange('manager', 'create', e.target.value)}
                    onChange4={(e) => handleChange('manager', 'read', e.target.value)}
                    onChange5={(e) => handleChange('manager', 'update', e.target.value)}
                    onChange6={(e) => handleChange('manager', 'delete', e.target.value)}
                />
                <ItemInput
                    column1="Partner"
                    column2={partner.can_assigned_as_an_admin}
                    column3={partner.create}
                    column4={partner.read}
                    column5={partner.update}
                    column6={partner.delete}
                    editable={editable}
                    onChange2={(e) => handleChange('partner', 'can_assigned_as_an_admin', e.target.value)}
                    onChange3={(e) => handleChange('partner', 'create', e.target.value)}
                    onChange4={(e) => handleChange('partner', 'read', e.target.value)}
                    onChange5={(e) => handleChange('partner', 'update', e.target.value)}
                    onChange6={(e) => handleChange('partner', 'delete', e.target.value)}
                />
                <ItemInput
                    column1="EQCR Partner"
                    column2={eqcrPartner.can_assigned_as_an_admin}
                    column3={eqcrPartner.create}
                    column4={eqcrPartner.read}
                    column5={eqcrPartner.update}
                    column6={eqcrPartner.delete}
                    editable={editable}
                    onChange2={(e) => handleChange('eqcrPartner', 'can_assigned_as_an_admin', e.target.value)}
                    onChange3={(e) => handleChange('eqcrPartner', 'create', e.target.value)}
                    onChange4={(e) => handleChange('eqcrPartner', 'read', e.target.value)}
                    onChange5={(e) => handleChange('eqcrPartner', 'update', e.target.value)}
                    onChange6={(e) => handleChange('eqcrPartner', 'delete', e.target.value)}
                />
                <ItemInput
                    column1="Managing Partner"
                    column2={managingPartner.can_assigned_as_an_admin}
                    column3={managingPartner.create}
                    column4={managingPartner.read}
                    column5={managingPartner.update}
                    column6={managingPartner.delete}
                    editable={editable}
                    onChange2={(e) => handleChange('managingPartner', 'can_assigned_as_an_admin', e.target.value)}
                    onChange3={(e) => handleChange('managingPartner', 'create', e.target.value)}
                    onChange4={(e) => handleChange('managingPartner', 'read', e.target.value)}
                    onChange5={(e) => handleChange('managingPartner', 'update', e.target.value)}
                    onChange6={(e) => handleChange('managingPartner', 'delete', e.target.value)}
                />
                <ItemInput
                    column1="Risk Management Partner"
                    column2={riskManagementPartner.can_assigned_as_an_admin}
                    column3={riskManagementPartner.create}
                    column4={riskManagementPartner.read}
                    column5={riskManagementPartner.update}
                    column6={riskManagementPartner.delete}
                    editable={editable}
                    onChange2={(e) => handleChange('riskManagementPartner', 'can_assigned_as_an_admin', e.target.value)}
                    onChange3={(e) => handleChange('riskManagementPartner', 'create', e.target.value)}
                    onChange4={(e) => handleChange('riskManagementPartner', 'read', e.target.value)}
                    onChange5={(e) => handleChange('riskManagementPartner', 'update', e.target.value)}
                    onChange6={(e) => handleChange('riskManagementPartner', 'delete', e.target.value)}
                />
                <ItemInput
                    column1="Auditor Expert"
                    column2={auditorExpert.can_assigned_as_an_admin}
                    column3={auditorExpert.create}
                    column4={auditorExpert.read}
                    column5={auditorExpert.update}
                    column6={auditorExpert.delete}
                    editable={editable}
                    onChange2={(e) => handleChange('auditorExpert', 'can_assigned_as_an_admin', e.target.value)}
                    onChange3={(e) => handleChange('auditorExpert', 'create', e.target.value)}
                    onChange4={(e) => handleChange('auditorExpert', 'read', e.target.value)}
                    onChange5={(e) => handleChange('auditorExpert', 'update', e.target.value)}
                    onChange6={(e) => handleChange('auditorExpert', 'delete', e.target.value)}
                />
                <ItemInput
                    column1="Guess"
                    column2={guess.can_assigned_as_an_admin}
                    column3={guess.create}
                    column4={guess.read}
                    column5={guess.update}
                    column6={guess.delete}
                    editable={editable}
                    onChange2={(e) => handleChange('guess', 'can_assigned_as_an_admin', e.target.value)}
                    onChange3={(e) => handleChange('guess', 'create', e.target.value)}
                    onChange4={(e) => handleChange('guess', 'read', e.target.value)}
                    onChange5={(e) => handleChange('guess', 'update', e.target.value)}
                    onChange6={(e) => handleChange('guess', 'delete', e.target.value)}
                />


                <div className="mt-5 flex gap-3 text-xs">
                    <button className="bg-[#4646460c] py-2 px-4 w-[100px] rounded-md text-[#464646]">Reset</button>
                    <button onClick={handleSaveEdit} className={`${editable ? "bg-[#00ff00e5]" : "bg-[#1088E4]"}  py-2 px-4 w-[100px] rounded-md text-white`}>{editable ? "Save" : "Edit"}</button>
                </div>

            </div>
        </WrapperSetting>
    );
}

const ItemInput = ({
    className = "",
    column1,
    column2,
    onChange2 = undefined,
    column3,
    onChange3 = undefined,
    column4,
    onChange4 = undefined,
    column5,
    onChange5 = undefined,
    column6,
    onChange6 = undefined,
    editable = false
}) => {
    const setValue = (value) => {
        let result = value
        if (value == '1') {
            result = 'Yes'
        } else if (value == '0') {
            result = 'No'
        }

        return result
    }

    return (
        <div className="flex gap-1">
            <div className={`${className} w-[500px] text-[#464646BF] bg-white border-b-4 border-[#f3f4f694] p-3`}>
                {
                    setValue(column1)
                }
            </div>
            <div className={`${className} flex-1 text-[#464646BF] bg-white border-b-4 border-[#f3f4f694] p-3`}>
                {
                    editable ? (
                        <select className="w-full" value={column2} onChange={onChange2}>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    ) : <span className="text-center block">{setValue(column2)}</span>
                }
            </div>
            <div className={`${className} flex-1 text-[#464646BF] bg-white border-b-4 border-[#f3f4f694] p-3`}>
                {
                    editable ? (
                        <select className="w-full" value={column3} onChange={onChange3}>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                            <option value="N/A">N/A</option>
                        </select>
                    ) : <span className="text-center block">{setValue(column3)}</span>
                }
            </div>
            <div className={`${className} flex-1 text-[#464646BF] bg-white border-b-4 border-[#f3f4f694] p-3`}>
                {
                    editable ? (
                        <select className="w-full" value={column4} onChange={onChange4}>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                            <option value="N/A">N/A</option>
                        </select>
                    ) : <span className="text-center block">{setValue(column4)}</span>
                }
            </div>
            <div className={`${className} flex-1 text-[#464646BF] bg-white border-b-4 border-[#f3f4f694] p-3`}>
                {
                    editable ? (
                        <select className="w-full" value={column5} onChange={onChange5}>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                            <option value="N/A">N/A</option>
                        </select>
                    ) : <span className="text-center block">{setValue(column5)}</span>
                }
            </div>
            <div className={`${className} flex-1 text-[#464646BF] bg-white border-b-4 border-[#f3f4f694] p-3`}>
                {
                    editable ? (
                        <select className="w-full" value={column6} onChange={onChange6}>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                            <option value="Admin Only">Admin Only</option>
                            <option value="N/A">N/A</option>
                        </select>
                    ) : <span className="text-center block">{setValue(column6)}</span>
                }
            </div>
        </div>
    )
}