import React, { useState } from "react";
import { Inertia } from "@inertiajs/inertia";
import WrapperSetting from "@/Components/WrapperSetting";

export default function RiskOfIncorrectAcceptance({ settings, ...props }) {
    const [editable, setEditable] = useState(false)
    const [currentData, setCurrentData] = useState(settings)

    const handleSaveEdit = () => {
        if (editable) {
            // Update Data
            let params = {
                data: currentData
            }
            Inertia.post(route('admin.settings.risk-of-incorrect-acceptance.update'), params)
            setEditable(false)
        } else {
            // local update
            setEditable(true)
        }
    }

    const handleChange = (risk, factor) => {
        const newArr = currentData.map(object => {
            if (object.risk === risk) {
                return { ...object, ["factor"]: factor };
            }
            return object;
        });
        setCurrentData(newArr)
    }

    return (
        <WrapperSetting {...props} title="Risk of Incorrect Acceptance (RIA)" back={route('admin.settings.index')} >
            <div className="w-[1000px] m-auto">
                <div className="flex gap-3">
                    <div className="bg-[#1088e459] w-[500px] text-white p-3 text-center">RISK OF INCORRECT ACCEPTANCE (%)</div>
                    <div className="bg-[#1088e459] flex-1 text-white p-3 text-center">FACTOR</div>
                </div>
                {
                    currentData.map((item,index) => (
                        <ItemInput
                            className={item?.bg1}
                            className2={item?.bg2}
                            text1={item?.risk}
                            text2={item?.factor}
                            onChange={(e) => handleChange(item?.risk, e.target.value)}
                            editable={editable}
                        />
                    ))
                }              


               

                <div className="mt-5 flex gap-3 text-xs">
                    <button className="bg-[#4646460c] py-2 px-4 w-[100px] rounded-md text-[#464646]">Reset</button>
                    <button onClick={handleSaveEdit} className={`${editable ? "bg-[#00ff00e5]" : "bg-[#1088E4]"}  py-2 px-4 w-[100px] rounded-md text-white`}>{editable ? "Save" : "Edit"}</button>
                </div>

            </div>
        </WrapperSetting>
    );
}

const ItemInput = ({ className = "", className2 = "", text1, text2, onChange = undefined, editable = false }) => {
    return (
        <div className="flex gap-3">
            <div className={`${className} w-[500px] text-[#464646BF] p-3`}>
                {
                    text1
                }
            </div>
            <div className={`${className2} flex-1 text-[#464646BF] p-3`}>
                {
                    editable ? (
                        <input type="text" value={text2} onChange={onChange} />
                    ) : (text2 + '%')
                }
            </div>
        </div>
    )
}