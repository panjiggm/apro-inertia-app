import React, { useState } from "react";
import { ListItemLink } from "@/Components/settings";
import WrapperSetting from "@/Components/WrapperSetting";

export default function Legal(props) {
    const [active, setActive] = useState(false)
    return (
        <WrapperSetting {...props} title="Legal & Regulatory" back={route('admin.settings.index')} >
            <ul>
                <ListItemLink title="Legal Notices" href={route("admin.settings.legal.legal-notice")} />
                <ListItemLink title="License" href={route("admin.settings.legal.license")}  />
                <ListItemLink title="Certification" href={route("admin.settings.legal.certification")} />
                <ListItemLink title="Privacy policy" href={route("admin.settings.legal.privacy-policy")} />
                <ListItemLink borderBottom={false} title="Term and conditions" href={route("admin.settings.legal.term-and-conditions")} />
            </ul>
        </WrapperSetting>
    );
}
