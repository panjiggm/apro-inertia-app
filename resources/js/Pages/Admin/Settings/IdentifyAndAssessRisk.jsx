import React, { useState } from "react";
import WrapperSetting from "@/Components/WrapperSetting";

const header = {
    title: "User Type",
    columns: [
        "B.1",
        "B.2",
        "B.3",
        "B.4",
        "B.5",
        "B.5",
        "B.5",
        "B.6",
        "B.7",
        "B.8",
        "B.9",
        "B.10",
        "B.11",
        "B.12",
        "B.13",
        "ERA",
    ]
}

export default function IdentifyAndAssessRisk({ settings, ...props }) {
    const [editable, setEditable] = useState(false)

    const handleSaveEdit = () => {
        if (editable) {
            // Update Data
            setEditable(false)
        } else {
            // local update
            setEditable(true)
        }
    }

    return (
        <WrapperSetting {...props} title="B. Identify and Assess Risk" back={route('admin.settings.sign-off-setting')} >
            <div className="w-full m-auto bg-white">
                <ItemInput title={header.title} columns={header.columns} editable={false} header={true} />
                {
                    settings?.map((item, index) => (
                        <ItemInput key={index} title={item?.title} columns={item?.columns} editable={editable} />
                    ))
                }

            </div>
        </WrapperSetting>
    );
}

const ItemInput = ({ className = "", title, columns = [], editable = false, header = false }) => {
    return (
        <div className="flex gap-1">
            <div className={`${className} w-[500px] ${header ? "text-white bg-[#1088e459] uppercase" : "text-[#464646BF] border-b-4 border-[#f3f4f694]"} p-3`}>
                {
                    title
                }
            </div>
            {
                columns.map((item) => (
                    <div className={`${className} flex-1 ${header ? "text-white bg-[#1088e459]" : "text-[#464646BF] border-b-4 border-[#f3f4f694]"} p-3`}>
                        {
                            <span className="text-center block">{item}</span>
                        }
                    </div>
                ))
            }
        </div>
    )
}