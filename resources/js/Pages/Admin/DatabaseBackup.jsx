import React from "react";
import Authenticated from "@/Layouts/Authenticated";
import { Head } from "@inertiajs/inertia-react";
import TopBreadcrumb from "@/Components/TopBreadcrumb";
import DataTable from "react-data-table-component";
import HeaderTable from "@/Components/clients/HeaderTable";

const columns = [
    {
        name: "NO",
        selector: (row) => row.id,
    },
    {
        name: "ID DATABASE",
        selector: (row) => row.id_db,
    },
    {
        name: "NAMA KAP",
        selector: (row) => row.nama_kap,
    },
    {
        name: "NPWP",
        selector: (row) => row.npwp,
    },
    {
        name: "TANGGAL UPDATE",
        selector: (row) => row.tgl_update,
    },
    {
        name: "JAM",
        selector: (row) => row.jam,
    },
    {
        name: "SIZE",
        selector: (row) => row.size,
    },
    {
        name: "TOTAL CLIENT",
        selector: (row) => row.total_client,
    },
    {
        name: "STATUS",
        selector: (row) => row.status,
    },
];

const data = [
    {
        id: 1,
        id_db: "23369874",
        nama_kap: "KAP Indonesia",
        npwp: "101058348365",
        tgl_update: "July 14, 2015",
        jam: "9:45",
        size: "341.4 Mb",
        total_client: "540 client",
        status: "complete",
    },
    {
        id: 1,
        id_db: "23369874",
        nama_kap: "KAP Indonesia",
        npwp: "101058348365",
        tgl_update: "July 14, 2015",
        jam: "9:45",
        size: "341.4 Mb",
        total_client: "540 client",
        status: "complete",
    },
    
    {
        id: 1,
        id_db: "23369874",
        nama_kap: "KAP Indonesia",
        npwp: "101058348365",
        tgl_update: "July 14, 2015",
        jam: "9:45",
        size: "341.4 Mb",
        total_client: "540 client",
        status: "complete",
    },
    
    {
        id: 1,
        id_db: "23369874",
        nama_kap: "KAP Indonesia",
        npwp: "101058348365",
        tgl_update: "July 14, 2015",
        jam: "9:45",
        size: "341.4 Mb",
        total_client: "540 client",
        status: "complete",
    },
    
    {
        id: 1,
        id_db: "23369874",
        nama_kap: "KAP Indonesia",
        npwp: "101058348365",
        tgl_update: "July 14, 2015",
        jam: "9:45",
        size: "341.4 Mb",
        total_client: "540 client",
        status: "complete",
    },
    
];

const customStyle = {
    headCells: {
        style: {
            background: "linear-gradient(180deg, #00CB75 0%, #02A962 100%)",
            color: "#FFFFFF",
            fontSize: 10,
            fontWeight: 700,
        },
    },
};


export default function DatabaseBackup(props) {
    return (
        <Authenticated
            auth={props.auth}
            errors={props.errors}
            header={
                <TopBreadcrumb current="Database Backup" />
            }
        >
            <Head title="Database Backup" />

            <div className="w-full">
                <div className="flex space-x-8">
                <div className="border border-[#E6E6E8] rounded-2xl w-full bg-white shadow-md">
                        <HeaderTable
                            title="Database"
                            textButton="Export"
                            onClick={() => {}}
                        />
                        <div className="">
                            <DataTable
                                columns={columns}
                                customStyles={customStyle}
                                data={data}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </Authenticated>
    );
}
