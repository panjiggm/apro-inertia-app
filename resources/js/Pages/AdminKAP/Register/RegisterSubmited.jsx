export default function RegisterSubmited() {
    return (
        <div className="">
            <div className="p-8 text-center flex items-center flex-col w-full">
                <img src="/images/logo.png" alt="APPRO" className="w-40" />
            </div>
            <div className="mt-16 text-center flex-col flex items-center">
                <img src="/images/register-submited.png" />
                <h1 className="my-4 text-[22px] font-semibold text-[#2A2A2A]">Registration Submitted</h1>
                <p className="text-sm text-[#89A3BB] mb-8">Your registration request has been submitted, <br />please check the activation link in your registered email, thank you.</p>
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="75"
                    height="75"
                    fill="none"
                    viewBox="0 0 75 75"
                >
                    <path
                        fill="#1088E4"
                        d="M30.71 48.657l23.181-24.846a1.136 1.136 0 011.481-.13l5.54 4.173a.88.88 0 01.117 1.332L38.53 53.302a1.135 1.135 0 01-1.481.13l-6.34-4.775z"
                    ></path>
                    <path
                        fill="#1088E4"
                        d="M25.423 34.784a1.136 1.136 0 011.48-.13l9.91 7.465-6.095 6.533-9.91-7.464a.88.88 0 01-.116-1.332l4.73-5.072z"
                    ></path>
                    <circle
                        cx="37.5"
                        cy="37.5"
                        r="33.5"
                        stroke="#1088E4"
                        strokeWidth="8"
                    ></circle>
                </svg>
            </div>
        </div>

    )
}