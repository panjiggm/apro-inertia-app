import TopBreadcrumb from "@/Components/TopBreadcrumb";
import RegisterLayout from "@/Layouts/RegisterLayout";
import { Head } from "@inertiajs/inertia-react";

export default function DocumentVerification(props) {

    return (
        <RegisterLayout
            step={3}
            auth={props.auth}
            errors={props.errors}
            header={
                <TopBreadcrumb home="Document Verification" />
            }
        >
            <Head title="Document Verification" />
            <div className="w-full flex justify-center items-center flex-col pt-16">
                <img src="/images/register-submited.png" />
                <h1 className="my-4 text-[22px] font-semibold text-[#2A2A2A]">Please wait a moment...!</h1>
                <p className="text-sm text-[#89A3BB] mb-8 text-center">Your files have been sent, please wait<br/>
                    verification from us, thank you.</p>
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="76"
                    height="75"
                    fill="none"
                    viewBox="0 0 76 75"
                >
                    <path
                        fill="#EFEFF1"
                        d="M75.5 37.5C75.5 58.21 58.71 75 38 75S.5 58.21.5 37.5 17.29 0 38 0s37.5 16.79 37.5 37.5zM8 37.5c0 16.569 13.431 30 30 30 16.569 0 30-13.431 30-30 0-16.569-13.431-30-30-30-16.569 0-30 13.431-30 30z"
                    ></path>
                    <path
                        fill="#FFC107"
                        d="M67.878 54.75c1.435.828 1.936 2.671.996 4.035A37.5 37.5 0 1138.194 0c1.656.009 2.881 1.474 2.74 3.125l-.127 1.495c-.141 1.65-1.596 2.859-3.252 2.883A30 30 0 1062.353 55.02c.968-1.345 2.791-1.847 4.226-1.019l1.299.75z"
                    ></path>
                </svg>
            </div>
        </RegisterLayout>
    );
}