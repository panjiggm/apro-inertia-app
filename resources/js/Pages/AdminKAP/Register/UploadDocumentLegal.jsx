import React, {useState} from "react";
import Card from "@/Components/homepage/Card";
import { Head } from "@inertiajs/inertia-react";
import ClientStatus from "@/Components/homepage/ClientStatus";
import ClientTalk from "@/Components/homepage/ClientTalk";
import Insight from "@/Components/homepage/Insight";
import TopBreadcrumb from "@/Components/TopBreadcrumb";
import RegisterLayout from "@/Layouts/RegisterLayout";
import TabCompanyInformation from "@/Components/TabCompanyInformation";
import ButtonDocument from "@/Components/ButtonDocument";
import MyDropzone from "@/Components/MyDropzone";
import ValidationErrors from '@/Components/ValidationErrors';
import { Inertia } from "@inertiajs/inertia";
import { usePage } from '@inertiajs/inertia-react'

export default function UploadDocumentLegal(props) {
    const [taxId, setTaxId] = useState(null)
    const [companyLicence, setCompanyLicence] = useState(null)
    const [businessId, setBusinessId] = useState(null)
    const [partnerCitizenIdCard, setPartnerCitizenIdCard] = useState(null)
    const [partnerBusinessIdCard, setPartnerBusinessIdCard] = useState(null)
    const [picCitizenIdCard, setPicCitizenIdCard] = useState(null)
    const [picBusinessIdCard, setPicBusinessIdCard] = useState(null)
    const [term, setTerm] = useState(false)
    const [privacy, setPrivacy] = useState(false)
    const { errors } = usePage().props

    const onChangeFile = (name, files) => {
        switch(name) {
            case 'tax_id' :
                setTaxId(files[0])
                break
            case 'company_licence' :
                setCompanyLicence(files[0])
                break
            case 'business_id' :
                setBusinessId(files[0])
                break
            case 'partner_citizen_id_card' :
                setPartnerCitizenIdCard(files[0])
                break
            case 'partner_business_id_card' :
                setPartnerBusinessIdCard(files[0])
                break
            case 'pic_citizen_id_card' :
                setPicCitizenIdCard(files[0])
                break
            case 'pic_business_id_card' :
                setPicBusinessIdCard(files[0])
                break
            default:
        }
    }

    const handleSubmit = () => {
        let params = {
            tax_id: taxId,
            company_licence: companyLicence,
            business_id: businessId,
            partner_citizen_id_card: partnerCitizenIdCard,
            partner_business_id_card: partnerBusinessIdCard,
            pic_citizen_id_card: picCitizenIdCard,
            pic_business_id_card: picBusinessIdCard,
            privacy: privacy,
            term_and_conditions: term
        }

        Inertia.post(route('register.upload-document-legal.store'), params)
    }

    return (
        <RegisterLayout
            step={2}
            auth={props.auth}
            errors={props.errors}
            header={
                <TopBreadcrumb home="Account Register" />
            }
        >
            <Head title="Upload Document Legal" />

            <ValidationErrors errors={errors} />

            <div className="w-full">
                <h1 className="text-[#464646] text-sm text-center mb-10">Upload Legal Document</h1>
                <h2 className="text-[#2A2A2A] font-medium text-sm mb-2">Please complete the following document:</h2>
                <ItemUpload onChange={(e) => onChangeFile('tax_id', e)} title="1. Tax ID" />
                <ItemUpload onChange={(e) => onChangeFile('company_licence', e)} title="2. Company Licence (Decision Letter from the Ministry of Finance regarding business license of public accounting firm, or Decision Letter from the Ministry of law and human right if the company is  limited liability company)." />
                <ItemUpload onChange={(e) => onChangeFile('business_id', e)} title="3. Business Identification Number (NIB) from Ministry of Investment." />
                <ItemUpload onChange={(e) => onChangeFile('partner_citizen_id_card', e)} title="4. Citizen ID Card of the Managing Partner/Partner who responsible for registration with APRO." />
                <ItemUpload onChange={(e) => onChangeFile('partner_business_id_card', e)} title="5. Business ID Card of the Managing Partner/Partner who responsible for registration with APRO." />
                <ItemUpload onChange={(e) => onChangeFile('pic_citizen_id_card', e)} title="6. Citizen ID Card of the Person-in-charge (PIC) who responsible as an admin in APRO." />
                <ItemUpload onChange={(e) => onChangeFile('pic_business_id_card', e)} title="7. Business ID Card of the Person-in-charge (PIC) who responsible as an admin in APRO" />
                <div className="text-[#2A2A2A] text-sm">
                    <span className=" mb-3 block">Please click the following privacy policy, term and conditions, as a sign of approval that you are agree with us for using apro software.  </span>
                    <div className="flex flex-col text-sm gap-3 font-normal">
                        <Checkbox onClick={() => setPrivacy(!privacy)} label="Privacy policy" active={privacy} />
                        <Checkbox onClick={() => setTerm(!term)} label="Term and conditions<" active={term} />
                    </div>
                </div>
                <div className="flex mt-7">
                    <div className="flex">
                        <button onClick={() => {}} className="py-4 px-7 text-[#1088E4] rounded-lg font-medium">Back</button>
                        <button disabled={!term || !privacy} onClick={handleSubmit} className="py-4 px-7 disabled:bg-gray-300w bg-[#1088E4] text-white rounded-lg font-semibold">Submit</button>
                    </div>
                </div>
            </div>
        </RegisterLayout>
    );
}

const ItemUpload = ({ title, onChange }) => {
    return (
        <div className="">
            <span className="text-sm text-[#2A2A2A] mb-6 block">{title}</span>
            <MyDropzone onChange={onChange} />
        </div>
    )
}

const Checkbox = ({ label, active = false, onClick = undefined }) => {
    return (
        <button onClick={onClick} className="flex gap-3 w-full">
            {
                active ? (
                    <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M7.37061 11.6772L12.4154 6.26981C12.7922 5.86598 13.4552 5.80798 13.8963 6.14025L14.0118 6.22719C14.4529 6.55947 14.5051 7.1562 14.1284 7.56003L9.76575 12.2363C9.389 12.6401 8.72596 12.6981 8.28481 12.3658L7.37061 11.6772Z" fill="#1088E4" />
                        <path d="M5.58273 8.90454C5.95948 8.50071 6.62251 8.4427 7.06366 8.77497L8.83492 10.1091L7.37201 11.6771L5.60075 10.343C5.1596 10.0108 5.1074 9.41404 5.48415 9.01021L5.58273 8.90454Z" fill="#1088E4" />
                        <circle cx="9" cy="9" r="8" stroke="#1088E4" strokeWidth="2" />
                    </svg>
                ) : (
                    <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="9" cy="9" r="8" stroke="#464646" strokeOpacity="0.4" strokeWidth="2" />
                    </svg>
                )
            }
            <span>{label}</span>
        </button>
    )
}
