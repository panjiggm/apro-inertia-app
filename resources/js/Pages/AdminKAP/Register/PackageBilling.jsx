import Input from "@/Components/Input";
import InputSelect from "@/Components/InputSelect";
import TopBreadcrumb from "@/Components/TopBreadcrumb";
import RegisterLayout from "@/Layouts/RegisterLayout";
import { formatRupiah } from "@/utlis/common";
import { Inertia } from "@inertiajs/inertia";
import { Head } from "@inertiajs/inertia-react";
import React, { useState } from "react"

const optionsYears = [
    {
        label: "1 Year", value: "1"
    },
    {
        label: "2 Year", value: "2"
    },
    {
        label: "3 Year", value: "3"
    },
    {
        label: "5 Year", value: "5"
    }
]

export default function PackageBilling({ packages, add_ons, ...props }) {
    const [selectedItem, setSelectedItem] = useState(packages[0])
    const [selectedYear, setSelectedYear] = useState(optionsYears[0])
    const [selectedAddons, setSelectedAddons] = useState(add_ons.map(item => {
        return ({
            ...item,
            qty: "0"
        })
    }))

    const handleChangeAddons = (key, value) => {
        const temp = [...selectedAddons]
        temp[key].qty = value
        setSelectedAddons(temp)
    }

    const handleSubmit = () => {
        let params = {
            package_id: selectedItem.value,
            period: selectedYear.value,
            add_ons: selectedAddons,
            voucher_code: ''
        }

        Inertia.post(route('register.package-billing.store'), params)
    }

    return (
        <RegisterLayout
            step={4}
            auth={props.auth}
            errors={props.errors}
            header={
                <TopBreadcrumb home="Upload Document" />
            }
        >
            <Head title="Package & Billing" />
            <div className="flex space-x-8">
                <div className="flex-1 bg-white rounded-2xl border px-5">
                    <div className="">
                        <Title label="Package" className="my-6" />
                        <InputSelect options={packages} value={selectedItem} onChange={(item) => setSelectedItem(item)} />
                        <div className="border rounded-md bg-white p-5 text-sm text-[#464646] font-normal mt-5">
                            <span className="block">Prices : Rp {selectedItem.price}</span>
                            <span>Detail Package:</span>
                            <ul>
                                {
                                    selectedItem.features.map((item, index) => (
                                        <li key={`item-${index}`}>{item}</li>
                                    ))
                                }
                            </ul>
                        </div>
                        <Title label="Additional Service" className="my-6" />
                        {
                            selectedAddons.map((item, index) => (
                                <ItemAddons key={`item-${index}`} label={`${item.name} (${item.description})`} value={item?.qty} onChange={(e) => handleChangeAddons(index, e.target.value)} />
                            ))
                        }

                        <Title label="Periode" className="my-6" />
                        <InputSelect options={optionsYears} value={selectedYear} onChange={(item) => setSelectedYear(item)} />

                        <Title label="Voucher Discount" className="my-6" />
                        <Input
                            type="text"
                            name="user"
                            className="block w-full"
                            autoComplete="item-addons"
                            isFocused={true}
                            placeholder={"Voucher Discount"}
                        />

                        <div className="flex mt-7 mb-5">
                            <div className="flex justify-end items-end flex-1">
                                <button onClick={() => { }} className="py-4 px-7 text-[#1088E4] rounded-lg font-medium">Cancel</button>
                                <button onClick={handleSubmit} className="py-4 px-7 disabled:bg-gray-300w bg-[#1088E4] text-white rounded-lg font-semibold">Checkout</button>
                            </div>
                        </div>
                    </div>
                </div>
                <Summary selectedItem={selectedItem} selectedYear={selectedYear} selectedAddons={selectedAddons} />
            </div>
        </RegisterLayout>
    );
}

const Summary = ({ selectedItem, selectedYear, selectedAddons }) => {
    const showAddons = selectedAddons.filter(item => (item.qty !== "0"))
    const totalMonth = (selectedYear?.value * 12)
    const totalItem = selectedItem?.price * totalMonth
    const totalAddons = showAddons.reduce((a, b) => a + (Number(totalMonth) * (Number(b.price) * Number(b.qty))), 0)
    const total = totalItem + totalAddons
    const vat = 0.11 * total
    const adminFee = 10000
    const totalBilling = total + vat + adminFee
    return (
        <div className="flex-1">
            <div className=" bg-white rounded-2xl border px-5 pb-5">
                <Title label="Subscription Summary " className="my-6" />
                <ItemSummary rows={["Type", `: ${selectedItem?.label}`, `${totalMonth} Month`, formatRupiah(totalItem)]} />
                {
                    showAddons.map((item) => (
                        <ItemSummary rows={[item?.name, `: ${item?.qty} ${item?.unit ?? ""}`, `${totalMonth} Month`, formatRupiah(totalMonth * (Number(item.price) * Number(item.qty)))]} />
                    ))
                }
                <hr />
                <ItemSummaryTotal rows={["Total", formatRupiah(totalItem + totalAddons)]} bold={true} />
                <ItemSummaryTotal rows={["VAT (11%)", formatRupiah(vat)]} />
                {/* <ItemSummaryTotal rows={["Discount", "(Rp68.000)"]} /> */}
                <ItemSummaryTotal rows={["Admin fee", formatRupiah(adminFee)]} />
                <hr />
                <ItemSummaryTotal rows={["Total Billing", formatRupiah(totalBilling)]} bold={true} />
            </div>
        </div>
    )
}

const Title = ({ className = "", label = "" }) => {
    return (<h2 className={`text-xl font-semibold text-[#2A2A2A] ${className}`}>{label}</h2>)
}

const ItemAddons = ({ label, value = "", onChange = undefined, placeholder = undefined }) => {
    return (
        <div className="flex items-center mb-6">
            <div className="flex-1">
                <span className="text-sm text-[#2A2A2A] font-normal">{label}</span>
            </div>
            <div className="w-[100px]">
                <Input
                    type="text"
                    name="user"
                    value={value}
                    handleChange={onChange}
                    className="block w-full"
                    autoComplete="item-addons"
                    isFocused={true}
                    placeholder={placeholder}
                />
            </div>
        </div>
    )
}

const ItemSummary = ({ rows = [] }) => {
    return (
        <div className="flex">
            {
                rows.map((item, index) => (
                    <div key={`item-summary-${index}`} className={`flex-1 text-sm text-[#2A2A2A] p-3 ${index === rows.length - 1 && "text-right"}`}>
                        <span>{item}</span>
                    </div>
                ))
            }
        </div>
    )
}

const ItemSummaryTotal = ({ rows = [], bold = false }) => (
    <div className="flex justify-between">
        {
            rows.map((item, index) => (
                <div key={`item-summary-${index}`} className={`flex-1 text-sm text-[#2A2A2A] p-3 ${bold && "font-bold"} ${index === rows.length - 1 && "text-right"}`}>
                    <span>{item}</span>
                </div>
            ))
        }
    </div>
)