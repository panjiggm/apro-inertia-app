import React from "react";
import Card from "@/Components/homepage/Card";
import { Head } from "@inertiajs/inertia-react";
import ClientStatus from "@/Components/homepage/ClientStatus";
import ClientTalk from "@/Components/homepage/ClientTalk";
import Insight from "@/Components/homepage/Insight";
import TopBreadcrumb from "@/Components/TopBreadcrumb";
import RegisterLayout from "@/Layouts/RegisterLayout";
import TabCompanyInformation from "@/Components/TabCompanyInformation";

export default function AccountRegistration(props) {
    return (
        <RegisterLayout
            step={1}
            auth={props.auth}
            errors={props.errors}
            header={
                <TopBreadcrumb home="Registration" />
            }
        >
            <Head title="Company Information" />

            <div className="w-full">
               <TabCompanyInformation />
            </div>
        </RegisterLayout>
    );
}
