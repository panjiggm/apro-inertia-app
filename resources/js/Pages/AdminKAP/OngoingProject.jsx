import React, { useState } from "react";
import Authenticated from "@/Layouts/Authenticated";
import { Head } from "@inertiajs/inertia-react";
import DataTable from "react-data-table-component";
import HeaderTable from "@/Components/clients/HeaderTable";
import TopBreadcrumb from "@/Components/TopBreadcrumb";

// Dummy Data
import dummyData from "@/DummyData/clients.json";
import Modal from "@/Components/clients/Modal";
import TabModal from "@/Components/clients/TabModal";
import { Inertia } from "@inertiajs/inertia";
import moment from "moment";

const columns = [
    {
        name: "NO",
        width: "75px",
        selector: (row) => row.id,
    },
    {
        name: "ID CLIENT",
        width: "120px",
        selector: (row) => row.client.id,
    },
    {
        name: "NAMA CLIENT",
        width: "200px",
        selector: (row) => row.client.name,
    },
    {
        name: "NPWP",
        width: "180px",
        selector: (row) => row.client.tax_id_number,
    },
    {
        name: "TANGGAL",
        selector: (row) => moment(row.created_at).format("LL"),
    },
    {
        name: "STATUS",
        selector: (row) => (
            <span className="p-1 text-xs bg-green-600/80 text-white rounded-lg">{row.status}</span>
        ),
    },
];

const customStyle = {
    headCells: {
        style: {
            background: "linear-gradient(180deg, #00CB75 0%, #02A962 100%)",
            color: "#FFFFFF",
            fontSize: 10,
            fontWeight: 700,
        },
    },
};

export default function OngoingProject({ projects, ...props }) {
    console.log('projects', projects)
    const [isOpen, setIsOpen] = useState(false);

    function closeModal() {
        setIsOpen(false);
    }

    function openModal() {
        setIsOpen(true);
    }

    const handlePageChange = page => {
        Inertia.get(route('company.clients.index'), { page })
    };

    return (
        <Authenticated
            auth={props.auth}
            errors={props.errors}
            header={
                <TopBreadcrumb current="Clients" />
            }
        >
            <Head title="Clients" />

            <div className="w-full">
                <div className="flex space-x-8">
                    <div className="border border-[#E6E6E8] rounded-2xl w-full bg-white shadow-md">
                        <HeaderTable
                            title="Ongoing Project"
                        />
                        <div className="">
                            <DataTable
                                columns={columns}
                                customStyles={customStyle}
                                data={projects.data}
                                pagination
                                paginationServer
                                paginationTotalRows={projects.total}
                                paginationPerPage={projects.per_page}
                                onChangePage={handlePageChange}
                                paginationDefaultPage={projects.current_page}
                                paginationComponentOptions={{
                                    noRowsPerPage: true
                                }}
                            />
                        </div>
                    </div>
                </div>
            </div>

            <Modal open={isOpen} onClose={closeModal}>
                <div className="">
                    <div className="flex justify-between">
                        <h2 className="text-lg font-semibold">Tambah Klien</h2>
                        <button
                            className="w-6 h-6"
                            onClick={closeModal}
                            as="button"
                        >
                            <svg
                                width="14"
                                height="14"
                                viewBox="0 0 14 14"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    d="M13.295 0.705C12.9056 0.315639 12.2744 0.315639 11.885 0.705L7 5.59L2.115 0.705C1.72564 0.315639 1.09436 0.315639 0.705 0.705C0.315639 1.09436 0.315639 1.72564 0.705 2.115L5.59 7L0.705 11.885C0.315639 12.2744 0.315639 12.9056 0.705 13.295C1.09436 13.6844 1.72564 13.6844 2.115 13.295L7 8.41L11.885 13.295C12.2744 13.6844 12.9056 13.6844 13.295 13.295C13.6844 12.9056 13.6844 12.2744 13.295 11.885L8.41 7L13.295 2.115C13.6844 1.72564 13.6844 1.09436 13.295 0.705Z"
                                    fill="#2A2A2A"
                                />
                            </svg>
                        </button>
                    </div>
                    <TabModal closeModal={closeModal} />
                </div>
            </Modal>
        </Authenticated>
    );
}
