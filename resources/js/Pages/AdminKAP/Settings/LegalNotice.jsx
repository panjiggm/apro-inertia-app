import React, { useState } from "react";
import WrapperSetting from "@/Components/WrapperSetting";

export default function LegalNotice({ settings, ...props}) {
    return (
        <WrapperSetting {...props} title="Legal Notice" titleBack="Legal & Regulatory" back={route('company.settings.legal')} >
           <div className="bg-white p-5 rounded-md" dangerouslySetInnerHTML={{__html: settings}} />
        </WrapperSetting>
    );
}
