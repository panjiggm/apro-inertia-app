import React, { useState } from "react";
import { ListItemLink } from "@/Components/settings";
import WrapperSetting from "@/Components/WrapperSetting";

export default function SoftwareUpdate({ settings, ...props }) {
    return (
        <WrapperSetting {...props} title="Fonts" back={route('company.settings.index')} >
            <ul>
                <ListItemLink href={route('company.settings.fonts.type',{ selected : settings.font_type})} title="Font Type" value={settings.font_type} borderBottom={false} />
            </ul>
        </WrapperSetting>
    );
}
