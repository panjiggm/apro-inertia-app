import React from "react";
import { ListItem, ListItemSpace } from "@/Components/settings";
import WrapperSetting from "@/Components/WrapperSetting";

export default function About({ settings, ...props }) {
    return (
        <WrapperSetting {...props} title="About" back={route('company.settings.index')} >
            <ul>
                <ListItem title="Name" value={settings.app_name} roundedTop={true} />
                <ListItem title="Software Version" value={settings.software_version} />
                <ListItem title="Model Name" value={settings.model_name} borderBottom={false} roundedBottom={true} />
                <ListItemSpace />
                <ListItem title="Subscription Type" value={settings.subscription_type} roundedTop={true} />
                <ListItem title="Expired Date" value={settings.subscription_expired_date} borderBottom={false} roundedBottom={true} />
                <ListItemSpace />
                <ListItem title="Security Version" value={settings.security_version} roundedTop={true} />
                <ListItem title="Expirate Date" value={settings.security_expirate_date} borderBottom={false} roundedBottom={true} />
            </ul>
        </WrapperSetting >
    );
}
