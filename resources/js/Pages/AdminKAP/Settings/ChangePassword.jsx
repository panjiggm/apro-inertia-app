import React, { useState } from "react";
import { Inertia } from "@inertiajs/inertia";
import WrapperSetting from "@/Components/WrapperSetting";

export default function ChangePassword({ settings, ...props }) {
    const [oldPassword, setOldPassword] = useState("")
    const [newPassword, setNewPassword] = useState("")

    const handleSave = () => {
        let params = {
            old_password: oldPassword,
            new_password: newPassword
        }
        Inertia.post(route('company.settings.change-password.update'), params)
    }

    return (
        <WrapperSetting {...props} title="Change Password" back={route('company.settings.index')} >
            <div className="bg-white border rounded-lg p-5 m-auto w-[500px]">
                <InputWithEye value={oldPassword} label="Old Password" onChange={(e) => setOldPassword(e.target.value)} />
                <InputWithEye value={newPassword} label="New Password" onChange={(e) => setNewPassword(e.target.value)} />
                <button onClick={handleSave} className={`bg-[#1088E4] w-full py-2 px-4 rounded-md text-white`}>Save</button>
            </div>
        </WrapperSetting >
    );
}

const InputWithEye = ({ value, onChange, label }) => {
    const [typeInput2, setTypeInput2] = useState("password")
    return (
        <div className="flex mb-4 border rounded-lg p-3">
            <input type={typeInput2} value={value} onChange={(e) => onChange(e)} className="w-full border-none" placeholder={label} />
            <button onClick={() => setTypeInput2(typeInput2 === "text" ? "password" : "text")}>
                {
                    typeInput2 === "text" ?
                        (<svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="32"
                            height="32"
                            viewBox="0 0 32 32"
                        >
                            <g fill="none" stroke="none">
                                <g fill="#000">
                                    <path d="M8.109 20.891C4.617 18.831 3 16 3 16s4-7 13-7c1.305 0 2.504.147 3.601.399L16.9 12.1a4 4 0 00-4.797 4.797l-3.993 3.993zm4.29 1.71c1.097.252 2.296.399 3.601.399 9 0 13-7 13-7s-1.617-2.83-5.109-4.891L19.9 15.1a4 4 0 01-4.797 4.797l-2.703 2.703zM19 16a2.99 2.99 0 01-.879 2.121A2.99 2.99 0 0116 19l3-3zm-3-3a2.99 2.99 0 00-2.121.879A2.99 2.99 0 0013 16l3-3zm8-6L7 24l1 1L25 8l-1-1z"></path>
                                </g>
                            </g>
                        </svg>)
                        :
                        (<svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="32"
                            height="32"
                            viewBox="0 0 32 32"
                        >
                            <g fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
                                <g fill="#000">
                                    <path d="M17 9C8 9 4 16 4 16s4 7 13 7 13-7 13-7-4-7-13-7zm0 11a4 4 0 100-8 4 4 0 000 8zm0-1a3 3 0 100-6 3 3 0 000 6zm0-2a1 1 0 100-2 1 1 0 000 2z"></path>
                                </g>
                            </g>
                        </svg>)
                }
            </button>
        </div>
    )
}
