import React, { useState } from "react";
import WrapperSetting from "@/Components/WrapperSetting";
import ButtonDocument from "@/Components/ButtonDocument";

export default function License({ settings, ...props}) {
    return (
        <WrapperSetting {...props} title="Permits & Legal Documents" titleBack="Company Profile" back={route('company.settings.company')} >
           <div>
           <div className="my-0 mx-auto max-w-[800px] flex gap-3 justify-center mt-12 ">
                    <ButtonDocument href={route('company.documents.type',{type : "legalitas"})} title="5 Docs" image="/images/document_uploaded.png" description="Scan Legalitas Dokumen" />
                    <ButtonDocument href={route('company.documents.type',{type : "npwp"})} title="0 Docs" image="/images/document.png" description="Scan NPWP" />
                    <ButtonDocument href={route('company.documents.type',{type : "identitas"})} title="0 Docs" image="/images/document.png" description="Scan Identitas" />
                </div>

                <div className="my-0 mx-auto max-w-[400px] flex gap-2 mt-16">
                    <button className="flex-1 text-sm font-medium text-[#1088E4] p-4 bg-transparent">Delete</button>
                    <button className="flex-1 text-sm font-medium text-white p-4 bg-[#1088E4] rounded-lg">Edit</button>
                </div>
           </div>
        </WrapperSetting>
    );
}
