import React from "react";
import { ListItem, ListItemLink, ListItemSpace } from "@/Components/settings";
import WrapperSetting from "@/Components/WrapperSetting";

export default function LanguageRegion({ settings, ...props }) {
    return (
        <WrapperSetting {...props} title="Language & Region" back={route('company.settings.index')} >
            <ul>
                <ListItemLink borderBottom={false} href={route('company.settings.language.detail')} title="APRO Language" value={settings.app_language} />
                <ListItemSpace />
                <ListItemLink href={route('company.settings.language.region')} title="Region" value={settings.region} />
                <ListItem title="Calendar" value={settings.calendar} borderBottom={false} />
            </ul>
        </WrapperSetting>
    );
}
