import React, { useState } from "react";
import { Inertia } from "@inertiajs/inertia";
import { ListRadiobox } from "@/Components/settings";
import WrapperSetting from "@/Components/WrapperSetting";

const plans = [
    {
        name: 'Indonesia',
    },
    {
        name: 'English',
    },
]

export default function Language(props) {
    const [selected, setSelected] = useState(plans[1])

    const handleChange = (value) => {
        setSelected(value)
        let params = {
            key: 'app_language',
            value: value.name
        }

        Inertia.post(route('company.settings.language.update'), params);
    }

    return (
        <WrapperSetting {...props} title="Language" titleBack="Language & Region" back={route('company.settings.language')} >
            <ListRadiobox selected={selected} setSelected={handleChange} data={plans} />
        </WrapperSetting>
    );
}
