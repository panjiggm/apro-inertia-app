import React, { useState } from "react";
import WrapperSetting from "@/Components/WrapperSetting";

export default function Certification({ settings, ...props}) {
    return (
        <WrapperSetting {...props} title="Privacy Policy" titleBack="Company Profile" back={route('company.settings.company')} >
           <div className="bg-white p-5 rounded-md" dangerouslySetInnerHTML={{__html: settings}} />
        </WrapperSetting>
    );
}
