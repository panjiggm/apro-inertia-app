import React, { useState } from "react";
import { Inertia } from "@inertiajs/inertia";
import { ListItemSwicther } from "@/Components/settings";
import WrapperSetting from "@/Components/WrapperSetting";

export default function Likelihood({ settings, ...props }) {
    const [active, setActive] = useState(settings == 'three-cell' ? true : false)

    const handleChange = () => {
        let params = {
            likelihood: !active === true ? 'three-cell' : 'five-cell'
        }
        setActive(!active)

        Inertia.post(route('company.settings.likelihood.update'), params);
    }

    return (
        <WrapperSetting {...props} title="Likelihood & Magnitude" back={route('company.settings.index')} >
            <div className="">
                <ul>
                    <ListItemSwicther label="Three-Cell-Matrix Type" roundedBottom={true} roundedTop={true} onChange={() => handleChange()} checked={active} />
                </ul>
                <div className="flex gap-3 mt-5">
                    <ItemTable title="LIKELIHOOD" data={[
                        {
                            label: "Low",
                            value: 1
                        },
                        {
                            label: "Moderate",
                            value: 2
                        },
                        {
                            label: "High",
                            value: 3
                        },
                    ]} />
                    <ItemTable title="MAGNITUDE" data={[
                        {
                            label: "Low",
                            value: 1
                        },
                        {
                            label: "Moderate",
                            value: 2
                        },
                        {
                            label: "High",
                            value: 3
                        },
                    ]} />
                    <div className="flex-1">
                        <img src="/images/matrix1.png" className="w-full" alt="Three-Cell-Matrix Type" />
                    </div>
                </div>
            </div>
            <div className="mt-10">
                <ul>
                    <ListItemSwicther label="Five-Cell-Matrix Type" roundedBottom={true} roundedTop={true} onChange={() => handleChange()} checked={!active} />
                </ul>
                <div className="flex gap-3 mt-5">
                    <ItemTable title="LIKELIHOOD" data={[
                        {
                            label: "Remote",
                            value: 1
                        },
                        {
                            label: "Unlikely",
                            value: 2
                        },
                        {
                            label: "Likely",
                            value: 3
                        },
                        {
                            label: "Most Likely",
                            value: 4
                        },
                        {
                            label: "Almost Certain",
                            value: 5
                        },
                    ]} />
                    <ItemTable title="MAGNITUDE" data={[
                        {
                            label: "Immaterial",
                            value: 1
                        },
                        {
                            label: "Minor",
                            value: 2
                        },
                        {
                            label: "Moderate",
                            value: 3
                        },
                        {
                            label: "Major",
                            value: 4
                        },
                        {
                            label: "Material",
                            value: 5
                        },
                    ]} />
                    <div className="flex-1">
                        <img src="/images/matrix2.png" className="w-full" alt="Five-Cell-Matrix Type" />
                    </div>
                </div>
            </div>
        </WrapperSetting>
    );
}

const ItemTable = ({ title, data = [] }) => {
    return (
        <div className="flex-1 text-xs">
            <div className="bg-[rgba(16,136,228,0.35)] text-center text-white rounded-t-md p-4">{title}</div>
            <div className="flex gap-2">
                <div className="bg-[#4646460c] py-3 px-4 flex-1">Criteria</div>
                <div className="bg-[#4646460c] py-3 px-4 text-center w-[120px]">Factor</div>
            </div>
            {
                data?.map((item, index) => (
                    <div className="flex gap-2 bg-white" key={`data-${index}`}>
                        <div className="py-3 px-4 flex-1">{item?.label}</div>
                        <div className="py-3 px-4 text-center w-[120px]">{item?.value}</div>
                    </div>
                ))
            }
        </div>
    )
}
