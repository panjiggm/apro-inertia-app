import React, { useState } from "react";
import { Inertia } from "@inertiajs/inertia";
import { ListRadiobox } from "@/Components/settings";
import WrapperSetting from "@/Components/WrapperSetting";
const plans = [
    {
        name: '2 minutes',
    },
    {
        name: '5 minutes',
    },
    {
        name: '10 minutes',
    },
    {
        name: '15 minutes',
    },
    {
        name: 'never',
    },
]

export default function AutoLock(props) {
    const [selected, setSelected] = useState(plans[1])

    const handleChange = (value) => {
        setSelected(value)
        let params = {
            key: 'auto_lock',
            value: value.name
        }

        Inertia.post(route('company.settings.display.update'), params);
    }

    return (
        <WrapperSetting {...props} title="Auto-Lock" back={route('company.settings.display')} titleBack="Display" >
            <ListRadiobox selected={selected} setSelected={handleChange} data={plans} />
        </WrapperSetting>
    );
}
