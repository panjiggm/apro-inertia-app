import React, { useEffect } from 'react';
import Button from '@/Components/Button';
import Checkbox from '@/Components/Checkbox';
import Guest from '@/Layouts/Guest';
import Input from '@/Components/Input';
import Label from '@/Components/Label';
import ValidationErrors from '@/Components/ValidationErrors';
import { Head, Link, useForm } from '@inertiajs/inertia-react';

export default function Register({ status, canResetPassword }) {
    const { data, setData, post, processing, errors, reset } = useForm({
        email: '',
        companyID: '',
        password: '',
        address: '',
        remember: '',
    });

    useEffect(() => {
        return () => {
            reset('password');
        };
    }, []);

    const onHandleChange = (event) => {
        setData(event.target.name, event.target.type === 'checkbox' ? event.target.checked : event.target.value);
    };

    const submit = (e) => {
        e.preventDefault();

        post(route('login'));
    };

    return (
        <Guest>
            <Head title="Login" />

            {status && <div className="mb-4 font-medium text-sm text-green-600">{status}</div>}

            <ValidationErrors errors={errors} />

            <form onSubmit={submit}>
                <h3 className="text-center text-lg font-medium mb-12 text-[#2A2A2A]">Register Awal</h3>
                <div>
                    <Input
                        type="text"
                        name="companyID"
                        value={data.companyID}
                        className="block w-full"
                        autoComplete="username"
                        isFocused={true}
                        placeholder="Nama KAP"
                        handleChange={onHandleChange}
                    />
                </div>
                <div className="mt-6">
                    <Input
                        type="text"
                        name="email"
                        value={data.email}
                        className="block w-full"
                        autoComplete="username"
                        isFocused={true}
                        placeholder="NPWP"
                        handleChange={onHandleChange}
                    />
                </div>

                <div className="mt-6">
                    <Input
                        type="text"
                        name="address"
                        value={data.address}
                        placeholder="Alamat"
                        className="block w-full"
                        autoComplete="current-password"
                        handleChange={onHandleChange}
                    />
                </div>

                <div className="flex mt-6 gap-4">
                    <Input
                        type="text"
                        name="address"
                        value={data.address}
                        placeholder="No. Telepon"
                        className="block w-full"
                        autoComplete="current-password"
                        handleChange={onHandleChange}
                    />
                    <Input
                        type="text"
                        name="address"
                        value={data.address}
                        placeholder="No. Fax"
                        className="block w-full"
                        autoComplete="current-password"
                        handleChange={onHandleChange}
                    />
                </div>

                <div className="mt-6">
                    <Input
                        type="text"
                        name="address"
                        value={data.address}
                        placeholder="Alamat Email"
                        className="block w-full"
                        autoComplete="current-password"
                        handleChange={onHandleChange}
                    />
                </div>

                <div className="mt-6">
                    <Input
                        type="text"
                        name="address"
                        value={data.address}
                        placeholder="Nomor Whatsapp"
                        className="block w-full"
                        autoComplete="current-password"
                        handleChange={onHandleChange}
                    />
                </div>

                <div className="flex justify-between mt-6">
                    <label className="flex items-center">
                        <Checkbox name="remember" value={data.remember} handleChange={onHandleChange} />

                        <span className="ml-2 text-sm text-[#9FB3C5]">Ingat Saya</span>
                    </label>
                    <Link
                            href={route('password.request')}
                            className="text-[#9FB3C5] text-sm hover:text-gray-900"
                        >
                           Lupa Kata Sandi?
                        </Link>
                </div>

                <div className="flex flex-col mt-12">
                    <Button processing={processing}>
                        Login
                    </Button>
                    <div className="mt-6 text-center">
                        <a href="#open" className="text-blue-500 text-base">Daftar Audit Pro</a>
                    </div>
                </div>
            </form>
        </Guest>
    );
}
