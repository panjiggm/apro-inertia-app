import MyDropzone from "@/Components/MyDropzone";
import TitleDocument from "@/Components/TitleDocument";
import { Link } from "@inertiajs/inertia-react";

export default function DocumentUploadDetail() {
    return (
        <div className="">
            <div className="border-b relative border-gray-200 p-8 text-center">
                <h2 className="text-3xl text-zinc-800">Apro</h2>
                <span className="text-slate-400">Audit Process Online </span>
                <Link className="absolute left-8 top-1/2 -translate-y-1/2 " href={route("company.documents.index")}>
                    <svg width="27" height="26" viewBox="0 0 27 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect opacity="0.5" x="1.75" y="14.7499" width="3.5" height="24.5" rx="1.75" transform="rotate(-90 1.75 14.7499)" fill="#2A2A2A" />
                        <path d="M13.3947 3.78221C14.1237 3.08854 14.1237 1.96387 13.3947 1.2702C12.6657 0.576521 11.4838 0.576521 10.7548 1.2702L0.546736 11.5599C-0.159945 12.2324 -0.18467 13.3152 0.490647 14.0162L10.7573 24.6738C11.4539 25.3969 12.6347 25.4458 13.3947 24.7829C14.1546 24.12 14.206 22.9964 13.5093 22.2733L4.375 12.9999L13.3947 3.78221Z" fill="#2A2A2A" />
                    </svg>

                </Link>
            </div>
            <div className="mt-16">
                <TitleDocument title="Upload Dokumen" description="Persiapkan scan dokumen legalitasmu dan unggah dimasing-masing folder yang disediakan" />

                <MyDropzone />
            </div>
        </div>
    )
}