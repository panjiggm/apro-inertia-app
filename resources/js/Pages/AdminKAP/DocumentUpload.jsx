import ButtonDocument from "@/Components/ButtonDocument";
import TitleDocument from "@/Components/TitleDocument";

export default function DocumentUpload() {
    return (
        <div className="">
            <div className="border-b border-gray-200 p-8 text-center">
                <h2 className="text-3xl text-zinc-800">Apro</h2>
                <span className="text-slate-400">Audit Process Online </span>
            </div>
            <div className="mt-16">
                <TitleDocument title="Upload Dokumen" description="Persiapkan scan dokumen legalitasmu dan unggah dimasing-masing folder yang disediakan" />

                <div className="my-0 mx-auto max-w-[800px] flex gap-3 justify-center mt-12 ">
                    <ButtonDocument href={route('company.documents.type',{type : "legalitas"})} title="5 Docs" image="/images/document_uploaded.png" description="Scan Legalitas Dokumen" />
                    <ButtonDocument href={route('company.documents.type',{type : "npwp"})} title="0 Docs" image="/images/document.png" description="Scan NPWP" />
                    <ButtonDocument href={route('company.documents.type',{type : "identitas"})} title="0 Docs" image="/images/document.png" description="Scan Identitas" />
                </div>

                <div className="my-0 mx-auto max-w-[400px] flex gap-2 mt-16">
                    <button className="flex-1 text-sm font-medium text-[#1088E4] p-4 bg-transparent">Batalkan</button>
                    <button className="flex-1 text-sm font-medium text-white p-4 bg-[#1088E4] rounded-lg">Kirim</button>
                </div>
            </div>
        </div>

    )
}