import React, { useEffect } from 'react';
import Button from '@/Components/Button';
import Checkbox from '@/Components/Checkbox';
import Guest from '@/Layouts/Guest';
import Input from '@/Components/Input';
import Label from '@/Components/Label';
import ValidationErrors from '@/Components/ValidationErrors';
import { Head, Link, useForm } from '@inertiajs/inertia-react';

export default function Login({ status, canResetPassword }) {
    const { data, setData, post, processing, errors, reset } = useForm({
        email: '',
        companyID: '',
        password: '',
        remember: '',
    });

    useEffect(() => {
        return () => {
            reset('password');
        };
    }, []);

    const onHandleChange = (event) => {
        setData(event.target.name, event.target.type === 'checkbox' ? event.target.checked : event.target.value);
    };

    const submit = (e) => {
        e.preventDefault();

        post(route('login'));
    };

    return (
        <Guest>
            <Head title="Login" />

            {status && <div className="mb-4 font-medium text-sm text-green-600">{status}</div>}

            <ValidationErrors errors={errors} />

            <form onSubmit={submit}>
                <h3 className="text-center text-lg font-medium mb-6 text-[#2A2A2A]">Log In</h3>

                <div>
                    <Input
                        type="text"
                        name="companyID"
                        value={data.companyID}
                        className="block w-full"
                        isFocused={true}
                        placeholder="Company ID"
                        handleChange={onHandleChange}
                    />
                </div>

                <div  className="mt-6">
                    <Input
                        type="text"
                        name="email"
                        value={data.email}
                        className="block w-full"
                        autoComplete="username"
                        placeholder="Email"
                        handleChange={onHandleChange}
                    />
                </div>

                <div className="mt-6">
                    <Input
                        type="password"
                        name="password"
                        value={data.password}
                        placeholder="Password"
                        className="block w-full"
                        autoComplete="current-password"
                        handleChange={onHandleChange}
                    />
                </div>

                <div className="flex justify-between mt-6">
                    <label className="flex items-center">
                        <Checkbox name="remember" value={data.remember} handleChange={onHandleChange} />

                        <span className="ml-2 text-sm text-[#9FB3C5]">Remember me</span>
                    </label>
                    <Link
                            href={route('password.request')}
                            className="text-[#9FB3C5] text-sm hover:text-gray-900"
                        >
                           Forgot Password?
                        </Link>
                </div>

                <div className="flex flex-col mt-6">
                    <Button processing={processing}>
                        Login
                    </Button>
                </div>
            </form>
        </Guest>
    );
}
