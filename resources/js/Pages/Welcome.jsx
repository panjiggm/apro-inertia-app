import React from 'react'
import {Link, Head} from '@inertiajs/inertia-react'
import TranslateButton from '@/Components/landing/TranslateButton'
import TextImage from '@/Components/landing/TextImage'
import TitleSection from '@/Components/landing/TitleSection'
import ProductTab from '@/Components/landing/ProductTab'
import Counting from '@/Components/landing/Counting'
import CardIcon from '@/Components/landing/CardIcon'
import {useTranslation} from 'react-i18next'
import Slider from 'react-slick'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import ButtonSignIn from '@/Components/landing/ButtonSignin'

export default function Welcome(props) {
  const [t, i18n] = useTranslation('common')
  const dashboardRoute = role => {
    let route = 'auditor.dashboard'

    if (role == 'super-admin') {
      route = 'admin.dashboard'
    } else if (role == 'company-admin') {
      route = 'company.dashboard'
    }

    return route
  }

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1,
  }

  return (
    <div className="custom-bg">
      <div className="container mx-auto py-[25px]">
        <Head title="APRO" />
        {/* HEADER */}
        <div className="flex justify-between">
          <a href="#">
            <h1 className="text-[44px] bg-clip-text text-transparent font-bold bg-gradient-to-r from-[#4C05BF] via-[#FF4F44] to-[#FFD744]">
              APRO
            </h1>
          </a>
          <div className="flex items-center gap-6">
            <TranslateButton />
            <ButtonSignIn text={t('welcome.signin')} />
          </div>
        </div>
        {/* Main Content */}
        <div className="">
          <div className="mt-[150px] max-w-[845px] mx-auto text-center">
            <h1 className="text-[50px] bg-clip-text text-transparent font-bold bg-gradient-to-r from-[#4C05BF] via-[#FF4F44] to-[#FFD744]">
              Recording, Tracking, Intergated, Secure and Paperless
            </h1>
            <h3 className="mt-[16px] mb-[24px] text-[#22222280] text-[24px]">One Platform. For All.</h3>
            <div className="max-w-[650px] mx-auto">
              <p className="text-[#22222280] text-[19px]">{t('welcome.title_description')}</p>
              <div className="flex mt-[16px]">
                <input
                  placeholder={t('welcome.subscription.input')}
                  className="px-[16px] py-[13px] flex-1 rounded-bl-lg border-r-transparent rounded-tl-lg border border-[rgba(34,34,34,0.25)]"
                />
                <button
                  type="button"
                  className="w-[157px] bg-gradient-to-b rounded-tr-lg rounded-br-lg from-[#2EA6FF] to-[#0C7CD1] text-white"
                >
                  {t('welcome.subscription.button')}
                </button>
              </div>
            </div>
          </div>
          <div className="mt-[140px] mb-[93px] flex justify-center">
            <img src="images/landing-top.png" />
          </div>
          <TitleSection
            title={t('welcome.section2.title')}
            descriptions={t('welcome.section2.descriptions')}
            learMore={t('welcome.lear_more')}
          />
          <div className="border-t border-b flex gap-6 justify-between py-9 mt-11">
            {[1, 2, 3, 4, 5].map(item => (
              <img src={`images/client${item}.png`} />
            ))}
          </div>
          <TitleSection
            title={t('welcome.section3.title')}
            descriptions={t('welcome.section3.descriptions')}
            learMore={t('welcome.lear_more')}
            className="mt-[250px]"
          />
          <div className="mt-[100px]">
            <TextImage
              title={t('welcome.cards.item1.title')}
              descriptions={t('welcome.cards.item1.descriptions')}
              labelButton={t('welcome.trialLabel')}
              image="images/user-friendly.png"
            />

            <TextImage
              title={t('welcome.cards.item2.title')}
              descriptions={t('welcome.cards.item2.descriptions')}
              labelButton={t('welcome.trialLabel')}
              image="images/easy-for-project-monitoring.png"
              imagePosition="left"
            />

            <TextImage
              title={t('welcome.cards.item3.title')}
              descriptions={t('welcome.cards.item3.descriptions')}
              labelButton={t('welcome.trialLabel')}
              image="images/safety-and-audit-file-encripted.png"
            />

            <TextImage
              title={t('welcome.cards.item4.title')}
              descriptions={t('welcome.cards.item4.descriptions')}
              labelButton={t('welcome.trialLabel')}
              image="images/24-hours-service.png"
              imagePosition="left"
            />
          </div>
          <TitleSection
            title={t('welcome.section4.title')}
            descriptions={t('welcome.section4.descriptions')}
            learMore={t('welcome.lear_more')}
            className="mt-[175px]"
          />
          <ProductTab />
          <TitleSection
            title={t('welcome.section6.title')}
            descriptions={t('welcome.section6.descriptions')}
            learMore={t('welcome.lear_more')}
            className="mt-[50px]"
          />
          <div className="flex gap-10 mt-24">
            <CardIcon
              icon={
                <svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                    d="M24.0006 38.0929L35.1381 44.8148C36.8953 45.875 39.0628 44.2993 38.5966 42.3023L35.6401 29.6316L45.4822 21.1054C47.0333 19.7627 46.204 17.2142 44.1598 17.0417L31.2045 15.9433L26.1366 3.98546C25.3365 2.10043 22.6647 2.10043 21.8645 3.98546L16.7967 15.9433L3.84135 17.0417C1.7972 17.2142 0.967869 19.7627 2.51892 21.1054L12.361 29.6316L9.40452 42.3023C8.93831 44.2993 11.1058 45.875 12.8631 44.8148L24.0006 38.0929Z"
                    fill="url(#paint0_linear_201_2789)"
                  />
                  <defs>
                    <linearGradient
                      id="paint0_linear_201_2789"
                      x1="24.1425"
                      y1="2.57169"
                      x2="24.1425"
                      y2="45.1554"
                      gradientUnits="userSpaceOnUse"
                    >
                      <stop stop-color="#2EA6FF" />
                      <stop offset="1" stop-color="#0C7CD1" />
                    </linearGradient>
                  </defs>
                </svg>
              }
              title1={t('welcome.focusLongTerm.item1.title')}
              title2={t('welcome.focusLongTerm.item1.subtitle')}
              descriptions={t('welcome.focusLongTerm.item1.content')}
            />
            <CardIcon
              icon={
                <svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                    d="M23.7219 1.71445C30.0796 1.71445 35.6915 4.9078 39.0411 9.77806L23.7233 25.0979L15.7461 17.12C15.0206 16.3945 13.8441 16.3944 13.1186 17.1199C12.3929 17.8456 12.3929 19.0219 13.1184 19.7474L22.4096 29.0393C23.1351 29.7649 24.3116 29.7649 25.0371 29.0393L40.8926 13.1844C41.8002 15.3744 42.3012 17.7756 42.3012 20.2938C42.3012 24.477 40.9187 28.3374 38.5857 31.4429L38.5878 31.4414L38.5688 31.4653C37.9631 32.2691 37.2935 33.0221 36.5679 33.7168L26.4993 45.0389C26.4026 45.1476 26.2996 45.2505 26.1909 45.3473C24.7377 46.6389 22.5533 46.5829 21.168 45.2693L20.9448 45.0386L10.8853 33.7256C7.34667 30.3428 5.14258 25.5757 5.14258 20.2938C5.14258 10.0327 13.4608 1.71445 23.7219 1.71445Z"
                    fill="url(#paint0_linear_201_2794)"
                  />
                  <defs>
                    <linearGradient
                      id="paint0_linear_201_2794"
                      x1="23.8402"
                      y1="1.71445"
                      x2="23.8403"
                      y2="46.2859"
                      gradientUnits="userSpaceOnUse"
                    >
                      <stop stop-color="#2EA6FF" />
                      <stop offset="1" stop-color="#0C7CD1" />
                    </linearGradient>
                  </defs>
                </svg>
              }
              title1={t('welcome.focusLongTerm.item2.title')}
              title2={t('welcome.focusLongTerm.item2.subtitle')}
              descriptions={t('welcome.focusLongTerm.item2.content')}
            />
            <CardIcon
              icon={
                <svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path
                    fill-rule="evenodd"
                    clip-rule="evenodd"
                    d="M23.6631 1.71627C29.7375 1.66028 35.7547 2.89474 41.3163 5.33799C42.3757 5.82953 43.05 6.89477 43.041 8.06252V22.8614C43.057 32.69 37.031 41.5181 27.872 45.084L25.8044 45.8862C24.4266 46.4191 22.8997 46.4191 21.5218 45.8862L19.4542 45.084C10.2952 41.5181 4.26925 32.69 4.28524 22.8614V8.06252C4.27629 6.89477 4.9506 5.82953 6.00988 5.33799C11.5716 2.89474 17.5887 1.66028 23.6631 1.71627ZM33.6752 25.4891L31.844 23.5145C31.2115 22.8317 31.2115 21.777 31.844 21.0942L33.6771 19.1215C34.3876 18.3582 34.518 17.2211 33.9985 16.3169C33.479 15.4128 32.431 14.9526 31.4138 15.1819L28.7938 15.7885C27.8884 15.9962 26.9769 15.4668 26.7088 14.5774L25.924 11.9982C25.6228 11 24.7033 10.3169 23.6607 10.3169C22.618 10.3169 21.6985 11 21.3974 11.9982L20.6125 14.5774C20.3444 15.4668 19.4329 15.9962 18.5275 15.7885L15.9115 15.1819C14.8946 14.9538 13.8476 15.4144 13.3287 16.3182C12.8099 17.222 12.9401 18.3584 13.6501 19.1215L15.4794 21.0942C16.1106 21.7766 16.1106 22.83 15.4794 23.5126L13.6481 25.4872C12.9376 26.2504 12.8073 27.3875 13.3268 28.2917C13.8463 29.1959 14.8942 29.6561 15.9115 29.4266L18.5313 28.8202C19.4367 28.6123 20.3484 29.1418 20.6164 30.0312L21.4012 32.6104C21.7023 33.6086 22.6219 34.2917 23.6645 34.2917C24.7072 34.2917 25.6267 33.6086 25.9279 32.6104L26.7127 30.0312C26.9808 29.1418 27.8923 28.6123 28.7978 28.8202L31.4138 29.4266C32.4298 29.6536 33.4754 29.1931 33.9939 28.2902C34.5124 27.3875 34.3832 26.2523 33.6752 25.4891Z"
                    fill="url(#paint0_linear_201_2799)"
                  />
                  <path
                    d="M23.6645 24.9695C25.136 24.9695 26.329 23.7767 26.329 22.3051C26.329 20.8335 25.136 19.6406 23.6645 19.6406C22.1929 19.6406 21 20.8335 21 22.3051C21 23.7767 22.1929 24.9695 23.6645 24.9695Z"
                    fill="#5B37D4"
                  />
                  <defs>
                    <linearGradient
                      id="paint0_linear_201_2799"
                      x1="23.7865"
                      y1="1.71445"
                      x2="23.7865"
                      y2="46.2859"
                      gradientUnits="userSpaceOnUse"
                    >
                      <stop stop-color="#2EA6FF" />
                      <stop offset="1" stop-color="#0C7CD1" />
                    </linearGradient>
                  </defs>
                </svg>
              }
              title1={t('welcome.focusLongTerm.item3.title')}
              title2={t('welcome.focusLongTerm.item3.subtitle')}
              descriptions={t('welcome.focusLongTerm.item3.content')}
            />
          </div>
          <div className="mt-24">
            <Slider {...settings}>
              <div>
                <div className="flex mb-10">
                  <div className="flex-1">
                    <img src="images/testimony.png" alt="dummy" />
                  </div>
                  <div className="flex-1 ">
                    <div className="h-full flex flex-col justify-center mx-auto max-w-[518px] p-8">
                      <div className="">
                        <img src="images/client1.png" />
                      </div>
                      <p className=" my-[48px]">
                      “{t("welcome.testimonies.content1")}”
                      </p>
                      <div className="flex flex-col">
                        <strong className="text-[21px] font-medium bg-clip-text text-transparent bg-gradient-to-r from-[#4C05BF] via-[#FF4F44] to-[#FFD744]">
                          RIDWAN SALEH, M.Ak., CPA
                        </strong>
                        <span>Managing Partner</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div>
                <div className="flex mb-10">
                  <div className="flex-1">
                    <img src="images/testimony3.png" alt="dummy" />
                  </div>
                  <div className="flex-1 ">
                    <div className="h-full flex flex-col justify-center mx-auto max-w-[518px] p-8">
                      <div className="p-4">
                        <img src="images/client2-testy.png" />
                      </div>
                      <p className=" my-[48px]">
                      “{t("welcome.testimonies.content2")}”
                      </p>
                      <div className="flex flex-col">
                        <strong className="text-[21px] font-medium bg-clip-text text-transparent bg-gradient-to-r from-[#4C05BF] via-[#FF4F44] to-[#FFD744]">
                          BIMA ADI VIRGANA, SE, M.Ak., CPA
                        </strong>
                        <span>Managing Partner</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div>
                <div className="flex mb-10">
                  <div className="flex-1">
                    <img src="images/testimony2.png" alt="dummy" />
                  </div>
                  <div className="flex-1 ">
                    <div className="h-full flex flex-col justify-center mx-auto max-w-[518px] p-8">
                      <div className="">
                        <img src="images/client1-testy.png" />
                      </div>
                      <p className=" my-[48px]">
                        “{t("welcome.testimonies.content3")}”
                      </p>
                      <div className="flex flex-col">
                        <strong className="text-[21px] font-medium bg-clip-text text-transparent bg-gradient-to-r from-[#4C05BF] via-[#FF4F44] to-[#FFD744]">
                          FERDY AMORA, M.Ak., CPA
                        </strong>
                        <span>Partner</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Slider>
          </div>
        </div>
      </div>
      <div className="flex mt-24">
        <div className="flex-1 ">
          <div className="h-full flex flex-col justify-center mx-auto max-w-[518px]">
            <h3 className="text-[38px] font-semibold">{t('welcome.sectionFooter.title')}</h3>
            <p className=" my-[48px]">{t('welcome.sectionFooter.descriptions')}</p>
            <div className="flex mt-[16px]">
              <input
                placeholder={t('welcome.subscription.input')}
                className="px-[16px] py-[13px] flex-1 rounded-bl-lg border-r-transparent rounded-tl-lg border border-[rgba(34,34,34,0.25)]"
              />
              <button
                type="button"
                className="w-[157px] bg-gradient-to-b rounded-tr-lg rounded-br-lg from-[#2EA6FF] to-[#0C7CD1] text-white"
              >
                {t('welcome.subscription.button')}
              </button>
            </div>
          </div>
        </div>
        <div className="flex-1 flex justify-end">
          <img src="images/dummy-footer.png" className="w-full" alt="dummy" />
        </div>
      </div>
      {/* Footer */}
      <div className="bg-[#F3F4F8] p-14">
        <div className="container mx-auto">
          <div className="flex">
            <div className="flex-1">
              <img src="images/logo.png" alt="apro" className="w-[100px]" />
            </div>
            <div className="flex-1">
              <h3 className="font-semibold text-[#222222] mb-6">{t('welcome.footer.menu.menu1.title')}</h3>
              <ul className="text-[#22222280] flex flex-col gap-3">
                <li>{t('welcome.footer.menu.menu1.item1')} </li>
                <li>{t('welcome.footer.menu.menu1.item2')}</li>
              </ul>
            </div>
            <div className="flex-1">
              <h3 className="font-semibold text-[#222222] mb-6">{t('welcome.footer.menu.menu2.title')}</h3>
              <ul className="text-[#22222280] flex flex-col gap-3">
                <li>{t('welcome.footer.menu.menu2.item1')} </li>
                <li>{t('welcome.footer.menu.menu2.item2')}</li>
              </ul>
            </div>
            <div className="flex-1">
              <h3 className="font-semibold text-[#222222] mb-6">{t('welcome.footer.menu.menu3.title')}</h3>
              <ul className="text-[#22222280] flex flex-col gap-3">
                <li>{t('welcome.footer.menu.menu3.item1')} </li>
                <li>{t('welcome.footer.menu.menu3.item2')}</li>
                <li>{t('welcome.footer.menu.menu3.item3')}</li>
              </ul>
            </div>
            <div className="flex-1">
              <img src="images/iso-270021.png" alt="iso-270021" />
            </div>
          </div>
        </div>
      </div>
      <div className="bg-[#E6E9EE] p-6">
        <div className="container mx-auto">
          <div className="flex justify-between">
            <div className="flex gap-3">
              <a href="#">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <g clip-path="url(#clip0_201_2960)">
                    <path
                      fill-rule="evenodd"
                      clip-rule="evenodd"
                      d="M2.2 0C0.984974 0 0 0.984974 0 2.2V21.8C0 23.015 0.984974 24 2.2 24H21.8C23.015 24 24 23.015 24 21.8V2.2C24 0.984974 23.015 0 21.8 0H2.2ZM7.42857 20V9.14285H4V20H7.42857ZM5.71429 4.57143C4.68571 4.57143 4 5.18286 4 5.98286C4 6.78286 4.68571 7.42857 5.71429 7.42857C6.78857 7.42857 7.42857 6.78286 7.42857 5.98286C7.38857 5.18286 6.74286 4.57143 5.71429 4.57143ZM16.5714 20H20V13.8629C20 10.7029 18.16 9.14285 15.8914 9.14285C13.6743 9.14285 12.5714 10.6057 12.5714 10.6057V9.14285H9.14286V20H12.5714V14.2857C12.5714 13.1543 13.1429 11.9771 14.5257 11.9771H14.5714C16 12 16.5714 13.1429 16.5714 14.2857V20Z"
                      fill="#545465"
                    />
                  </g>
                  <defs>
                    <clipPath id="clip0_201_2960">
                      <rect width="24" height="24" fill="white" />
                    </clipPath>
                  </defs>
                </svg>
              </a>
              <a href="#">
                <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <g clip-path="url(#clip0_201_2962)">
                    <path
                      d="M7.85547 0C3.99647 0 0.857422 3.14195 0.857422 7.00195V17.002C0.857422 20.861 3.99937 24 7.85937 24H17.8594C21.7184 24 24.8574 20.858 24.8574 16.998V6.99805C24.8574 3.13905 21.7154 0 17.8554 0H7.85547ZM19.8574 4C20.4094 4 20.8574 4.448 20.8574 5C20.8574 5.552 20.4094 6 19.8574 6C19.3054 6 18.8574 5.552 18.8574 5C18.8574 4.448 19.3054 4 19.8574 4ZM12.8574 6C16.1664 6 18.8574 8.691 18.8574 12C18.8574 15.309 16.1664 18 12.8574 18C9.54842 18 6.85742 15.309 6.85742 12C6.85742 8.691 9.54842 6 12.8574 6ZM12.8574 8C10.6483 8 8.85742 9.79086 8.85742 12C8.85742 14.2091 10.6483 16 12.8574 16C15.0665 16 16.8574 14.2091 16.8574 12C16.8574 9.79086 15.0665 8 12.8574 8Z"
                      fill="#545465"
                    />
                  </g>
                  <defs>
                    <clipPath id="clip0_201_2962">
                      <rect width="24" height="24" fill="white" transform="translate(0.857422)" />
                    </clipPath>
                  </defs>
                </svg>
              </a>
            </div>
            <div className="text-[#22222280]">
              <span>© Copyright 2023 Karsasoft</span>
            </div>
            <div className="">
              {i18n.language === 'en' ? (
                <div className="flex gap-2 items-center">
                  <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" viewBox="0 0 640 480">
                    <path fill="#fff" d="M0 0h640v480H0z" />
                    <path fill="#ce1124" d="M281.6 0h76.8v480h-76.8z" />
                    <path fill="#ce1124" d="M0 201.6h640v76.8H0z" />
                  </svg>
                  <span>English</span>
                </div>
              ) : (
                <div className="flex gap-2 items-center">
                  <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" viewBox="0 0 640 480">
                    <path fill="#e70011" d="M0 0h640v240H0Z" />
                    <path fill="#fff" d="M0 240h640v240H0Z" />
                  </svg>
                  <span>Indonesia</span>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
