@extends('pdf.company._layout')

@section('content')
    <div>
        <b>To: {{ $user->company->name }},</b>
    </div>
    <div>
        {{ $user->company->address }}
    </div>
    <div>
        Tax ID No. {{ $user->company->tax_id_number }}
    </div>

    <div class="text-center" style="margin-top: 30px;">
        <div><b class="text-md">INVOICE</b></div>
        <div class="text-secondary">No. {{ $transaction->invoice_number }}</div>
        <div class="text-secondary">Date: {{ date('j F Y', strtotime($transaction->created_at)) }}</div>
    </div>

    <!-- Begin::Billing Summary -->
    <div style="margin-top: 20px; margin-bottom: 10px;">
        Here your order details:
    </div>

    <div style="border-radius: 8px; border: 1px solid #c1c1c1; padding: 10px;">
        <table>
            <thead>
                <tr>
                    <th style="padding: 0; text-align: left;">Order Description</th>
                    <th style="padding: 0; text-align: right;">Unit</th>
                    <th style="padding: 0; text-align: right;">Period (Month)</th>
                    <th style="padding: 0; text-align: right;">Unit Price/Month</th>
                    <th style="padding: 0; text-align: right;">Total Price</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="padding: 0; text-align: left; display: list-item; margin-left: 20px;">
                        {{ $transaction->package_detail['name'] }}
                    </td>
                    <td style="padding: 0; text-align: right;">
                        1 Package
                    </td>
                    <td style="padding: 0; text-align: right;">
                        {{ $transaction->period }}
                    </td>
                    <td style="padding: 0; text-align: right;">
                        {{ thousandFormat($transaction->package_detail['price'], 'Rp') }}
                    </td>
                    <td style="padding: 0; text-align: right;">
                        {{ thousandFormat($transaction->package_detail['price'] * $transaction->period, 'Rp') }}
                    </td>
                </tr>

                @isset($transaction->package_add_ons)
                    @foreach($transaction->package_add_ons as $add_on)
                        <tr>
                            <td style="padding: 0; text-align: left; display: list-item; margin-left: 20px;">
                                {{ $add_on['name'] }}
                            </td>
                            <td style="padding: 0; text-align: right;">
                                1 Package
                            </td>
                            <td style="padding: 0; text-align: right;">
                                {{ $add_on['unit'] }}
                            </td>
                            <td style="padding: 0; text-align: right;">
                                {{ thousandFormat($add_on['price'], 'Rp') }}
                            </td>
                            <td style="padding: 0; text-align: right;">
                                {{ thousandFormat($add_on['total_price'], 'Rp') }}
                            </td>
                        </tr>
                    @endforeach
                @endisset

                <tr>
                    <td colspan="5" style="padding: 2px 0;">
                        <hr style="border-top: 1px solid #c1c1c1; border-bottom: none; border-left: none; border-right: none;">
                    </td>
                </tr>

                <tr>
                    <th colspan="2" style="padding: 0; text-align: left;">Total</th>
                    <th colspan="3" style="padding: 0; text-align: right;">
                        {{ thousandFormat($transaction->amount, 'Rp') }}
                    </th>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; text-align: left;">VAT 11%</td>
                    <td colspan="3" style="padding: 0; text-align: right;">
                        {{ thousandFormat($transaction->tax_amount, 'Rp') }}
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; text-align: left;">Promo discount</td>
                    <td colspan="3" style="padding: 0; text-align: right;">
                        Rp ({{ thousandFormat($transaction->voucher_discount) }})
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding: 0; text-align: left;">Admin Fee</td>
                    <td colspan="3" style="padding: 0; text-align: right;">
                        {{ thousandFormat($transaction->admin_fee, 'Rp') }}
                    </td>
                </tr>

                <tr>
                    <td colspan="5" style="padding: 2px 0;">
                        <hr style="border-top: 1px solid #c1c1c1; border-bottom: none; border-left: none; border-right: none;">
                    </td>
                </tr>

                <tr>
                    <th colspan="2" style="padding: 0; text-align: left;">Total Billing</th>
                    <th colspan="3" style="padding: 0; text-align: right; color: #FE0707;">
                        {{ thousandFormat($transaction->total_amount, 'Rp') }}
                    </th>
                </tr>
            </tbody>
        </table>
    </div>
    <!-- End::Billing Summary -->

    <div style="margin-top: 20px;">
        <b class="text-md">Payment Method</b>
    </div>
    <div class="text-justify" style="margin-top: 5px;">
        Please make a payment through our payment gateway for automatic verification. You should login to the registration dashboard of APRO and choose ”payment” then follow the instruction.
    </div>
@endsection
