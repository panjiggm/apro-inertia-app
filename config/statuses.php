<?php

return [
	'transaction' => [
        'pending'       => 'Pending',
        'completed'     => 'Completed',	// paid
        'expired'       => 'Expired',
        'canceled'      => 'Canceled',  // canceled by customer
        'rejected'      => 'Rejected',  // canceled by admin
    ]
];
