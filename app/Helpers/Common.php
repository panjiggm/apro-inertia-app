<?php

function redirectByUserRole($role, $status, $transaction=null) {
    switch ($role) {
        case 'super-admin':
            $redirect = route('admin.dashboard');
            break;
        case 'company-admin':
            $redirect = route('company.dashboard');
            if ($status == 'New' || $status == 'Incomplete') {
                $redirect = route('register.account-registration');
            } else if (!isset($transaction)) {
                $redirect = route('register.package-billing.index');
            }
            break;
        default:
            $redirect = route('auditor.dashboard');
            break;
    }

    return $redirect;
}

function isUpdateUrl($url)
{
    if (str_contains($url, 'update')) {
        return true;
    }

    return false;
}

function isDestroyUrl($url)
{
    if (str_contains($url, 'destroy')) {
        return true;
    }

    return false;
}

function thousandFormat($number, $prefix='')
{
    $number = number_format($number, 0, '', '.');
    return ($prefix ? $prefix . ' ' : '') . $number;
}

function companyCode($tax_id_number, $count)
{
    return substr($tax_id_number, 2, 6) .'-'. sprintf('%02d', $count + 1);
}

function randomNumber($digit)
{
    return rand(pow(10, $digit-1), pow(10, $digit)-1);
}

function getFileName($file, $key='')
{
    return time() . $key .'.'. $file->extension();
}

function upload($file, $upload_path, $key='')
{
    $file_name = getFileName($file, $key);
    $file->storeAs($upload_path, $file_name);

    return $file_name;
}

function storageUrl($path)
{
    if ($path) {
        if (strpos($path, 'http') !== false) {
            return $path;
        }

        return Storage::url($path);
    }

    return null;
}
