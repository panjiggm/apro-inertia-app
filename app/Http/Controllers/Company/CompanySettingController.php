<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Http\Requests\Company\Setting\BillingRateRequest;
use App\Http\Requests\Company\Setting\DateTimeRequest;
use App\Http\Requests\Company\Setting\DisplayRequest;
use App\Http\Requests\Company\Setting\InherentRiskRequest;
use App\Http\Requests\Company\Setting\LanguageRequest;
use App\Http\Requests\Company\Setting\LikelihoodRequest;
use App\Http\Requests\Company\Setting\MaterialityRequest;
use App\Http\Requests\Company\Setting\MenuRequest;
use App\Http\Requests\Company\Setting\RiskRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\Models\CompanySetting;
use App\Models\InherentRiskConsiderationFactor;
use App\Models\Setting;
use Illuminate\Http\Request;
use Inertia\Inertia;
use DB;
use Hash;

class CompanySettingController extends Controller
{
    public function index()
    {
        return Inertia::render('AdminKAP/Settings/Index');
    }

    public function about()
    {
        $user = auth()->user();
        $settings = Setting::query()
            ->where('group', 'about')
            ->pluck('value', 'key');

        $settings->put('subscription_type', $user->company->package->name);

        $subscription_expired_date = $user->company->package_expired_at
            ? date('d/m/Y', strtotime($user->company->package_expired_at))
            : 'Unlimited';
        $settings->put('subscription_expired_date', $subscription_expired_date);

        return Inertia::render('AdminKAP/Settings/About', [
            'settings' => $settings
        ]);
    }

    public function date()
    {
        $user = auth()->user();
        $settings = CompanySetting::query()
            ->where('company_id', $user->company_id)
            ->where('group', 'date_time')
            ->pluck('value', 'key');

        return Inertia::render('AdminKAP/Settings/DateTime', [
            'settings' => $settings
        ]);
    }

    public function updateDate(DateTimeRequest $request)
    {
        $user = auth()->user();
        CompanySetting::query()
            ->where('company_id', $user->company_id)
            ->where('key', $request->key)
            ->update([
                'value' => json_encode($request->value)
            ]);

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully update date time setting'
        ]);
    }

    public function fonts()
    {
        $user = auth()->user();
        $settings = CompanySetting::query()
            ->where('company_id', $user->company_id)
            ->where('group', 'font')
            ->pluck('value', 'key');

        return Inertia::render('AdminKAP/Settings/Fonts', [
            'settings' => $settings
        ]);
    }

    public function updateFonts(Request $request)
    {
        if (!$request->font_type) {
            return response()->ijson([
                'status'  => false,
                'message' => 'Font type is required'
            ]);
        }

        $user = auth()->user();
        CompanySetting::query()
            ->where('company_id', $user->company_id)
            ->where('key', 'font_type')
            ->update([
                'value' => json_encode($request->font_type)
            ]);

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully update font type setting'
        ]);
    }

    public function language()
    {
        $user = auth()->user();
        $settings = CompanySetting::query()
            ->where('company_id', $user->company_id)
            ->where('group', 'language_and_region')
            ->pluck('value', 'key');

        return Inertia::render('AdminKAP/Settings/LanguageRegion', [
            'settings' => $settings
        ]);
    }

    public function updateLanguageRegion(LanguageRequest $request)
    {
        $user = auth()->user();
        CompanySetting::query()
            ->where('company_id', $user->company_id)
            ->where('key', $request->key)
            ->update([
                'value' => json_encode($request->value)
            ]);

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully update '. ($request->key == 'app_language' ? 'language' : $request->key) .' setting'
        ]);
    }

    public function legal()
    {
        $settings = Setting::query()
            ->where('group', 'legal_and_regulatory')
            ->pluck('value', 'key');

        return Inertia::render('AdminKAP/Settings/Legal', [
            'settings' => $settings
        ]);
    }

    public function legalNotice()
    {
        $settings = Setting::query()
            ->where('key', 'legal_notices')
            ->firstOrFail();

        return Inertia::render('AdminKAP/Settings/LegalNotice', [
            'settings' => $settings->value
        ]);
    }

    public function legalLicense()
    {
        $settings = Setting::query()
            ->where('key', 'license')
            ->firstOrFail();

        return Inertia::render('AdminKAP/Settings/License', [
            'settings' => $settings->value
        ]);
    }

    public function legalCertification()
    {
        $settings = Setting::query()
            ->where('key', 'certification')
            ->firstOrFail();

        return Inertia::render('AdminKAP/Settings/Certification', [
            'settings' => $settings->value
        ]);
    }

    public function legalPrivacyPolicy()
    {
        $settings = Setting::query()
            ->where('key', 'privacy_policy')
            ->firstOrFail();

        return Inertia::render('AdminKAP/Settings/PrivacyPolicy', [
            'settings' => $settings->value
        ]);
    }

    public function legalTermAndConditions()
    {
        $settings = Setting::query()
            ->where('key', 'term_and_conditions')
            ->firstOrFail();

        return Inertia::render('AdminKAP/Settings/TermAndConditions', [
            'settings' => $settings->value
        ]);
    }

    public function materiality()
    {
        $user = auth()->user();
        $settings = CompanySetting::query()
            ->where('company_id', $user->company_id)
            ->where('group', 'materiality')
            ->get();

        $data = [];
        $value_keys = [
            'profit_before_tax',
            'total_assets',
            'gross_income',
        ];
        foreach ($settings as $key => $setting) {
            foreach ($setting->value as $i => $item) {
                $data[] = [
                    'id'     => $i,
                    'parent' => in_array($i, $value_keys) ? $setting->name : '',
                    'label'  => ucfirst(str_replace('_', ' ', $i)),
                    'min'    => $item['min'],
                    'max'    => $item['max'],
                ];
            }
        }

        return Inertia::render('AdminKAP/Settings/Materiality', [
            'settings' => $data
        ]);
    }

    public function updateMateriality(MaterialityRequest $request)
    {
        $user = auth()->user();
        DB::beginTransaction();
            CompanySetting::query()
                ->where('company_id', $user->company_id)
                ->where('key', 'profit_based_entities')
                ->update([
                    'value' => [
                        'profit_before_tax' => [
                            'min' => $request->profit_before_tax['min'],
                            'max' => $request->profit_before_tax['max']
                        ],
                        'revenue' => [
                            'min' => $request->revenue['min'],
                            'max' => $request->revenue['max']
                        ]
                    ]
                ]);
            CompanySetting::query()
                ->where('company_id', $user->company_id)
                ->where('key', 'asset_based_or_investment_entities')
                ->update([
                    'value' => [
                        'total_assets' => [
                            'min' => $request->total_assets['min'],
                            'max' => $request->total_assets['max']
                        ],
                        'net_assets' => [
                            'min' => $request->net_assets['min'],
                            'max' => $request->net_assets['max']
                        ]
                    ]
                ]);
            CompanySetting::query()
                ->where('company_id', $user->company_id)
                ->where('key', 'not_for_profit_entities')
                ->update([
                    'value' => [
                        'gross_income' => [
                            'min' => $request->gross_income['min'],
                            'max' => $request->gross_income['max']
                        ],
                        'expenditure' => [
                            'min' => $request->expenditure['min'],
                            'max' => $request->expenditure['max']
                        ]
                    ]
                ]);
        DB::commit();

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully update materiality setting'
        ]);
    }

    public function risk()
    {
        $user = auth()->user();
        $risks = CompanySetting::query()
            ->where('company_id', $user->company_id)
            ->where('group', 'risk_material_misstatement_level')
            ->pluck('value', 'key');

        $inherent_risks = InherentRiskConsiderationFactor::query()
            ->whereNull('company_id')
            ->where('is_editable', 0)
            ->where('is_active', 1)
            ->get();

        $company_inherent_risks = InherentRiskConsiderationFactor::query()
            ->where('company_id', $user->company_id)
            ->where('is_active', 1)
            ->get();

        return Inertia::render('AdminKAP/Settings/Risk', [
            'risks'          => $risks,
            'inherent_risks' => $inherent_risks->concat($company_inherent_risks)
        ]);
    }

    public function updateRisk(RiskRequest $request)
    {
        $user = auth()->user();
        DB::beginTransaction();
            foreach ($request->all() as $key => $value) {
                CompanySetting::query()
                    ->where('company_id', $user->company_id)
                    ->where('key', $key)
                    ->update([
                        'value' => json_encode($value)
                    ]);
            }
        DB::commit();

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully update risk setting'
        ]);
    }

    public function storeInherentRisk(InherentRiskRequest $request)
    {
        $user = auth()->user();
        InherentRiskConsiderationFactor::create([
            'company_id'     => $user->company_id,
            'name'           => $request->name,
            'is_significant' => $request->is_significant,
            'is_active'      => 1,
            'is_editable'    => 1,
        ]);

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully add inherent risk setting'
        ]);
    }

    public function updateInherentRisk(InherentRiskRequest $request)
    {
        $user = auth()->user();
        foreach ($request->data as $key => $item) {
            if ($item['is_editable'] == 0 || $item['company_id'] != $user->company_id) {
                continue;
            }

            InherentRiskConsiderationFactor::query()
                ->where('id', $item['id'])
                ->where('is_editable', 1)
                ->where('company_id', $user->company_id)
                ->update([
                    'name'           => $item['name'],
                    'is_significant' => $item['is_significant']
                ]);
        }

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully update inherent risk setting'
        ]);
    }

    public function destroyInherentRisk(InherentRiskRequest $request)
    {
        $user = auth()->user();
        InherentRiskConsiderationFactor::query()
            ->where('id', $request->id)
            ->where('is_editable', 1)
            ->where('company_id', $user->company_id)
            ->update([
                'is_active' => 0,
            ]);

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully delete inherent risk setting'
        ]);
    }

    public function likelihood()
    {
        $user = auth()->user();
        $setting = CompanySetting::query()
            ->where('company_id', $user->company_id)
            ->where('group', 'likelihood_and_magnitude')
            ->firstOrFail();

        return Inertia::render('AdminKAP/Settings/Likelihood', [
            'settings' => $setting->value
        ]);
    }

    public function updateLikelihood(LikelihoodRequest $request)
    {
        $user = auth()->user();
        CompanySetting::query()
            ->where('company_id', $user->company_id)
            ->where('key', 'likelihood_and_magnitude')
            ->update([
                'value' => json_encode($request->likelihood)
            ]);

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully update likelihood setting'
        ]);
    }

    public function menu()
    {
        $user = auth()->user();
        $settings = CompanySetting::query()
            ->where('company_id', $user->company_id)
            ->where('group', 'apro_menu_setting')
            ->pluck('value', 'key');

        return Inertia::render('AdminKAP/Settings/MenuSettings', [
            'settings' => $settings
        ]);
    }

    public function updateMenu(MenuRequest $request)
    {
        $user = auth()->user();
        CompanySetting::query()
            ->where('company_id', $user->company_id)
            ->where('key', $request->key)
            ->update([
                'value' => json_encode($request->value)
            ]);

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully update menu setting'
        ]);
    }

    public function companyProfile()
    {
        $user = auth()->user();
        $company = $user->company;

        $data = [
            'client_id'              => $company->code,
            'company_name'           => $company->name,
            'tax_id'                 => $company->tax_id_number,
            'address'                => $company->address,
            'practice_license'       => $company->practice_license_number,
            'practice_expiry_date'   => $company->practice_expiry_date
                ? date('d/m/Y', strtotime($company->practice_expiry_date))
                : '',
            'managing_partner_name'  => $company->managing_partner_name,
            'managing_partner_email' => $company->managing_partner_email,
            'admin_name'             => $user->name,
            'admin_email'            => $user->email,
        ];

        return Inertia::render('AdminKAP/Settings/GeneralInformation', [
            'settings' => $data
        ]);
    }

    public function billingRate()
    {
        $user = auth()->user();
        $setting = CompanySetting::query()
            ->where('company_id', $user->company_id)
            ->where('key', 'billing_rate')
            ->firstOrFail();

        return Inertia::render('AdminKAP/Settings/BillingRateSettings', [
            'settings' => $setting->value
        ]);
    }

    public function updateBillingRate(BillingRateRequest $request)
    {
        $user = auth()->user();
        $setting = CompanySetting::query()
            ->where('company_id', $user->company_id)
            ->where('key', 'billing_rate')
            ->update([
                'value' => json_encode($request->billing_rate)
            ]);

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully update billing rate setting'
        ]);
    }

    public function display()
    {
        $user = auth()->user();
        $settings = CompanySetting::query()
            ->where('company_id', $user->company_id)
            ->where('group', 'display')
            ->pluck('value', 'key');

        return Inertia::render('AdminKAP/Settings/Display', [
            'settings' => $settings
        ]);
    }

    public function updateDisplay(DisplayRequest $request)
    {
        $user = auth()->user();
        CompanySetting::query()
            ->where('company_id', $user->company_id)
            ->where('key', $request->key)
            ->update([
                'value' => json_encode($request->value)
            ]);

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully update display setting'
        ]);
    }

    public function updateChangePassword(ChangePasswordRequest $request)
    {
        $user = auth()->user();
        if (!Hash::check($request->old_password, $user->password)) {
            return response()->ijson([
                'status'  => false,
                'message' => 'Invalid old password'
            ]);
        }

        $user->update([
            'password' => bcrypt($request->new_password)
        ]);

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully change password'
        ]);
    }
}
