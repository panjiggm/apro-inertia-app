<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Project;
use Illuminate\Http\Request;
use Inertia\Inertia;

class CompanyProjectController extends Controller
{
    public function onGoing()
    {
        $user = auth()->user();
        $projects = Project::query()
            ->with('client:id,name,code,tax_id_number')
            ->where('company_id', $user->company_id)
            ->where('status', 'On Going')
            ->latest('id')
            ->paginate();

        return Inertia::render('AdminKAP/OngoingProject', [
            'projects' => $projects
        ]);
    }

    public function completed()
    {
        $user = auth()->user();
        $projects = Project::query()
            ->with('client:id,name,code,tax_id_number')
            ->where('company_id', $user->company_id)
            ->where('status', 'Completed')
            ->latest('id')
            ->paginate();

        return Inertia::render('AdminKAP/CompletedProject', [
            'projects' => $projects
        ]);
    }
}
