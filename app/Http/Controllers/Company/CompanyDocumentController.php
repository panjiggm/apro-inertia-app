<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Http\Requests\Company\CompanyDocumentRequest;
use App\Models\CompanyDocument;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Storage;

class CompanyDocumentController extends Controller
{
    public function index()
    {
        return Inertia::render('AdminKAP/DocumentUpload');
    }

    public function type($type)
    {
        $user = auth()->user();
        $documents = CompanyDocument::query()
            ->where('company_id', $user->company_id)
            ->where('type', $type)
            ->get();

        return Inertia::render('AdminKAP/DocumentUploadDetail', [
            'documents' => $documents
        ]);
    }

    public function store(CompanyDocumentRequest $request)
    {
        $user = auth()->user();
        $file = $request->file('file');
        $title = $file->getClientOriginalName();
        $size = $file->getSize();
        $file_size = $size / 1024;
        $size_unit = ' KB';
        if ($file_size >= 1000) {
            $file_size = $size / 1048576;
            $size_unit = ' MB';
        }

        $file_name = upload($file, config('storage.document'));

        CompanyDocument::create([
            'company_id' => $user->company_id,
            'type'       => $request->type,
            'title'      => $title,
            'file_name'  => $file_name,
            'file_size'  => round($size, 2) . $size_unit
        ]);

        return response()->json([
            'status'  => true,
            'message' => 'Successfully upload document'
        ]);
    }

    public function rename(CompanyDocumentRequest $request, CompanyDocument $company_document)
    {
        $user = auth()->user();
        if ($company_document->company_id != $user->company_id) {
            return response()->json([
                'status'  => false,
                'message' => 'Forbidden access'
            ]);
        }

        $company_document->update([
            'title' => $request->title
        ]);

        return response()->json([
            'status'  => true,
            'message' => 'Successfully rename document'
        ]);
    }

    public function destroy(Request $request, CompanyDocument $company_document)
    {
        $user = auth()->user();
        if ($company_document->company_id != $user->company_id) {
            return response()->json([
                'status'  => false,
                'message' => 'Forbidden access'
            ]);
        }

        Storage::delete($company_document->file_name);

        if ($company_document->delete()) {
            return response()->json([
                'status'  => true,
                'message' => 'Successfully delete document'
            ]);
        }

        return response()->json([
            'status'  => false,
            'message' => 'Failed to delete document'
        ]);
    }
}
