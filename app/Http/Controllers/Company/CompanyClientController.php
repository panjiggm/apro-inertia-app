<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Http\Requests\Company\CompanyClientRequest;
use App\Models\CompanyClient;
use App\Models\Project;
use Inertia\Inertia;

class CompanyClientController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $company_clients = CompanyClient::query()
            ->where('company_id', $user->company_id)
            ->latest('id')
            ->paginate(20);

        return Inertia::render('AdminKAP/Clients', [
            'company_clients' => $company_clients
        ]);
    }

    public function store(CompanyClientRequest $request)
    {
        $user = auth()->user();

        $tax_id_number = $request->tax_id_number;
        $count = CompanyClient::query()
            ->where('tax_id_number', $tax_id_number)
            ->count();
        $code = companyCode($tax_id_number, $count);

        CompanyClient::create([
            'company_id'                 => $user->company_id,
            'code'                       => $code,
            'tax_id_number'              => $tax_id_number,
            'name'                       => $request->name,
            'business_id_number'         => $request->business_id_number,
            'business_id_date'           => $request->business_id_date,
            'deed_of_establishment'      => $request->deed_of_establishment,
            'deed_of_establishment_date' => $request->deed_of_establishment_date,
            'phone'                      => $request->phone,
            'address'                    => $request->address,
            'website'                    => $request->website,
            'pic_name'                   => $request->pic_name,
            'pic_email'                  => $request->pic_email,
            'pic_phone'                  => $request->pic_phone,
            'status'                     => $request->status
        ]);

        return redirect()->route('company.clients.index')->withSuccess('Successfully add client');
    }

    public function edit(CompanyClient $company_client)
    {
        return response()->json([
            'status' => true,
            'data'   => $company_client
        ]);
    }

    public function update(CompanyClientRequest $request)
    {
        $user = auth()->user();

        $company_client = CompanyClient::query()
            ->where('id', $request->id)
            ->where('company_id', $user->company_id)
            ->first();

        if (!$company_client) {
            return back()->withErrors('Client not found');
        }

        $tax_id_number = $request->tax_id_number;

        if (substr($tax_id_number, 2, 6) == substr($company_client->tax_id_number, 2, 6)) {
            $code = $company_client->code;
        } else {
            $count = CompanyClient::query()
                ->where('tax_id_number', $tax_id_number)
                ->count();
            $code = companyCode($tax_id_number, $count);
        }

        $company_client->update([
            'code'                       => $code,
            'tax_id_number'              => $tax_id_number,
            'name'                       => $request->name,
            'business_id_number'         => $request->business_id_number,
            'business_id_date'           => $request->business_id_date,
            'deed_of_establishment'      => $request->deed_of_establishment,
            'deed_of_establishment_date' => $request->deed_of_establishment_date,
            'phone'                      => $request->phone,
            'address'                    => $request->address,
            'website'                    => $request->website,
            'pic_name'                   => $request->pic_name,
            'pic_email'                  => $request->pic_email,
            'pic_phone'                  => $request->pic_phone,
            'status'                     => $request->status
        ]);

        return redirect()->route('company.clients.index')->withSuccess('Successfully update client');
    }

    public function destroy(CompanyClient $company_client)
    {
        $user = auth()->user();
        if ($company_client->company_id != $user->company_id) {
            return back()->withErrors('Forbidden access');
        }

        $project = Project::query()
            ->where('company_client_id', $company_client->id)
            ->first();
        if ($project) {
            return back()->withErrors('Client has been assigned in project');
        }

        $company_client->delete();

        return redirect()->route('company.clients.index')->withSuccess('Successfully delete client');
    }
}
