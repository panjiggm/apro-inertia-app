<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;

class PdfController extends Controller
{
    public function invoice()
    {
        $transaction = Transaction::query()
            ->with(['user.company', 'payment'])
            ->has('payment')
            ->firstOrFail();

        $data = [
            'user'        => $transaction->user,
            'transaction' => $transaction,
        ];
        $pdf = Pdf::loadView('pdf.company.transaction-invoice', $data);

        return $pdf->stream();
    }
}
