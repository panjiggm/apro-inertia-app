<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Illuminate\Http\Request;
use Inertia\Inertia;

class TransactionController extends Controller
{
    public function index()
    {
        $transactions = Transaction::query()
            ->with(['user:id,company_id', 'user.company:id,name,tax_id_number'])
            ->latest('id')
            ->paginate(20);

        $data = [];
        foreach ($transactions as $key => $item) {
            $add_on_1_qty = null;
            $add_on_1_amt = null;
            $add_on_2_qty = null;
            $add_on_2_amt = null;
            $add_on_3_qty = null;
            $add_on_3_amt = null;
            $add_on_4_qty = null;
            $add_on_4_amt = null;
            if ($item->package_add_ons) {
                foreach ($item->package_add_ons as $key => $add_on) {
                    switch ($add_on['id']) {
                        case 1:
                            $add_on_1_qty = $add_on['qty'];
                            $add_on_1_amt = thousandFormat($add_on['total_price'], 'Rp');
                            break;
                        case 2:
                            $add_on_2_qty = $add_on['qty'];
                            $add_on_2_amt = thousandFormat($add_on['total_price'], 'Rp');
                            break;
                        case 3:
                            $add_on_3_qty = $add_on['qty'];
                            $add_on_3_amt = thousandFormat($add_on['total_price'], 'Rp');
                            break;
                        case 4:
                            $add_on_4_qty = $add_on['qty'];
                            $add_on_4_amt = thousandFormat($add_on['total_price'], 'Rp');
                            break;

                        default:
                            break;
                    }
                }
            }

            $data[] = [
                'id' => $item->id,
                'created_at'     => date('j F Y H:i:s', strtotime($item->created_at)),
                'invoice_number' => $item->invoice_number,
                'company_name'   => $item->user->company->name,
                'tax_id_number'  => $item->user->company->tax_id_number,
                'period'         => ($item->period / 12) . ' Year',
                'package_name'   => $item->package_detail['name'],
                'package_qty'    => 1,
                'package_amount' => thousandFormat($item->package_detail['price'] * $item->period, 'Rp'),
                'status'         => $item->status,
                'add_on_1_qty'   => $add_on_1_qty,
                'add_on_1_amt'   => $add_on_1_amt,
                'add_on_2_qty'   => $add_on_2_qty,
                'add_on_2_amt'   => $add_on_2_amt,
                'add_on_3_qty'   => $add_on_3_qty,
                'add_on_3_amt'   => $add_on_3_amt,
                'add_on_4_qty'   => $add_on_4_qty,
                'add_on_4_amt'   => $add_on_4_amt,
            ];
        }

        return Inertia::render('Admin/Finance', [
            'transactions' => $data,
        ]);
    }
}
