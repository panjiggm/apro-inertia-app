<?php

namespace App\Http\Controllers\Auditor;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auditor\ActivateRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Inertia\Inertia;

class ActivationController extends Controller
{
    public function index($token)
    {
        return Inertia::render('Auditor/Activation', [
            'token' => $token
        ]);
    }

    public function activate(ActivateRequest $request)
    {
        $user = User::query()
            ->where('role', 'auditor')
            ->where('activate_token', $request->token)
            ->where('status', 'Inactive')
            ->firstOrFail();

        $user->update([
            'activate_token'    => null,
            'password'          => Hash::make($request->password),
            'status'            => 'Active',
            'email_verified_at' => date('Y-m-d H:i:s')
        ]);

        return redirect()->route('login')->withSuccess('Successfully add auditor');
    }
}
