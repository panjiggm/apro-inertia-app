<?php

namespace App\Http\Controllers\Auditor;

use App\Http\Controllers\Controller;
use App\Models\AuditStage;
use App\Models\Project;
use Illuminate\Http\Request;
use Inertia\Inertia;

class AuditStageController extends Controller
{
    public function index($project_id)
    {
        $user = auth()->user();
        $project = Project::query()
            ->with('client:id,name')
            ->where('company_id', $user->company_id)
            ->find($project_id);
        if (!$project) {
            return back()->withErrors("Project not found");
        }

        $check = $project->auditors()->where('auditor_id', $user->auditor_id)->exists();
        if (!$check) {
            return back()->withErrors("You are unauthorize in this project");
        }

        $stages = AuditStage::query()
            ->where('workspace_type', $project->workspace_type)
            ->whereNull('audit_stage_id')
            ->get();

        return Inertia::render("Auditor/AuditMenu", [
            'stages'  => $stages,
            'project' => $project
        ]);
    }

    public function detail($audit_stage_id)
    {
        $stages = AuditStage::query()
            ->where('audit_stage_id', $audit_stage_id)
            ->get();

        return response()->json([
            'status' => true,
            'data'   => $stages
        ]);
    }
}
