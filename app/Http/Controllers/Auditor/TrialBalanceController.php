<?php

namespace App\Http\Controllers\Auditor;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auditor\TrialBalanceRequest;
use App\Models\ChartOfAccountTemplate;
use App\Models\ChartOfAccount;
use App\Models\TrialBalance;
use Illuminate\Http\Request;
use Inertia\Inertia;

class TrialBalanceController extends Controller
{
    public function index(Request $request, $project_id)
    {
        $search = $request->query('search');

        $data = TrialBalance::query()
            ->with('coa')
            ->where('project_id', $project_id)
            ->when($search, function($q) use($search) {
                $q->where(function($q) use($search) {
                    $q->where('client_code_account', 'like', "%$search%")
                        ->orWhere('account', 'like', "%$search%");
                });
            })
            ->get();
     
        return Inertia::render("Auditor/TrialBalance", [
            'data' => $data,
            'project_id' =>  $project_id,
        ]);
    }

    public function store(TrialBalanceRequest $request, $project_id)
    {
        $trial_balance = TrialBalance::query()
            ->where('project_id', $project_id)
            ->where('client_code_account', $request->client_code_account)
            ->first();

        $data = [
            'reference_id'        => $request->coa_id,
            'reference_type'      => $request->coa_is_template
                ? 'App\Models\ChartOfAccountTemplate'
                : 'App\Models\ChartOfAccount',
            'client_code_account' => $request->client_code_account,
            'account'             => $request->account,
            'unaudited'           => $request->unaudited,
            'current_balance'     => $request->current_balance,
            'prior_balance'       => $request->prior_balance,
        ];

        if ($trial_balance) {
            $data['updated_by'] = auth()->id();
            $trial_balance->update();
        } else {
            $data['project_id'] = $project_id;
            $data['created_by'] = auth()->id();
            $trial_balance = TrialBalance::create($data);
        }

        return response()->json([
            'status' => true,
            'data'   => $trial_balance
        ]);
    }

    public function destroy($project_id, TrialBalance $trial_balance)
    {
        $trial_balance->delete();

        return response()->json([
            'status'  => true,
            'message' => 'Successfully delete trial balance'
        ]);
    }
}
