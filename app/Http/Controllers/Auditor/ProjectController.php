<?php

namespace App\Http\Controllers\Auditor;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auditor\ProjectRequest;
use App\Models\CompanyClient;
use App\Models\Project;
use App\Models\ProjectAuditor;
use Illuminate\Http\Request;
use Inertia\Inertia;
use DB;

class ProjectController extends Controller
{
    public function index()
    {
        $user = auth()->user();

        return Inertia::render("Auditor/Projects", [
            'companyClients' => $this->getCompanyClient($user->company_id)
        ]);
    }

    public function existing()
    {
        $user = auth()->user();
        $clients = CompanyClient::query()
            ->select([
                'id',
                'company_id',
                'name',
                'created_at'
            ])
            ->whereHas('projects.auditors', function ($q) use ($user) {
                $q->where('auditor_id', $user->auditor_id);
            })
            ->with('projects')
            ->where('company_id', $user->company_id)
            ->get();

        return Inertia::render("Auditor/ExistingProject", [
            'clients' => $clients,
        ]);
    }

    public function audit()
    {
        return Inertia::render("Auditor/AuditMenu");
    }

    public function create()
    {
        $user = auth()->user();

        return Inertia::render('Auditor/Projects/Create', [
            'companyClients' => $this->getCompanyClient($user->company_id)
        ]);
    }

    public function store(ProjectRequest $request)
    {
        $user = auth()->user();

        $count = Project::query()
            ->where('company_id', $user->company_id)
            ->count();
        if ($count >= $user->company->package->max_project) {
            return back()->withErrors('You reach maximum number of projects. Please upgrade your package.');
        }

        $company_client = CompanyClient::query()
            ->where('id', $request->company_client_id)
            ->where('company_id', $user->company_id)
            ->first();
        if (!$company_client) {
            return back()->withErrors('Client not found');
        }

        $project = Project::query()
            ->where('company_client_id', $request->company_client_id)
            ->where('start_date', $request->start_date)
            ->where('end_date', $request->end_date)
            ->first();
        if ($project) {
            return back()->withErrors('Project with same client and period already exist');
        }

        DB::beginTransaction();
        $project = Project::create([
            'company_id'        => $user->company_id,
            'company_client_id' => $request->company_client_id,
            'code'              => date('Ym', strtotime($request->end_date)),
            'workspace_type'    => $request->workspace_type,
            'workspace'         => $request->workspace,
            'status'            => 'On Going',
            'start_date'        => $request->start_date,
            'end_date'          => $request->end_date,
        ]);

        $project->auditors()->attach($user->auditor_id, ['is_admin' => 1]);
        DB::commit();

        return redirect()->route('auditor.projects.existing')->withSuccess('Successfully create project');
    }

    public function edit(Project $project)
    {
        $user = auth()->user();

        return Inertia::render('Auditor/Projects/Create', [
            'companyClients' => $this->getCompanyClient($user->company_id),
            'project'         => $project
        ]);
    }

    public function update(ProjectRequest $request)
    {
        $user = auth()->user();

        $project_auditor = ProjectAuditor::query()
            ->where('project_id', $project->id)
            ->where('auditor_id', $user->auditor_id)
            ->where('is_admin', 1)
            ->first();
        if (!$project_auditor) {
            return back()->withErrors("Only project's admin that allowed to update the project");
        }

        $company_client = CompanyClient::query()
            ->where('id', $request->company_client_id)
            ->where('company_id', $user->company_id)
            ->first();
        if (!$company_client) {
            return back()->withErrors('Client not found');
        }

        $check = Project::query()
            ->where('id', '!=', $request->id)
            ->where('company_client_id', $request->company_client_id)
            ->where('start_date', $request->start_date)
            ->where('end_date', $request->end_date)
            ->first();
        if ($check) {
            return back()->withErrors('Project with same client and period already exist');
        }

        $project->update([
            'company_client_id' => $request->company_client_id,
            'workspace_type'    => $request->workspace_type,
            'workspace'         => $request->workspace,
            'start_date'        => $request->start_date,
            'end_date'          => $request->end_date,
            'status'            => $request->status,
        ]);

        return redirect()->route('auditor.projects.existing')->withSuccess('Successfully update project');
    }

    public function destroy(Project $project)
    {
        $user = auth()->user();
        $project_auditor = ProjectAuditor::query()
            ->where('project_id', $project->id)
            ->where('auditor_id', $user->auditor_id)
            ->where('is_admin', 1)
            ->first();
        if (!$project_auditor) {
            return back()->withErrors("Only project's admin that allowed to delete the project");
        }

        DB::beginTransaction();
        $project->auditors()->detach();
        $project->delete();
        DB::commit();

        return redirect()->route('auditor.projects.existing')->withSuccess('Successfully delete project');
    }

    private function getCompanyClient($company_id)
    {
        return CompanyClient::query()
            ->select([
                'id',
                'company_id',
                'code',
                'name'
            ])
            ->where('company_id', $company_id)
            ->latest('id')
            ->get();
    }
}
