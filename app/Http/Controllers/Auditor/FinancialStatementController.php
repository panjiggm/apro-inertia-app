<?php

namespace App\Http\Controllers\Auditor;

use App\Http\Controllers\Controller;
use App\Models\TrialBalance;
use Illuminate\Http\Request;
use Inertia\Inertia;

class FinancialStatementController extends Controller
{
    public function index(Request $request, $project_id)
    {
        $search = $request->query('search');

        $trial_balances = TrialBalance::query()
            ->with('coa.level1')
            ->where('project_id', $project_id)
            ->when($search, function($q) use($search) {
                $q->whereHas('coa', function($q) use($search) {
                    $q->where('code', 'like', "%$search%")
                        ->orWhere('description', 'like', "%$search%");
                });
            })
            ->get()
            ->groupBy(function($item, $key){
                return $item['reference_type'] .'-'. $item['reference_id'];
            });

        $data = [];
        foreach($trial_balances as $key => $item) {
            $data[] = [
                'code'            => $item->first()->coa->code,
                'description'     => $item->first()->coa->description,
                'type'            => null,
                'classification'  => $item->first()->coa->level1->description,
                'sign'            => $item->first()->coa->sign,
                'current_balance' => $item->sum('current_balance'),
                'prior_balance'   => $item->sum('prior_balance'),
            ];
        }

        // dd($data);

        return Inertia::render("Auditor/ASeries/A24", [
            'data' => $data,
            'project_id' => $project_id
        ]);
    }
}
