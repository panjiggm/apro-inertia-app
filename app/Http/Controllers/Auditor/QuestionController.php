<?php

namespace App\Http\Controllers\Auditor;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auditor\QuestionRequest;
use App\Http\Requests\Auditor\SignOffRequest;
use App\Models\AuditStage;
use App\Models\ScopingQuestionnaire;
use Illuminate\Http\Request;
use Inertia\Inertia;

class QuestionController extends Controller
{
    public function a1($project_id)
    {
        $user = auth()->user();

        $data = ScopingQuestionnaire::query()
            ->where('company_id', $user->company_id)
            ->where('project_id', $project_id)
            ->firstOrFail();

        return Inertia::render('Auditor/ASeries/A1/index', [
            'questionnaire' => $data->questionnaire
        ]);
    }

    public function a3()
    {
        $audit_stage = AuditStage::query()
            ->where('code', 'A.1')
            ->first();

        return Inertia::render('Auditor/ASeries/A3/index');
    }

    public function a1Store(QuestionRequest $request, $project_id)
    {
        $user = auth()->user();

        // TODO: check sign off

        $scoping_questionnaire = ScopingQuestionnaire::firstOrNew(
            [
                'project_id'    => $project_id,
                'company_id'    => $user->company_id,
            ],
            [
                'questionnaire' => $request->questionnaire,
                'is_sign_off'   => 0,
                'created_by'    => $user->id,
            ]
        );

        if ($scoping_questionnaire->id) {
            $scoping_questionnaire->updated_by = $user->id;
        }

        $scoping_questionnaire->save();

        return back()->withSuccess('Successfully save A.1');
    }
}
