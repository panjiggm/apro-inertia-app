<?php

namespace App\Http\Controllers\Auditor;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auditor\FeedbackRequest;
use App\Http\Requests\Auditor\FeedbackReplyRequest;
use App\Models\AuditStage;
use App\Models\Feedback;
use App\Models\FeedbackReply;
use Illuminate\Http\Request;
use Inertia\Inertia;

class FeedbackController extends Controller
{
    public function index()
    {
        $user = auth()->user();

        $feedbacks = Feedback::query()
            ->with([
                'project:id,company_client_id,end_date',
                'project.client:id,name',
                'auditStage:id,code,name',
                'sender:id,name'
            ])
            ->where('receiver_id', $user->id)
            ->latest('id')
            ->get();

        $data = [];
        foreach ($feedbacks as $key => $item) {
            $names = explode(' ', $item->sender->name);
            $initial = '';
            foreach ($names as $value) {
                $initial .= mb_substr($value, 0, 1);
            }

            $data[] = [
                'index'            => $item->auditStage->code,
                'form'             => $item->auditStage->name,
                'comment'          => $item->comment,
                'project_client'   => $item->project->client->name,
                'project_end_date' => date('d/m/Y', strtotime($item->project->end_date)),
                'status'           => $item->status,
                'date'             => date('j M Y, H:i', strtotime($item->created_at)),
                'sender'           => $initial,
            ];
        }

        return Inertia::render('Auditor/Feedback/Index', [
            'feedbacks' => $data
        ]);
    }

    public function store(FeedbackRequest $request, $project_id)
    {
        $user = auth()->user();
        $audit_stage = AuditStage::query()
            ->where('code', $request->audit_stage_code)
            ->first();

        Feedback::create([
            'project_id'     => $project_id,
            'audit_stage_id' => $audit_stage->id,
            'sender_id'      => $user->id,
            'receiver_id'    => $request->receiver_id,
            'comment'        => $request->comment,
            'status'         => 'Open',
        ]);

        return back()->withSuccess('Successfully add feedback');
    }

    public function reply(FeedbackReplyRequest $request, $project_id)
    {
        $feedback = Feedback::findOrFail($request->feedback_id);
        $user = auth()->user();

        if ($feedback->receiver_id == $user->id) {
            $receiver_id = $feedback->sender_id;
        } else {
            $receiver_id = $feedback->receiver_id;
        }

        FeedbackReply::create([
            'feedback_id' => $request->feedback_id,
            'sender_id'   => $user->id,
            'receiver_id' => $receiver_id,
            'comment'     => $request->comment,
        ]);

        return back()->withSuccess('Successfully add reply');
    }
}
