<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Artisan;

class ArtisanController extends Controller
{
    public function migrate()
    {
        Artisan::call('migrate');

        return 'Successfully run migrate';
    }

    public function migrateFresh()
    {
        Artisan::call('migrate:fresh');

        return 'Successfully run migrate fresh';
    }

    public function seed()
    {
        Artisan::call('db:seed');

        return 'Successfully run seed';
    }
}
