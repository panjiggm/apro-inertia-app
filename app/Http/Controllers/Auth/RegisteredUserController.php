<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\Register\AccountRegistrationRequest;
use App\Http\Requests\Auth\Register\PackageBillingRequest;
use App\Http\Requests\Auth\Register\PackageBillingCancelRequest;
use App\Http\Requests\Auth\Register\PackageBillingPaymentRequest;
use App\Http\Requests\Auth\Register\UploadDocumentLegalRequest;
use App\Models\Company;
use App\Models\CompanyDocument;
use App\Models\Package;
use App\Models\PackageAddOn;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Voucher;
use App\Providers\RouteServiceProvider;
use App\Services\MidtransService;
use App\Services\TransactionService;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Inertia\Inertia;
use DB;
use Storage;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Inertia\Response
     */
    public function create()
    {
        return Inertia::render('Auth/Register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'          => 'required|string|max:255',
            'tax_id_number' => 'required|digits:16',
            'email'         => 'required|email|max:255|unique:users,email',
            'password'      => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        DB::beginTransaction();
            $tax_id_number = $request->tax_id_number;
            $count = Company::query()
                ->where('tax_id_number', $tax_id_number)
                ->count();
            $code = companyCode($tax_id_number, $count);

            $company = Company::create([
                'code'          => $code,
                'tax_id_number' => $tax_id_number,
            ]);

            $user = User::create([
                'company_id' => $company->id,
                'name'       => $request->name,
                'email'      => $request->email,
                'role'       => 'company-admin',
                'status'     => 'New',
                'password'   => Hash::make($request->password),
            ]);
        DB::commit();

        event(new Registered($user));

        return redirect()->route('register.submited');
    }

    public function accountRegistration()
    {
        $user = auth()->user();
        $user->load('company');

        if (
            $user->status == 'Incomplete' ||
            !$user->company->name ||
            !$user->company->phone ||
            !$user->company->address ||
            !$user->company->business_id_number ||
            !$user->company->business_id_date ||
            !$user->company->business_license_number ||
            !$user->company->business_license_date ||
            !$user->company->managing_partner_name ||
            !$user->company->managing_partner_email ||
            !$user->company->managing_partner_phone ||
            !$user->company->pic_phone ||
            !$user->company->pic_status
        ) {
            return Inertia::render('AdminKAP/Register/AccountRegistration', [
                'data' => $user->company
            ]);
        }

        return redirect()->route('register.upload-document-legal');
    }

    public function accountRegistrationStore(AccountRegistrationRequest $request)
    {
        $user = auth()->user();

        Company::query()
            ->where('id', $user->company_id)
            ->update([
                'name'                    => $request->company_name,
                'phone'                   => $request->phone,
                'address'                 => $request->address,
                'business_id_number'      => $request->business_id_number,
                'business_id_date'        => $request->business_id_date,
                'business_license_number' => $request->business_license_number,
                'business_license_date'   => $request->business_license_date,
                'website'                 => $request->website,
                'managing_partner_name'   => $request->managing_partner_name,
                'managing_partner_email'  => $request->managing_partner_email,
                'managing_partner_phone'  => $request->managing_partner_phone,
                'pic_phone'               => $request->person_in_charge_phone,
                'pic_status'              => $request->status,
            ]);

        return redirect()->route('register.upload-document-legal');
    }

    public function uploadDocumentLegal()
    {
        $user = auth()->user();
        $document_count = CompanyDocument::where('company_id', $user->company_id)->count();

        if ($user->status == 'Incomplete' || $document_count != 7) {
            return Inertia::render('AdminKAP/Register/UploadDocumentLegal');
        }

        return redirect()->route('register.document-verification');
    }

    public function uploadDocumentLegalStore(UploadDocumentLegalRequest $request)
    {
        $user = auth()->user();
        $documents = CompanyDocument::where('company_id', $user->company_id)->get();
        $document_path = config('storage.document') . $user->company->code . '/';
        $removed_documents = [];

        DB::beginTransaction();
            foreach ($request->except(['privacy', 'term_and_conditions']) as $key => $item) {
                if (!$request->hasFile($key)) {
                    continue;
                }

                $file = $request->file($key);
                $title = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $size = $file->getSize();
                $file_size = $size / 1024;
                $size_unit = 'KB';
                if ($file_size >= 1000) {
                    $file_size = $size / 1048576;
                    $size_unit = 'MB';
                }

                $file_name = upload($file, $document_path, '_' . $key);
                $data = [
                    'type'       => $key,
                    'title'      => $title ?? ucwords(str_replace('_', ' ', $key)),
                    'file_name'  => $file_name,
                    'file_size'  => round($file_size, 2),
                    'size_unit'  => $size_unit
                ];

                $temp = $documents->where('type', $key)->first();
                if ($temp) {
                    $removed_documents[] = $document_path . $temp->file_name;
                    CompanyDocument::where('id', $temp->id)->update($data);
                } else {
                    $data['company_id'] = $user->company_id;
                    CompanyDocument::create($data);
                }
            }
        DB::commit();

        if (count($removed_documents) > 0) {
            Storage::delete($removed_documents);
        }

        return redirect()->route('register.document-verification');
    }

    public function documentVerification()
    {
        $user = auth()->user();

        if ($user->status == 'New' || $user->status == 'Incomplete') {
            return Inertia::render('AdminKAP/Register/DocumentVerification');
        }

        return redirect()->route('register.package-billing.index');
    }

    public function packageBilling()
    {
        $packages = Package::get();
        $add_ons = PackageAddOn::get();

        $packages = $packages->map(function ($item, $key) {
            return [
                'value'    => $item->id,
                'label'    => $item->name,
                'price'    => $item->price,
                'features' => $item->description,
            ];
        });

        return Inertia::render('AdminKAP/Register/PackageBilling', [
            'packages' => $packages,
            'add_ons'  => $add_ons
        ]);
    }

    /**
     * Checkout and create transaction
     */
    public function packageBillingStore(PackageBillingRequest $request)
    {
        $user = auth()->user();
        $package = Package::findOrFail($request->package_id);
        $period = $request->period * 12;
        $amount = $period * $package->price;

        $add_ons = [];
        if ($request->add_ons) {
            $add_on_ids = [];
            foreach ($request->add_ons as $key => $item) {
                $add_on = PackageAddOn::where('id', $item['id'])->first();
                if ($add_on) {
                    $total_price = $add_on->price * $item['qty'] * $period;
                    $add_ons[] = [
                        'id'          => $add_on->id,
                        'name'        => $add_on->name,
                        'description' => $add_on->description ?? '',
                        'price'       => $add_on->price,
                        'unit'        => $add_on->unit ?? '',
                        'qty'         => $item['qty'],
                        'total_price' => $total_price,
                    ];
                    $amount += $total_price;
                }
            }
        }

        $total_amount = $amount;
        unset($package['deleted_at']);
        unset($package['created_at']);
        unset($package['updated_at']);

        DB::beginTransaction();
            if ($request->voucher_code) {
                $voucher = Voucher::where('code', $request->voucher_code)->first();
                if ($voucher) {
                    $discount_price = $voucher->discountPrice($amount);
                    if ($discount_price['discount'] !== null) {
                        $total_amount = $discount_price['amount'];
                        // Update quota
                        $voucher->decrement('quota');
                    }
                }
            }

            $tax_amount = 0.11 * $total_amount;
            $admin_fee = 10000;
            $total_amount += $tax_amount + $admin_fee;
            $transaction_service = new TransactionService();

            $transaction = Transaction::create([
                'user_id'          => $user->id,
                'package_id'       => $package->id,
                'voucher_id'       => $voucher->id ?? null,
                'code'             => $transaction_service->createCode(),
                'invoice_number'   => $transaction_service->createInvoiceNumber(),
                'status'           => config('statuses.transaction.pending'),
                'package_detail'   => $package->toArray(),
                'package_add_ons'  => count($add_ons) > 0 ? $add_ons : null,
                'period'           => $period,
                'voucher_code'     => $request->voucher_code ?? null,
                'voucher_discount' => $discount_price['discount'] ?? null,
                'amount'           => $amount,
                'tax_amount'       => $tax_amount,
                'admin_fee'        => $admin_fee,
                'total_amount'     => $total_amount,
            ]);
        DB::commit();

        $token = null;
        if ($total_amount > 0) {
            try {
                $midtrans_service = new MidtransService();
                $token = $midtrans_service->snapToken($transaction->code, $total_amount);
            } catch (\Exception $e) {
                return back()->withErrors($e->getMessage());
            }
        }

        $user->load('company:id,name,address');

        return Inertia::render('AdminKAP/Register/PaymentInquiry', [
            'company'     => $user->company,
            'transaction' => $transaction,
            'token'       => $token,
        ]);
    }

    public function packageBillingCancel(PackageBillingCancelRequest $request)
    {
        $user = auth()->user();
        $transaction = Transaction::query()
            ->where('user_id', $user->id)
            ->where('code', $request->transaction_code)
            ->first();

        if (!$transaction) {
            return back()->withErrors('Data not found');
        } elseif ($transaction->status == config('statuses.transaction.canceled')) {
            return back()->withErrors('Transaction already canceled');
        } elseif ($transaction->status !== config('statuses.transaction.pending')) {
            return back()->withErrors('Cannot cancel transaction');
        }

        $transaction->update([
            'status' => config('statuses.transaction.canceled')
        ]);

        if ($transaction->total_amount > 0) {
            try {
                $midtrans_service = new MidtransService();
                $midtrans_service->cancel($transaction->code);
            } catch (\Exception $e) {
                return back()->withErrors($e->getMessage());
            }
        }

        return redirect()->route('register.package-billing.index')->withSuccess('Successfully cancel transaction');
    }

    public function packageBillingPayment(PackageBillingPaymentRequest $request)
    {
        $user = auth()->user();
        $transaction = Transaction::query()
            ->where('user_id', $user->id)
            ->where('code', $request->transaction_code)
            ->firstOrFail();

        if ($transaction->total_amount > 0 && $request->midtrans_transaction_id) {
            $payment = $transaction->payment()->firstOrNew();
            $payment->external_id     = $request->midtrans_transaction_id;
            $payment->va_number       = $request->va_number ?? null;
            $payment->biller_code     = $request->biller_code ?? null;
            $payment->bill_key        = $request->bill_key ?? null;
            $payment->payment_code    = $request->payment_code ?? null;
            $payment->payment_type    = $request->payment_type;
            $payment->payment_channel = $request->payment_channel ?? null;
            $payment->status          = $request->transaction_status;
            $payment->expired_at      = isset($transaction->status->expired_at)
                ? date('Y-m-d H:i:s', strtotime($transaction->status->expired_at))
                : date('Y-m-d H:i:s', strtotime('+24 hours'));
            $payment->save();

            // Invoice email will be send after get midtrans notification
        } else {
            $transaction_service = new TransactionService();
            $transaction_service->settled($transaction, $user);
        }

        return Inertia::render('AdminKAP/Register/PaymentVerification');
    }
}
