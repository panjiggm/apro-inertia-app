<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;

class EmailVerificationPromptController extends Controller
{
    /**
     * Display the email verification prompt.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function __invoke(Request $request)
    {
        $user = $request->user();
        $redirect = redirectByUserRole($user->role, $user->status);

        return $user->hasVerifiedEmail()
                    ? redirect()->intended($redirect)
                    : Inertia::render('Auth/VerifyEmail', ['status' => session('status')]);
    }
}
