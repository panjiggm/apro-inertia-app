<?php

namespace App\Http\Controllers;

use App\Mail\Company\InvoiceMail;
use App\Mail\Company\TransactionExpiredMail;
use App\Mail\Company\TransactionPaidMail;
use App\Models\Package;
use App\Models\PaymentGuide;
use App\Models\Transaction;
use App\Models\User;
use App\Services\TransactionService;
use Illuminate\Http\Request;

class MailController extends Controller
{
    public function invoice(TransactionService $transaction_service)
    {
        $transaction = Transaction::query()
            ->with(['user.company', 'payment'])
            ->has('payment')
            ->first();

        if (!$transaction || !$transaction->payment) {
            $user = User::whereNotNull('company_id')->where('status', 'Active')->firstOrFail();
            $package = Package::where('price', '>', 0)->firstOrFail();

            $period = 12;
            $amount = $period * $package->price;
            $tax_amount = 0.11 * $amount;
            $admin_fee = 0;
            $total_amount = $amount + $tax_amount + $admin_fee;

            $transaction = Transaction::create([
                'user_id'          => $user->id,
                'package_id'       => $package->id,
                'voucher_id'       => null,
                'code'             => $transaction_service->createCode(),
                'invoice_number'   => $transaction_service->createInvoiceNumber(),
                'status'           => config('statuses.transaction.pending'),
                'package_detail'   => $package->toArray(),
                'package_add_ons'  => null,
                'period'           => $period,
                'voucher_code'     => null,
                'voucher_discount' => null,
                'amount'           => $amount,
                'tax_amount'       => $tax_amount,
                'admin_fee'        => $admin_fee,
                'total_amount'     => $total_amount,
            ]);

            $transaction->payment()->create([
                'external_id'      => 'Test-' . time(),
                'va_number'        => 'Test-VA-' . time(),
                'payment_type'     => 'bank_transfer',
                'payment_channel'  => 'bni',
                'status'           => 'Unpaid',
                'fraud_status'     => null,
                'note'             => null,
                'transaction_date' => date('Y-m-d H:i:s'),
                'expired_at'       => date('Y-m-d H:i:s', strtotime('+1 day')),
            ]);

            $transaction->load('payment');
        }

        $payment_guide = PaymentGuide::query()
            ->where('payment_type', $transaction->payment->payment_type)
            ->when($transaction->payment->payment_channel, function($q) use($transaction) {
                $q->where('payment_channel', $transaction->payment->payment_channel);
            })
            ->get();

        return new InvoiceMail($transaction->user, $transaction, $payment_guide);
    }

    public function expired()
    {
        $transaction = Transaction::query()
            ->with(['user.company', 'payment'])
            ->has('payment')
            ->firstOrFail();

        return new TransactionExpiredMail($transaction->user, $transaction);
    }

    public function paid()
    {
        $transaction = Transaction::query()
            ->with(['user.company', 'payment'])
            ->has('payment')
            ->firstOrFail();

        return new TransactionPaidMail($transaction->user, $transaction);
    }
}
