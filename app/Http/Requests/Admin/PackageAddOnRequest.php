<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PackageAddOnRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'name'        => 'required|string|max:255',
            'description' => 'required',
            'price'       => 'nullable|integer',
            'unit'        => 'nullable|string|max:255',
        ];

        if (isUpdateUrl($this->url())) {
            $rules['id'] = 'required';
        }

        return $rules;
    }
}
