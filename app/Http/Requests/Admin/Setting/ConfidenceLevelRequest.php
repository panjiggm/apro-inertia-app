<?php

namespace App\Http\Requests\Admin\Setting;

use Illuminate\Foundation\Http\FormRequest;

class ConfidenceLevelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'very_low_level_1' => 'required',
            'very_low_level_2' => 'required',
            'very_low_level_3' => 'required',
            'low_level_1'      => 'required',
            'low_level_2'      => 'required',
            'low_level_3'      => 'required',
            'moderate_level_1' => 'required',
            'moderate_level_2' => 'required',
            'moderate_level_3' => 'required',
            'high_level_1'     => 'required',
            'high_level_2'     => 'required',
            'high_level_3'     => 'required',
            '*.level'          => 'required|numeric|between:0,100',
            '*.factor'         => 'required|numeric',
        ];
    }
}
