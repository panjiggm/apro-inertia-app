<?php

namespace App\Http\Requests\Admin\Setting;

use Illuminate\Foundation\Http\FormRequest;

class ControlRiskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'type' => 'required|in:level,design,implementation',
            'control_risk_low'       => 'required_if:type,level|numeric',
            'control_risk_moderate'  => 'required_if:type,level|numeric',
            'control_risk_high'      => 'required_if:type,level|numeric',
            'control_design_yes'     => 'required_if:type,design|numeric',
            'control_design_partial' => 'required_if:type,design|numeric',
            'control_design_no'      => 'required_if:type,design|numeric',
            'control_implementation_yes'     => 'required_if:type,implementation|numeric',
            'control_implementation_partial' => 'required_if:type,implementation|numeric',
            'control_implementation_no'      => 'required_if:type,implementation|numeric',
        ];
    }
}
