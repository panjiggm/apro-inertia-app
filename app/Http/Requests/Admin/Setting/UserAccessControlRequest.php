<?php

namespace App\Http\Requests\Admin\Setting;

use Illuminate\Foundation\Http\FormRequest;

class UserAccessControlRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'user_access_auditor'                 => 'required',
            'user_access_manager'                 => 'required',
            'user_access_partner'                 => 'required',
            'user_access_eqcr_partner'            => 'required',
            'user_access_managing_partner'        => 'required',
            'user_access_risk_management_partner' => 'required',
            'user_access_auditor_expert'          => 'required',
            'user_access_guess'                   => 'required',
            '*.can_assigned_as_an_admin'          => 'required|in:1,0',
            '*.create'                            => 'required|in:1,0,N/A',
            '*.read'                              => 'required|in:1,0,N/A',
            '*.update'                            => 'required|in:1,0,N/A',
            '*.delete'                            => 'required|in:1,0,N/A,Admin Only',
        ];
    }
}
