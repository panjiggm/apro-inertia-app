<?php

namespace App\Http\Requests\Admin\Setting;

use Illuminate\Foundation\Http\FormRequest;

class CertificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'registration_number'      => 'required',
            'registration_date'        => 'required',
            'date_of_protection_start' => 'required',
            'app_name'                 => 'required',
            'registered_by'            => 'required',
            'domain_name'              => 'required',
        ];
    }
}
