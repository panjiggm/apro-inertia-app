<?php

namespace App\Http\Requests\Auditor;

use Illuminate\Foundation\Http\FormRequest;

class TrialBalanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'client_code_account' => 'required|numeric',
            'account'             => 'required',
            'coa_id'              => 'required',
            'coa_is_template'     => 'required',
            'unaudited'           => 'required|integer',
            'current_balance'     => 'required|integer',
            'prior_balance'       => 'required|integer',
        ];
    }
}
