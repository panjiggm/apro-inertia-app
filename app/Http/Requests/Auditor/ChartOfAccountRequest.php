<?php

namespace App\Http\Requests\Auditor;

use Illuminate\Foundation\Http\FormRequest;

class ChartOfAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'parent_id'   => 'required|exists:chart_of_account_templates,id',
            'code'        => 'required',
            'description' => 'required',
            'sign'        => 'required|in:Debit,Credit',
        ];
    }
}
