<?php

namespace App\Http\Requests\Auth\Register;

use Illuminate\Foundation\Http\FormRequest;

class AccountRegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'company_name'            => 'required|string|max:255',
            'phone'                   => 'required|numeric',
            'address'                 => 'required|string|max:255',
            'business_id_number'      => 'required|string|max:255',
            'business_id_date'        => 'required|date',
            'business_license_number' => 'required|string|max:255',
            'business_license_date'   => 'required|date',
            'website'                 => 'nullable|string|max:255',
            'managing_partner_name'   => 'required|string|max:255',
            'managing_partner_email'  => 'required|email',
            'managing_partner_phone'  => 'required|numeric',
            'person_in_charge_phone'  => 'required|numeric',
            'status'                  => 'required|in:Permanent,Contract',
        ];
    }
}
