<?php

namespace App\Http\Requests\Auth\Register;

use Illuminate\Foundation\Http\FormRequest;

class PackageBillingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'package_id'    => 'required',
            'period'        => 'required|numeric|min:0',
            'add_ons'       => 'nullable|array',
            'add_ons.*.id'  => 'required',
            'add_ons.*.qty' => 'required|numeric|min:0',
        ];
    }
}
