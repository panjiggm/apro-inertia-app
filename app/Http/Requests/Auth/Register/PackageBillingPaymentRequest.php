<?php

namespace App\Http\Requests\Auth\Register;

use Illuminate\Foundation\Http\FormRequest;

class PackageBillingPaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'transaction_code'        => 'required',
            'midtrans_transaction_id' => 'nullable',
            'transaction_status'      => 'required_with:midtrans_transaction_id',
            'payment_type'            => 'required_with:midtrans_transaction_id',
            'payment_channel'         => 'nullable',
            'va_number'               => 'nullable',
            'biller_code'             => 'nullable',
            'bill_key'                => 'nullable',
            'payment_code'            => 'nullable',
        ];
    }
}
