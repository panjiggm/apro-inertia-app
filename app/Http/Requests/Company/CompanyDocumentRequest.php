<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;

class CompanyDocumentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        if (request()->route()->named('company.documents.rename')) {
            $rules = [
                'title' => 'required|string|max:255'
            ];
        } else {
            $rules = [
                'type' => 'required|in:Legalitas,NPWP,Identitas',
                'file' => 'required|file|max:5000'
            ];
        }

        return $rules;
    }
}
