<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;

class CompanyAuditorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'auditor_role_id' => 'required|exists:auditor_roles,id',
            'staff_id'        => 'required',
            'name'            => 'required|string|max:255',
            'id_number'       => 'required|numeric|digits:16',
            'address'         => 'required',
            'education'       => 'required',
            'email'           => 'required|email|unique:users,email',
        ];

        if (isUpdateUrl($this->url())) {
            $rules['id'] = 'required';
            $rules['email'] = 'required|email|unique:users,email,' . $this->id;
            $rules['status'] = 'required|in:Active,Inactive';
        }

        return $rules;
    }
}
