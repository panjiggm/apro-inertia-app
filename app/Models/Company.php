<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'tax_id_number',
        'name',
        'phone',
        'address',
        'business_id_number',
        'business_id_date',
        'business_license_number',
        'business_license_date',
        'practice_license_number',
        'practice_expiry_date',
        'website',
        'managing_partner_name',
        'managing_partner_email',
        'managing_partner_phone',
        'pic_phone',
        'pic_status',
    ];

    public function user()
    {
        return $this->hasOne(User::class);
    }

    public function documents()
    {
        return $this->hasOne(CompanyDocument::class);
    }

    public function package()
    {
        return $this->belongsTo(Package::class);
    }
}
