<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SignOffSetting extends Model
{
    use HasFactory;

    public function role()
    {
        return $this->belongsTo(AuditorRole::class, 'auditor_role_id');
    }
}
