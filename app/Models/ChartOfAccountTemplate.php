<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChartOfAccountTemplate extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'parent_id',
        'code',
        'description',
        'sign',
    ];

    protected $appends = [
        'is_template'
    ];

    public function getIsTemplateAttribute()
    {
        return 1;
    }

    public function level1()
    {
        return $this->belongsTo(ChartOfAccountTemplate::class, 'parent_id');
    }
}
