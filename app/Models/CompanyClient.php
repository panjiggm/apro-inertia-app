<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyClient extends Model
{
    use HasFactory;

    protected $fillable = [
        'company_id',
        'code',
        'name',
        'tax_id_number',
        'phone',
        'business_id_number',
        'business_id_date',
        'deed_of_establishment',
        'deed_of_establishment_date',
        'address',
        'website',
        'pic_name',
        'pic_email',
        'pic_phone',
        'status',
    ];

    public function projects()
    {
        return $this->hasMany(Project::class);
    }
}
