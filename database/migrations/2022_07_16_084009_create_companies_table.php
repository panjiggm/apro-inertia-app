<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('code')->unique();
            $table->string('tax_id_number');
            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->string('business_id_number')->nullable();
            $table->date('business_id_date')->nullable();
            $table->string('business_license_number')->nullable();
            $table->string('business_license_date')->nullable();
            $table->string('practice_license_number')->nullable();
            $table->string('practice_expiry_date')->nullable();
            $table->string('website')->nullable();
            $table->string('managing_partner_name')->nullable();
            $table->string('managing_partner_email')->nullable();
            $table->string('managing_partner_phone')->nullable();
            $table->string('pic_phone')->nullable();
            $table->string('pic_status')->comment('Permanent, Contract');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
};
