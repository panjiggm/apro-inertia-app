<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chart_of_account_templates', function (Blueprint $table) {
            $table->id();
            $table->foreignId('parent_id')
                    ->nullable()
                    ->constrained('chart_of_account_templates')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->string('code');
            $table->string('description');
            $table->string('sign')->nullable();
            $table->string('type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chart_of_account_templates');
    }
};
