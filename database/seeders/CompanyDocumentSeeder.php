<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\CompanyDocument;
use Faker\Factory as Faker;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CompanyDocumentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CompanyDocument::truncate();

        $types = collect([
            'tax_id',
            'company_licence',
            'business_id',
            'partner_citizen_id_card',
            'partner_business_id_card',
            'pic_citizen_id_card',
            'pic_business_id_card',
        ]);

        $faker = Faker::create();
        $companies = Company::get();
        foreach ($companies as $company) {
            foreach ($types as $type) {
                $title = implode(' ', $faker->words(3));

                CompanyDocument::create([
                    'company_id' => $company->id,
                    'type'       => $type,
                    'title'      => Str::title($title),
                    'file_name'  => Str::slug($title, '-') . '.pdf',
                    'file_size'  => $faker->randomFloat(2, 0.1, 1.5),
                    'size_unit'  => 'MB'
                ]);
            }
        }
    }
}
