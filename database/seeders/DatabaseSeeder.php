<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call([
            PackageSeeder::class,
            PackageAddOnSeeder::class,
            AuditorRoleSeeder::class,
            UserSeeder::class,
            CompanyDocumentSeeder::class,
            CompanyClientSeeder::class,
            ProjectSeeder::class,
            AuditStageSeeder::class,
            ChartOfAccountTemplateSeeder::class,
            SettingSeeder::class,
            PaymentGuideSeeder::class,
        ]);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
