<?php

namespace Database\Seeders;

use App\Models\AuditStage;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AuditStageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AuditStage::truncate();

        $data = [
            [
                'id'   => 1,
                'code' => 'TB',
                'name' => 'Trial Balance',
            ],
            [
                'id'   => 2,
                'code' => 'A',
                'name' => 'Pre-Engagement',
            ],
            [
                'id'   => 3,
                'code' => 'B',
                'name' => 'Identify and Assess Risk',
            ],
            [
                'id'   => 4,
                'code' => 'C',
                'name' => 'Design Audit Response',
            ],
            [
                'id'   => 5,
                'code' => 'D',
                'name' => 'Obtain Audit Evidence',
            ],
            [
                'id'   => 6,
                'code' => 'E',
                'name' => 'Completion and Reporting',
            ],

            // Second Level
            // TB
            [
                'id'             => 7,
                'audit_stage_id' => 1,
                'code'           => 'TB.1',
                'name'           => 'Trial Balance',
                'route_name'     => 'auditor.trial-balances.index',
            ],
            // A
            [
                'id'             => 8,
                'audit_stage_id' => 2,
                'code'           => 'A.1',
                'name'           => 'Scoping Questionnaire',
                'route_name'     => 'auditor.aseries.a1',
            ],
            [
                'id'             => 9,
                'audit_stage_id' => 2,
                'code'           => 'A.2',
                'name'           => 'Enter Financial Statement Area (FSA) Data',
            ],
            [
                'id'             => 10,
                'audit_stage_id' => 2,
                'code'           => 'A.3',
                'name'           => 'Materiality Determination',
                'route_name'     => 'auditor.aseries.a3',
            ],
            [
                'id'             => 11,
                'audit_stage_id' => 2,
                'code'           => 'A.4',
                'name'           => 'Scope in Financial Statement Area',
            ],
            [
                'id'             => 12,
                'audit_stage_id' => 2,
                'code'           => 'A.5',
                'name'           => 'Set Financial Statement Area & Its Cycle',
            ],
            [
                'id'             => 13,
                'audit_stage_id' => 2,
                'code'           => 'A.6',
                'name'           => 'Acceptance and Understanding the Engagement',
            ],
            [
                'id'             => 14,
                'audit_stage_id' => 2,
                'code'           => 'CWS',
                'name'           => 'Create Workspace Structure',
            ],
            // B
            [
                'id'             => 15,
                'audit_stage_id' => 3,
                'code'           => 'B.1',
                'name'           => 'Understand the Entity and Its Environment',
            ],
            [
                'id'             => 16,
                'audit_stage_id' => 3,
                'code'           => 'B.2',
                'name'           => 'Understand the Internal Control',
            ],
            [
                'id'             => 17,
                'audit_stage_id' => 3,
                'code'           => 'B.3',
                'name'           => 'Design and Implementation of Internal Control',
            ],
            [
                'id'             => 18,
                'audit_stage_id' => 3,
                'code'           => 'B.4',
                'name'           => 'Using the Work of Internal Auditors',
            ],
            [
                'id'             => 19,
                'audit_stage_id' => 3,
                'code'           => 'B.5',
                'name'           => 'Using the Work of an Auditor\'s expert',
            ],
            [
                'id'             => 20,
                'audit_stage_id' => 3,
                'code'           => 'B.6',
                'name'           => 'Service Organisations or Management Expert',
            ],
            [
                'id'             => 21,
                'audit_stage_id' => 3,
                'code'           => 'B.7',
                'name'           => 'Preliminary Analytical Review (PAR)',
            ],
            [
                'id'             => 22,
                'audit_stage_id' => 3,
                'code'           => 'B.8',
                'name'           => 'Related Party Transactions',
            ],
            [
                'id'             => 23,
                'audit_stage_id' => 3,
                'code'           => 'B.9',
                'name'           => 'Initial Audit Engagement - Review Opening Balance',
            ],
            [
                'id'             => 24,
                'audit_stage_id' => 3,
                'code'           => 'B.10',
                'name'           => 'Management Significant Accounting Estimates',
            ],
            [
                'id'             => 25,
                'audit_stage_id' => 3,
                'code'           => 'B.11',
                'name'           => 'Compliance with Law and Regulation',
            ],
            [
                'id'             => 26,
                'audit_stage_id' => 3,
                'code'           => 'B.12',
                'name'           => 'Commitment and Contingency',
            ],
            [
                'id'             => 27,
                'audit_stage_id' => 3,
                'code'           => 'B.13',
                'name'           => 'Planning Meeting Agenda',
            ],
            [
                'id'             => 28,
                'audit_stage_id' => 3,
                'code'           => 'ERA',
                'name'           => 'Evaluation of Risk Assessment',
            ],
            // C
            [
                'id'             => 29,
                'audit_stage_id' => 4,
                'code'           => 'C.1',
                'name'           => 'Summary of Data and Information Produced by the Entity (DIPE)',
            ],
            [
                'id'             => 30,
                'audit_stage_id' => 4,
                'code'           => 'C.2',
                'name'           => 'Financial Reporting Closing Process Design',
            ],
            [
                'id'             => 31,
                'audit_stage_id' => 4,
                'code'           => 'C.3',
                'name'           => 'Revenue Design',
            ],
            [
                'id'             => 32,
                'audit_stage_id' => 4,
                'code'           => 'C.4',
                'name'           => 'Purchases Design',
            ],
            [
                'id'             => 33,
                'audit_stage_id' => 4,
                'code'           => 'C.5',
                'name'           => 'Payroll Design',
            ],
            [
                'id'             => 34,
                'audit_stage_id' => 4,
                'code'           => 'C.6',
                'name'           => 'Other Design',
            ],
            [
                'id'             => 35,
                'audit_stage_id' => 4,
                'code'           => 'ETDC',
                'name'           => 'Engagement Team Discussion & Communication',
            ],
            [
                'id'             => 36,
                'audit_stage_id' => 4,
                'code'           => 'PSO',
                'name'           => 'Audit Strategy Memorandum and Planning Sign Off',
            ],
            // D
            [
                'id'             => 37,
                'audit_stage_id' => 5,
                'code'           => 'D.1',
                'name'           => 'Test of Control (TOC)',
            ],
            [
                'id'             => 38,
                'audit_stage_id' => 5,
                'code'           => 'D.2',
                'name'           => 'Financial Statement Preparation',
            ],
            [
                'id'             => 39,
                'audit_stage_id' => 5,
                'code'           => 'D.3',
                'name'           => 'Financial Statement Area (FSA)',
            ],
            [
                'id'             => 40,
                'audit_stage_id' => 5,
                'code'           => 'D.4',
                'name'           => 'Subsequent Events',
            ],
            [
                'id'             => 41,
                'audit_stage_id' => 5,
                'code'           => 'D.5',
                'name'           => 'Going Concern',
            ],
            [
                'id'             => 42,
                'audit_stage_id' => 5,
                'code'           => 'D.6',
                'name'           => 'Related Party Transactions',
            ],
            [
                'id'             => 43,
                'audit_stage_id' => 5,
                'code'           => 'D.7',
                'name'           => 'Laws and Regulations',
            ],
            [
                'id'             => 44,
                'audit_stage_id' => 5,
                'code'           => 'D.8',
                'name'           => 'Management Significant Accounting Estimates',
            ],
            [
                'id'             => 45,
                'audit_stage_id' => 5,
                'code'           => 'D.9',
                'name'           => 'Using the Work of Internal Auditors',
            ],
            [
                'id'             => 46,
                'audit_stage_id' => 5,
                'code'           => 'D.10',
                'name'           => 'Using the Work of an Auditor\'s Expert',
            ],
            [
                'id'             => 47,
                'audit_stage_id' => 5,
                'code'           => 'D.11',
                'name'           => 'Service Organisations or Management Expert',
            ],
            [
                'id'             => 48,
                'audit_stage_id' => 5,
                'code'           => 'D.12',
                'name'           => 'Commitment and Contingency',
            ],
            // E
            [
                'id'             => 49,
                'audit_stage_id' => 6,
                'code'           => 'E.1',
                'name'           => 'Evaluate of Misstatements',
            ],
            [
                'id'             => 50,
                'audit_stage_id' => 6,
                'code'           => 'E.2',
                'name'           => 'Final Materiality ',
            ],
            [
                'id'             => 51,
                'audit_stage_id' => 6,
                'code'           => 'E.3',
                'name'           => 'Final Analytical Review',
            ],
            [
                'id'             => 52,
                'audit_stage_id' => 6,
                'code'           => 'E.4',
                'name'           => 'Communication with TCWG',
            ],
            [
                'id'             => 53,
                'audit_stage_id' => 6,
                'code'           => 'E.5',
                'name'           => 'Subsequent Events Clearance Letter',
            ],
            [
                'id'             => 54,
                'audit_stage_id' => 6,
                'code'           => 'E.6',
                'name'           => 'Key Audit Matters',
            ],
            [
                'id'             => 55,
                'audit_stage_id' => 6,
                'code'           => 'E.7',
                'name'           => 'Audit Consultation',
            ],
            [
                'id'             => 56,
                'audit_stage_id' => 6,
                'code'           => 'E.8',
                'name'           => 'Engagement Quality Control Review Checklist',
            ],
            [
                'id'             => 57,
                'audit_stage_id' => 6,
                'code'           => 'E.9',
                'name'           => 'Final Audit Memorandum',
            ],
            [
                'id'             => 58,
                'audit_stage_id' => 6,
                'code'           => 'E.10',
                'name'           => 'Completion Control Checklist',
            ],
            [
                'id'             => 59,
                'audit_stage_id' => 6,
                'code'           => 'IARD',
                'name'           => 'Independen Auditor\'s Report Decision',
            ],
            [
                'id'             => 60,
                'audit_stage_id' => 6,
                'code'           => 'ASO',
                'name'           => 'Audit Sign Off',
            ],
            [
                'id'             => 61,
                'audit_stage_id' => 6,
                'code'           => 'AC',
                'name'           => 'Archiving Checklist',
            ],

            // Third Level
            // A.2
            [
                'audit_stage_id' => 9,
                'code'           => 'A.2.1',
                'name'           => 'Statement of Financial Position',
                'route_name'     => 'auditor.financial-statements.index',
            ],
            [
                'audit_stage_id' => 9,
                'code'           => 'A.2.2',
                'name'           => 'Statement of Profit or Loss and Other Comprehensive Income',
            ],
            [
                'audit_stage_id' => 9,
                'code'           => 'A.2.3',
                'name'           => 'Financial Statements Area',
            ],
            [
                'audit_stage_id' => 9,
                'code'           => 'A.2.4',
                'name'           => 'Mapping Financial Statement Area (FSA Mapping)',
                'route_name'     => 'auditor.chart-of-accounts.index',
            ],
            // A.6
            [
                'audit_stage_id' => 13,
                'code'           => 'A.6.1',
                'name'           => 'Engagement Letter',
            ],
            [
                'audit_stage_id' => 13,
                'code'           => 'A.6.2',
                'name'           => 'Client Re-acceptance Checklist',
            ],
            [
                'audit_stage_id' => 13,
                'code'           => 'A.6.3',
                'name'           => 'New client acceptance',
            ],
            [
                'audit_stage_id' => 13,
                'code'           => 'A.6.4',
                'name'           => 'Engagement Team and Project Budget',
            ],
            // B.1
            [
                'audit_stage_id' => 15,
                'code'           => 'B.1.1',
                'name'           => 'Understand the Entity Questionnaire',
            ],
            [
                'audit_stage_id' => 15,
                'code'           => 'B.1.2',
                'name'           => 'Fraud Considerations',
            ],
            [
                'audit_stage_id' => 15,
                'code'           => 'B.1.3',
                'name'           => 'Going Concern Considerations',
            ],
            [
                'audit_stage_id' => 15,
                'code'           => 'B.1.4',
                'name'           => 'Inherent Risk Assessment',
            ],
            // B.2
            [
                'audit_stage_id' => 16,
                'code'           => 'B.2.1',
                'name'           => 'Understand the IT Environment',
            ],
            [
                'audit_stage_id' => 16,
                'code'           => 'B.2.2',
                'name'           => 'Understand the Control Environment',
            ],
            // D.3
            [
                'audit_stage_id' => 39,
                'code'           => '1.101',
                'name'           => 'Cash and Cash Equivalent',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.102',
                'name'           => 'Time Deposits',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.103',
                'name'           => 'Derivative Assets',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.104',
                'name'           => 'Investment Securities Available for Sale',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.105',
                'name'           => 'Trade Receivable - Current',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.106',
                'name'           => 'Others Receivable - Current',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.107',
                'name'           => 'Credit and Financing',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.108',
                'name'           => 'Inventories',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.109',
                'name'           => 'Prepaid Taxes - Current Portion',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.110',
                'name'           => 'Advances and Prepayment - Current Portion',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.111',
                'name'           => 'Other Current Financial Assets',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.201',
                'name'           => 'Trade Receivable - Non-current',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.202',
                'name'           => 'Other Receivable - Non-current',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.203',
                'name'           => 'Credit and Financing - Non-current',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.204',
                'name'           => 'Investment in share',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.205',
                'name'           => 'Inventories - Non-current',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.206',
                'name'           => 'Investment Properties - Net',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.207',
                'name'           => 'Fixed assets - Net',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.208',
                'name'           => 'Right of Use Assets',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.209',
                'name'           => 'Biological Asset',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.210',
                'name'           => 'Intangible Assets',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.211',
                'name'           => 'Deferred charges',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.212',
                'name'           => 'Deferred tax assets',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.213',
                'name'           => 'Goodwill',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.214',
                'name'           => 'Prepaid Taxes - Non-current Portion',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.215',
                'name'           => 'Advances and Prepayment - Non-current Portion',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.216',
                'name'           => 'Other Financial Assets',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '1.217',
                'name'           => 'Other Non-current Assets',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '2.101',
                'name'           => 'Accounts Payable - Short Term',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '2.102',
                'name'           => 'Other Payables - Short Term',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '2.103',
                'name'           => 'Advances and Unearned Income',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '2.104',
                'name'           => 'Bank Loans - Short Term',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '2.105',
                'name'           => 'Securities Payable - Short Term',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '2.106',
                'name'           => 'Lease Liability - Short Term',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '2.107',
                'name'           => 'Insurance Liability - Short Term',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '2.108',
                'name'           => 'Tax Payables',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '2.109',
                'name'           => 'Accrued expenses',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '2.110',
                'name'           => 'Other Short-Term Liabilities',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '2.201',
                'name'           => 'Bank Loans - Long Term Portion',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '2.202',
                'name'           => 'Other Payables - Long Term Portion',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '2.203',
                'name'           => 'Securities Payable - Long Term Portion',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '2.204',
                'name'           => 'Lease Liability - Long Term Portion',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '2.206',
                'name'           => 'Insurance Liability - Long Term Portion',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '2.208',
                'name'           => 'Advances and Unearned Income - Long Term Portion',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '2.209',
                'name'           => 'Employee Benefits Liability',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '2.210',
                'name'           => 'Guarantee Debt',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '2.211',
                'name'           => 'Derivative Financial Instruments',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '2.212',
                'name'           => 'Provision',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '2.213',
                'name'           => 'Other Long-Term Liabilities',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '3.101',
                'name'           => 'Capital stock',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '3.102',
                'name'           => 'Additional Paid-in Capital',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '3.103',
                'name'           => 'Other Comprehensive Income Balance',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '3.104',
                'name'           => 'Retained earning',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '3.105',
                'name'           => 'Other Equity Component',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '3.201',
                'name'           => 'Equity Attributed to Non-controlling Interests',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '4.101',
                'name'           => 'Revenues',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '4.102',
                'name'           => 'Financial Income',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '4.103',
                'name'           => 'Insurance Income',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '4.104',
                'name'           => 'Other Operating Income',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '5.101',
                'name'           => 'Cost of goods sold',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '5.102',
                'name'           => 'Marketing Expenses',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '5.103',
                'name'           => 'General and Administrative Expenses',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '5.201',
                'name'           => 'Share of Profit/(loss) of Associates, Joint Ventures and Joint Controls',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '5.301',
                'name'           => 'Interest expense',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '5.401',
                'name'           => 'Other Income (Expenses)',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '5.501',
                'name'           => 'Final Income Tax',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '5.601',
                'name'           => 'Income Tax',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '6.101',
                'name'           => 'Share of Profit/(Loss) and Other Comprehensive Income of Associates and Joint Ventures',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '6.201',
                'name'           => 'Other Comprehensive Income - Not Reclassified to Profit and Loss',
            ],
            [
                'audit_stage_id' => 39,
                'code'           => '6.301',
                'name'           => 'Other Comprehensive Income - Reclassified to Profit and Loss',
            ],
        ];

        foreach ($data as $key => $item) {
            $item['workspace_type'] = 'AUD';
            AuditStage::create($item);
        }
    }
}
