<?php

namespace Database\Seeders;

use App\Models\Package;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Package::truncate();

        $data = [
            [
                'id'          => 1,
                'name'        => 'Free Zone',
                'description' => [
                    '5 User, 5Gb SSD (RAID 1)',
                    'Shared Server',
                    'Automatic Backup',
                    'Security SSL',
                    'APRO international modul',
                    'Archiving encrypted in ZIP',
                ],
                'max_user'    => 5,
                'storage'     => 5,
                'price'       => 0,
                'period'      => null,
            ],
            [
                'id'          => 2,
                'name'        => 'Starter',
                'description' => [
                    '12 User, 20Gb SSD (RAID 1)',
                    'Shared Server',
                    'Automatic Backup',
                    'Security SSL',
                    'APRO international modul',
                    'Add Company Logo',
                    'Archiving encrypted in ZIP',
                ],
                'max_user'    => 12,
                'storage'     => 20,
                'price'       => 250000,
                'period'      => 1,
            ],
            [
                'id'          => 3,
                'name'        => 'Small',
                'description' => [
                    '26 User, 40Gb SSD (RAID 1)',
                    'Shared Server',
                    'Automatic Backup',
                    'Security SSL',
                    'APRO international modul',
                    'Add Company Logo',
                    'Archiving encrypted in ZIP',
                ],
                'max_user'    => 26,
                'storage'     => 40,
                'price'       => 500000,
                'period'      => 1,
            ],
            [
                'id'          => 4,
                'name'        => 'Medium',
                'description' => [
                    '58 User, 80Gb SSD (RAID 1)',
                    'Shared Server',
                    'Automatic Backup',
                    'Security SSL',
                    'APRO international modul',
                    'Add Company Logo',
                    'Archiving encrypted in ZIP',
                    'Timesheet',
                ],
                'max_user'    => 58,
                'storage'     => 80,
                'price'       => 1500000,
                'period'      => 1,
            ],
            [
                'id'          => 5,
                'name'        => 'Business',
                'description' => [
                    '128 User, 200Gb SSD (RAID 1)',
                    'Dedicated Server',
                    'Automatic Backup',
                    'Security SSL',
                    'APRO international modul',
                    'Add Company Logo',
                    'Archiving encrypted in ZIP',
                    'Timesheet',
                    'PBC Form',
                ],
                'max_user'    => 128,
                'storage'     => 200,
                'price'       => 5000000,
                'period'      => 1,
            ],
            [
                'id'          => 6,
                'name'        => 'Enterprise',
                'description' => [
                    'Unlimited user, 480Gb X 2 SSD (RAID 1)',
                    'Dedicated Server',
                    'Automatic Backup',
                    'Security SSL',
                    'APRO international modul',
                    'Add Company Logo',
                    'Archiving encrypted in ZIP',
                    'Timesheet',
                    'PBC Form',
                    'Customize form/working paper add on',
                ],
                'max_user'    => null,
                'storage'     => 480,
                'price'       => null,
                'period'      => 1,
            ]
        ];

        foreach ($data as $key => $item) {
            Package::create($item);
        }
    }
}
