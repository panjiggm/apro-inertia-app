<?php

namespace Database\Seeders;

use App\Models\Auditor;
use App\Models\AuditorRole;
use App\Models\Company;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Auditor::truncate();
        Company::truncate();
        User::truncate();

        // Super Admin
        User::create([
            'id'                => 1,
            'company_id'        => null,
            'auditor_id'        => null,
            'name'              => 'Admin',
            'email'             => 'admin@mail.com',
            'password'          => bcrypt('123456'),
            'role'              => 'super-admin',
            'status'            => 'Active',
            'email_verified_at' => date('Y-m-d H:i:s'),
        ]);

        $faker = Faker::create();
        $auditor_role_ids = AuditorRole::pluck('id');

        for ($i=0; $i < 10; $i++) {
            $tax_id_number = $i == 0 ? '112223334555666' : randomNumber(16);
            $code = $i == 0 ? '222333-01' : companyCode($tax_id_number, 1);
            $date_time = $faker->dateTimeBetween('-30 years', 'now');
            $date = $date_time->format('Y-m-d');
            $company_name = $faker->company;
            $managing_partner_name = $faker->firstName;

            $company = Company::create([
                'code'                    => $code,
                'tax_id_number'           => $tax_id_number,
                'name'                    => $company_name,
                'phone'                   => '021' . $faker->randomNumber(8),
                'address'                 => $faker->address,
                'business_id_number'      => randomNumber(13),
                'business_id_date'        => $date,
                'business_license_number' => $faker->randomNumber(8),
                'business_license_date'   => $date,
                'practice_license_number' => strtoupper(Str::random(3)) .'/'. $faker->randomNumber(6),
                'practice_expiry_date'    => $faker->dateTimeBetween('+1 years', '+5 years'),
                'website'                 => Str::slug($company_name, '-') . '.com',
                'managing_partner_name'   => $managing_partner_name,
                'managing_partner_email'  => Str::slug($managing_partner_name, '_') . '@mail.com',
                'managing_partner_phone'  => '081' . randomNumber(10),
                'pic_phone'               => '082' . randomNumber(10),
                'pic_status'              => $i % 2 == 0 ? 'Permanent' : 'Contract'
            ]);

            $user_company = User::create([
                'company_id'        => $company->id,
                'auditor_id'        => null,
                'name'              => $faker->firstName,
                'email'             => 'company' . $i+1 . '@mail.com',
                'password'          => bcrypt('123456'),
                'role'              => 'company-admin',
                'status'            => 'Active',
                'email_verified_at' => date('Y-m-d H:i:s'),
            ]);

            for ($k=0; $k < 5; $k++) {
                $auditor = Auditor::create([
                    'auditor_role_id' => $k==0 ? 4 : $auditor_role_ids->random(),
                    'staff_id'        => $faker->randomNumber(4),
                    'id_number'       => randomNumber(16),
                    'address'         => $faker->address,
                    'education'       => 'S1 Akutansi',
                ]);

                User::create([
                    'company_id' => $company->id,
                    'auditor_id' => $auditor->id,
                    'name'       => $faker->firstName,
                    'email'      => 'auditor' . $i+1 . $k+1 . '@mail.com',
                    'password'   => bcrypt('123456'),
                    'role'       => 'auditor',
                    'status'     => 'Active',
                    'email_verified_at' => date('Y-m-d H:i:s'),
                ]);
            }
        }
    }
}
