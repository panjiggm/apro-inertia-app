<?php

namespace Database\Seeders;

use App\Models\PackageAddOn;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PackageAddOnSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        PackageAddOn::truncate();

        $data = [
            [
                'id'          => 1,
                'name'        => 'Add User',
                'description' => 'Rp 38.000/User/Month',
                'unit'        => 'User',
                'price'       => 38000,
            ],
            [
                'id'          => 2,
                'name'        => 'Storage',
                'description' => 'Rp 5.500/1 Gb/Month',
                'unit'        => 'Gb',
                'price'       => 5500,
            ],
            [
                'id'          => 3,
                'name'        => 'Timesheet',
                'description' => 'Rp 150.000/Month',
                'unit'        => null,
                'price'       => 150000,
            ],
            [
                'id'          => 4,
                'name'        => 'PCC',
                'description' => 'Rp 150.000/Month',
                'unit'        => null,
                'price'       => 150000,
            ],
        ];

        foreach ($data as $key => $item) {
            PackageAddOn::create($item);
        }
    }
}
