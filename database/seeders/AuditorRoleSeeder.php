<?php

namespace Database\Seeders;

use App\Models\AuditorRole;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class AuditorRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AuditorRole::truncate();

        $offices = [
            'Junior Auditor',
            'Senior Auditor',
            'Supervisor',
            'Manager',
            'Partner',
        ];

        $projects = [
            'Managing Partner',
            'Risk Management Partner',
            'EQCR Partner',
            'Partner',
            'Manager',
            'Auditor',
            'Auditor Expert',
            'Guest',
        ];

        $sign_offs = [
            'Preparer',
            'Checker',
            'Reviewer',
            'Approver',
        ];

        foreach ($offices as $key => $value) {
            AuditorRole::create([
                'type'       => 'Office',
                'name'       => $value,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }

        foreach ($projects as $key => $value) {
            AuditorRole::create([
                'type'       => 'Project',
                'name'       => $value,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }

        foreach ($sign_offs as $key => $value) {
            AuditorRole::create([
                'type'       => 'Sign Off',
                'name'       => $value,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
    }
}
